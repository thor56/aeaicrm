/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50151
Source Host           : localhost:3306
Source Database       : aeaicrm

Target Server Type    : MYSQL
Target Server Version : 50151
File Encoding         : 65001

Date: 2016-05-11 19:20:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for crm_city
-- ----------------------------
DROP TABLE IF EXISTS `crm_city`;
CREATE TABLE `crm_city` (
  `CODE_ID` varchar(32) DEFAULT NULL,
  `CITY_ID` varchar(32) NOT NULL,
  `CITY_NAME` varchar(32) DEFAULT NULL,
  `CITY_SORT` int(32) DEFAULT NULL,
  PRIMARY KEY (`CITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_city
-- ----------------------------
INSERT INTO `crm_city` VALUES ('SiChuan', 'ABa', '阿坝', '19');
INSERT INTO `crm_city` VALUES ('XinJiang', 'AKeSu', '阿克苏', '6');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'ALaShan', '阿拉善', '12');
INSERT INTO `crm_city` VALUES ('XinJiang', 'ALeTai', '阿勒泰', '14');
INSERT INTO `crm_city` VALUES ('XiZang', 'ALi', '阿里', '6');
INSERT INTO `crm_city` VALUES ('SanXi', 'AnKang', '安康', '9');
INSERT INTO `crm_city` VALUES ('AnHui', 'AnQing', '安庆', '8');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'AnShan', '鞍山', '3');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'AnShun', '安顺', '4');
INSERT INTO `crm_city` VALUES ('HeNan', 'AnYang', '安阳', '8');
INSERT INTO `crm_city` VALUES ('JiLin', 'BaiCheng', '白城', '8');
INSERT INTO `crm_city` VALUES ('GuangXi', 'BaiSe', '百色', '10');
INSERT INTO `crm_city` VALUES ('JiLin', 'BaiShan', '白山', '6');
INSERT INTO `crm_city` VALUES ('GanSu', 'BaiYin', '白银', '4');
INSERT INTO `crm_city` VALUES ('HeBei', 'BaoDing', '保定', '6');
INSERT INTO `crm_city` VALUES ('SanXi', 'BaoJi', '宝鸡', '3');
INSERT INTO `crm_city` VALUES ('YunNan', 'BaoShan', '保山', '4');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'BaoTou', '包头', '2');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'BaYanNaoEr', '巴彦淖尔', '8');
INSERT INTO `crm_city` VALUES ('XinJiang', 'BaYinGuoLeMengGu', '巴音郭楞蒙古', '9');
INSERT INTO `crm_city` VALUES ('SiChuan', 'BaZhong', '巴中', '17');
INSERT INTO `crm_city` VALUES ('GuangXi', 'BeiHai', '北海', '5');
INSERT INTO `crm_city` VALUES ('AnHui', 'BengBu', '蚌埠', '3');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'BenXi', '本溪', '5');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'BiJie', '毕节', '6');
INSERT INTO `crm_city` VALUES ('ShanDong', 'BinZhou', '滨州', '16');
INSERT INTO `crm_city` VALUES ('XinJiang', 'BoErTaLaMengGu', '博尔塔拉蒙古', '11');
INSERT INTO `crm_city` VALUES ('AnHui', 'BoZhou', '亳州', '15');
INSERT INTO `crm_city` VALUES ('HeBei', 'CangZhou', '沧州', '9');
INSERT INTO `crm_city` VALUES ('JiLin', 'ChangChun', '长春', '1');
INSERT INTO `crm_city` VALUES ('HuNan', 'ChangDe', '常德', '7');
INSERT INTO `crm_city` VALUES ('XiZang', 'ChangDu', '昌都', '2');
INSERT INTO `crm_city` VALUES ('XinJiang', 'ChangJi', '昌吉', '10');
INSERT INTO `crm_city` VALUES ('HuNan', 'ChangSha', '长沙', '1');
INSERT INTO `crm_city` VALUES ('Shanxi', 'ChangZhi', '长治', '4');
INSERT INTO `crm_city` VALUES ('JiangSu', 'ChangZhou', '常州', '4');
INSERT INTO `crm_city` VALUES ('AnHui', 'ChaoHu', '巢湖', '13');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'ChaoYang', '朝阳', '13');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ChaoZhou', '潮州', '19');
INSERT INTO `crm_city` VALUES ('HeBei', 'ChengDe', '承德', '8');
INSERT INTO `crm_city` VALUES ('SiChuan', 'ChengDu', '成都', '1');
INSERT INTO `crm_city` VALUES ('HuNan', 'ChenZhou', '郴州', '10');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'ChiFeng', '赤峰', '4');
INSERT INTO `crm_city` VALUES ('AnHui', 'ChiZhou', '池州', '16');
INSERT INTO `crm_city` VALUES ('GuangXi', 'ChongZuo', '崇左', '14');
INSERT INTO `crm_city` VALUES ('YunNan', 'ChuXiong', '楚雄', '12');
INSERT INTO `crm_city` VALUES ('AnHui', 'ChuZhou', '滁州', '10');
INSERT INTO `crm_city` VALUES ('YunNan', 'DaLi', '大理', '13');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'DaLian', '大连', '2');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'DanDong', '丹东', '6');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'DaQing', '大庆', '6');
INSERT INTO `crm_city` VALUES ('Shanxi', 'DaTong', '大同', '2');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'DaXingAnLing', '大兴安岭', '13');
INSERT INTO `crm_city` VALUES ('SiChuan', 'DaZhou', '达州', '14');
INSERT INTO `crm_city` VALUES ('YunNan', 'DeHong', '德宏', '14');
INSERT INTO `crm_city` VALUES ('SiChuan', 'DeYang', '德阳', '5');
INSERT INTO `crm_city` VALUES ('ShanDong', 'DeZhou', '德州', '14');
INSERT INTO `crm_city` VALUES ('GanSu', 'DingXi', '定西', '11');
INSERT INTO `crm_city` VALUES ('YunNan', 'DiQing', '迪庆', '16');
INSERT INTO `crm_city` VALUES ('GuangDong', 'DongGuan', '东莞', '17');
INSERT INTO `crm_city` VALUES ('ShanDong', 'DongYing', '东营', '5');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'EErDuoSi', '鄂尔多斯', '6');
INSERT INTO `crm_city` VALUES ('HuBei', 'EnShi', '恩施', '13');
INSERT INTO `crm_city` VALUES ('HuBei', 'EZhou', '鄂州', '8');
INSERT INTO `crm_city` VALUES ('GuangXi', 'FangChengGang', '防城港', '6');
INSERT INTO `crm_city` VALUES ('GuangDong', 'Foshan', '佛山', '6');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'FuShun', '抚顺', '4');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'FuXin', '阜新', '9');
INSERT INTO `crm_city` VALUES ('AnHui', 'FuYang', '阜阳', '11');
INSERT INTO `crm_city` VALUES ('FuJian', 'FuZhou', '福州', '1');
INSERT INTO `crm_city` VALUES ('JiangXi', 'FuZhouShi', '抚州', '10');
INSERT INTO `crm_city` VALUES ('GanSu', 'GanNan', '甘南', '14');
INSERT INTO `crm_city` VALUES ('JiangXi', 'GanZhou', '赣州', '7');
INSERT INTO `crm_city` VALUES ('SiChuan', 'GanZi', '甘孜', '20');
INSERT INTO `crm_city` VALUES ('TaiWan', 'GaoXiong', '高雄', '2');
INSERT INTO `crm_city` VALUES ('SiChuan', 'GuangAn', '广安', '13');
INSERT INTO `crm_city` VALUES ('SiChuan', 'GuangYuan', '广元', '7');
INSERT INTO `crm_city` VALUES ('GuangDong', 'GuangZhou', '广州', '1');
INSERT INTO `crm_city` VALUES ('GuangXi', 'GuiGang', '贵港', '8');
INSERT INTO `crm_city` VALUES ('GuangXi', 'GuiLin', '桂林', '3');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'GuiYang', '贵阳', '1');
INSERT INTO `crm_city` VALUES ('QingHai', 'GuoLuo', '果洛', '6');
INSERT INTO `crm_city` VALUES ('NingXia', 'GuYuan', '固原', '4');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'HaErBin', '哈尔滨', '1');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiBei', '海北', '3');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiDong', '海东', '2');
INSERT INTO `crm_city` VALUES ('HaiNan', 'HaiKou', '海口', '1');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiNanShi', '海南', '5');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiXi', '海西', '8');
INSERT INTO `crm_city` VALUES ('XinJiang', 'HaMi', '哈密', '4');
INSERT INTO `crm_city` VALUES ('HeBei', 'HanDan', '邯郸', '4');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'HangZhou', '杭州', '1');
INSERT INTO `crm_city` VALUES ('SanXi', 'HanZhong', '汉中', '7');
INSERT INTO `crm_city` VALUES ('HeNan', 'HeBi', '鹤壁', '6');
INSERT INTO `crm_city` VALUES ('GuangXi', 'HeChi', '河池', '12');
INSERT INTO `crm_city` VALUES ('AnHui', 'HeFei', '合肥', '1');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'HeGang', '鹤岗', '4');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'HeiHe', '黑河', '11');
INSERT INTO `crm_city` VALUES ('HeBei', 'HengShui', '衡水', '11');
INSERT INTO `crm_city` VALUES ('HuNan', 'HengYang', '衡阳', '4');
INSERT INTO `crm_city` VALUES ('XinJiang', 'HeTian', '和田', '5');
INSERT INTO `crm_city` VALUES ('GuangDong', 'HeYuan', '河源', '14');
INSERT INTO `crm_city` VALUES ('ShanDong', 'HeZe', '菏泽', '17');
INSERT INTO `crm_city` VALUES ('GuangXi', 'HeZhou', '贺州', '11');
INSERT INTO `crm_city` VALUES ('YunNan', 'HongHe', '红河', '10');
INSERT INTO `crm_city` VALUES ('JiangSu', 'HuaiAn', '淮安', '8');
INSERT INTO `crm_city` VALUES ('AnHui', 'HuaiBei', '淮北', '6');
INSERT INTO `crm_city` VALUES ('HuNan', 'HuaiHua', '怀化', '12');
INSERT INTO `crm_city` VALUES ('AnHui', 'HuaiNan', '淮南', '4');
INSERT INTO `crm_city` VALUES ('HuBei', 'HuangGang', '黄冈', '10');
INSERT INTO `crm_city` VALUES ('QingHai', 'HuangNan', '黄南', '4');
INSERT INTO `crm_city` VALUES ('AnHui', 'HuangShan', '黄山', '9');
INSERT INTO `crm_city` VALUES ('HuBei', 'Huangshi', '黄石', '2');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'HuHuHaoTe', '呼呼浩特', '1');
INSERT INTO `crm_city` VALUES ('GuangDong', 'HuiZhou', '惠州', '11');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'HuLuDao', '葫芦岛', '14');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'HuLuoBeiEr', '呼伦贝尔', '7');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'HuZhou', '湖州', '5');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'JiaMuSi', '佳木斯', '8');
INSERT INTO `crm_city` VALUES ('JiangXi', 'JiAn', '吉安', '8');
INSERT INTO `crm_city` VALUES ('GuangDong', 'JiangMen', '江门', '7');
INSERT INTO `crm_city` VALUES ('HeNan', 'JiaoZuo', '焦作', '5');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'JiaXing', '嘉兴', '4');
INSERT INTO `crm_city` VALUES ('TaiWan', 'JiaYi', '嘉义', '7');
INSERT INTO `crm_city` VALUES ('GanSu', 'JiaYuGuan', '嘉峪关', '2');
INSERT INTO `crm_city` VALUES ('GuangDong', 'JieYang', '揭阳', '20');
INSERT INTO `crm_city` VALUES ('JiLin', 'JiLinShi', '吉林', '2');
INSERT INTO `crm_city` VALUES ('TaiWan', 'JiLong', '基隆', '3');
INSERT INTO `crm_city` VALUES ('ShanDong', 'JiNan', '济南', '1');
INSERT INTO `crm_city` VALUES ('GanSu', 'JinChang', '金昌', '3');
INSERT INTO `crm_city` VALUES ('Shanxi', 'JinCheng', '晋城', '5');
INSERT INTO `crm_city` VALUES ('JiangXi', 'JingDeZhen', '景德镇', '2');
INSERT INTO `crm_city` VALUES ('HuBei', 'JingMen', '荆门', '7');
INSERT INTO `crm_city` VALUES ('HuBei', 'JingZhou', '荆州', '5');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'JinHua', '金华', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'JiNing', '济宁', '9');
INSERT INTO `crm_city` VALUES ('Shanxi', 'JinZhong', '晋中', '7');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'JinZhou', '锦州', '7');
INSERT INTO `crm_city` VALUES ('JiangXi', 'JiuJiang', '九江', '4');
INSERT INTO `crm_city` VALUES ('GanSu', 'JiuQuan', '酒泉', '9');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'JiXi', '鸡西', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'KaiFeng', '开封', '2');
INSERT INTO `crm_city` VALUES ('XinJiang', 'KaShi', '喀什', '7');
INSERT INTO `crm_city` VALUES ('XinJiang', 'KeLaMaYi', '克拉玛依', '2');
INSERT INTO `crm_city` VALUES ('XinJiang', 'KeZiLeSuKeErKeZi', '克孜勒苏柯尔克孜', '8');
INSERT INTO `crm_city` VALUES ('YunNan', 'KunMing', '昆明', '1');
INSERT INTO `crm_city` VALUES ('GuangXi', 'LaiBin', '来宾', '13');
INSERT INTO `crm_city` VALUES ('ShanDong', 'LaiWu', '莱芜', '12');
INSERT INTO `crm_city` VALUES ('HeBei', 'LangFang', '廊坊', '10');
INSERT INTO `crm_city` VALUES ('GanSu', 'LanZhou', '兰州', '1');
INSERT INTO `crm_city` VALUES ('XiZang', 'LaSa', '拉萨', '1');
INSERT INTO `crm_city` VALUES ('SiChuan', 'LeShan', '乐山', '10');
INSERT INTO `crm_city` VALUES ('SiChuan', 'LiangShan', '凉山', '21');
INSERT INTO `crm_city` VALUES ('JiangSu', 'LianYunGang', '连云港', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'LiaoCheng', '聊城', '15');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'LiaoYang', '辽阳', '10');
INSERT INTO `crm_city` VALUES ('JiLin', 'LiaoYuan', '辽源', '4');
INSERT INTO `crm_city` VALUES ('YunNan', 'LiJiang', '丽江', '6');
INSERT INTO `crm_city` VALUES ('YunNan', 'LinCang', '临沧', '8');
INSERT INTO `crm_city` VALUES ('Shanxi', 'LinFen', '临汾', '10');
INSERT INTO `crm_city` VALUES ('GanSu', 'LinXia', '临夏', '13');
INSERT INTO `crm_city` VALUES ('ShanDong', 'LinYi', '临沂', '13');
INSERT INTO `crm_city` VALUES ('XiZang', 'LinZhi', '林芝', '7');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'LiShui', '丽水', '11');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'LiuPanShui', '六盘水', '2');
INSERT INTO `crm_city` VALUES ('GuangXi', 'LiuZhou', '柳州', '2');
INSERT INTO `crm_city` VALUES ('GanSu', 'LongNan', '陇南', '12');
INSERT INTO `crm_city` VALUES ('FuJian', 'LongYan', '龙岩', '8');
INSERT INTO `crm_city` VALUES ('HuNan', 'LouDi', '娄底', '13');
INSERT INTO `crm_city` VALUES ('AnHui', 'LuAn', '六安', '14');
INSERT INTO `crm_city` VALUES ('HeNan', 'LuoHe', '漯河', '11');
INSERT INTO `crm_city` VALUES ('HeNan', 'Luoyang', '洛阳', '3');
INSERT INTO `crm_city` VALUES ('SiChuan', 'LuZhou', '泸州', '4');
INSERT INTO `crm_city` VALUES ('Shanxi', 'LvLiang', '吕梁', '11');
INSERT INTO `crm_city` VALUES ('AnHui', 'MaAnShan', '马鞍山', '5');
INSERT INTO `crm_city` VALUES ('GuangDong', 'MaoMing', '茂名', '9');
INSERT INTO `crm_city` VALUES ('SiChuan', 'MeiShan', '眉山', '15');
INSERT INTO `crm_city` VALUES ('GuangDong', 'MeiZhou', '梅州', '12');
INSERT INTO `crm_city` VALUES ('SiChuan', 'MianYang', '绵阳', '6');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'MuDanJiang', '牡丹江', '10');
INSERT INTO `crm_city` VALUES ('JiangXi', 'NanChang', '南昌', '1');
INSERT INTO `crm_city` VALUES ('SiChuan', 'NanChong', '南充', '11');
INSERT INTO `crm_city` VALUES ('JiangSu', 'NanJing', '南京', '1');
INSERT INTO `crm_city` VALUES ('GuangXi', 'NanNing', '南宁', '1');
INSERT INTO `crm_city` VALUES ('FuJian', 'NanPing', '南平', '7');
INSERT INTO `crm_city` VALUES ('JiangSu', 'NanTong', '南通', '6');
INSERT INTO `crm_city` VALUES ('HeNan', 'NanYang', '南阳', '13');
INSERT INTO `crm_city` VALUES ('XiZang', 'NaQv', '那曲', '5');
INSERT INTO `crm_city` VALUES ('SiChuan', 'NeiJiang', '内江', '9');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'NingBo', '宁波', '2');
INSERT INTO `crm_city` VALUES ('FuJian', 'NingDe', '宁德', '9');
INSERT INTO `crm_city` VALUES ('YunNan', 'NuJiang', '怒江', '15');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'PanJin', '盘锦', '11');
INSERT INTO `crm_city` VALUES ('SiChuan', 'PanZhiHua', '攀枝花', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'PingDingShan', '平顶山', '4');
INSERT INTO `crm_city` VALUES ('GanSu', 'PingLiang', '平凉', '8');
INSERT INTO `crm_city` VALUES ('JiangXi', 'PingXiang', '萍乡', '3');
INSERT INTO `crm_city` VALUES ('YunNan', 'PuEr', '普洱', '7');
INSERT INTO `crm_city` VALUES ('FuJian', 'PuTian', '莆田', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'PuYang', '濮阳', '9');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'QianDongNan', '黔东南', '8');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'QianNan', '黔南', '9');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'QianXiNan', '黔西南', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'QingDao', '青岛', '2');
INSERT INTO `crm_city` VALUES ('HeBei', 'QingHuangDao', '秦皇岛', '3');
INSERT INTO `crm_city` VALUES ('GanSu', 'QingYang', '庆阳', '10');
INSERT INTO `crm_city` VALUES ('GuangDong', 'QingYuan', '清远', '16');
INSERT INTO `crm_city` VALUES ('GuangXi', 'QinZhou', '钦州', '7');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'QiQiHaEr', '齐齐哈尔', '2');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'QiTaiHe', '七台河', '9');
INSERT INTO `crm_city` VALUES ('FuJian', 'QuanZhou', '泉州', '5');
INSERT INTO `crm_city` VALUES ('YunNan', 'QvJing', '曲靖', '2');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'QvZhou', '衢州', '8');
INSERT INTO `crm_city` VALUES ('XiZang', 'RiKaZe', '日喀则', '4');
INSERT INTO `crm_city` VALUES ('ShanDong', 'RiZhao', '日照', '11');
INSERT INTO `crm_city` VALUES ('HeNan', 'SanMenXia', '三门峡', '12');
INSERT INTO `crm_city` VALUES ('FuJian', 'SanMing', '三明', '4');
INSERT INTO `crm_city` VALUES ('HaiNan', 'SanYa', '三亚', '2');
INSERT INTO `crm_city` VALUES ('SanXi', 'ShangLuo', '商洛', '10');
INSERT INTO `crm_city` VALUES ('HeNan', 'ShangQiU', '商丘', '14');
INSERT INTO `crm_city` VALUES ('JiangXi', 'ShangRao', '上饶', '11');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'ShangYaShan', '双鸭山', '5');
INSERT INTO `crm_city` VALUES ('XiZang', 'ShanNan', '山南', '3');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShanTou', '汕头', '4');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShanWei', '汕尾', '13');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShaoGuan', '韶关', '5');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'ShaoXing', '绍兴', '6');
INSERT INTO `crm_city` VALUES ('HuNan', 'ShaoYang', '邵阳', '5');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'ShenYang', '沈阳', '1');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShenZhen', '深圳', '2');
INSERT INTO `crm_city` VALUES ('HeBei', 'ShiJiaZhuag', '石家庄', '1');
INSERT INTO `crm_city` VALUES ('HuBei', 'ShiYan', '十堰', '4');
INSERT INTO `crm_city` VALUES ('NingXia', 'ShiZuiShan', '石嘴山', '2');
INSERT INTO `crm_city` VALUES ('JiLin', 'SiPing', '四平', '3');
INSERT INTO `crm_city` VALUES ('JiLin', 'SongYuan', '松原', '7');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'SuiHua', '绥化', '12');
INSERT INTO `crm_city` VALUES ('SiChuan', 'SuiNing', '遂宁', '8');
INSERT INTO `crm_city` VALUES ('HuBei', 'SuiZhou', '随州', '12');
INSERT INTO `crm_city` VALUES ('Shanxi', 'SuoZhou', '朔州', '6');
INSERT INTO `crm_city` VALUES ('JiangSu', 'SuQian', '宿迁', '13');
INSERT INTO `crm_city` VALUES ('JiangSu', 'SuZhou', '苏州', '5');
INSERT INTO `crm_city` VALUES ('AnHui', 'SuZhouShi', '宿州', '12');
INSERT INTO `crm_city` VALUES ('XinJiang', 'TaCheng', '塔城', '13');
INSERT INTO `crm_city` VALUES ('ShanDong', 'TaiAn', '泰安', '10');
INSERT INTO `crm_city` VALUES ('TaiWan', 'TaiBei', '台北', '1');
INSERT INTO `crm_city` VALUES ('TaiWan', 'TaiNan', '台南', '5');
INSERT INTO `crm_city` VALUES ('Shanxi', 'TaiYuan', '太原', '1');
INSERT INTO `crm_city` VALUES ('TaiWan', 'TaiZhong', '台中', '4');
INSERT INTO `crm_city` VALUES ('JiangSu', 'TaiZhou', '泰州', '12');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'TaiZhouShi', '台州', '10');
INSERT INTO `crm_city` VALUES ('HeBei', 'TangShan', '唐山', '2');
INSERT INTO `crm_city` VALUES ('GanSu', 'TianShui', '天水', '5');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'TieLing', '铁岭', '12');
INSERT INTO `crm_city` VALUES ('SanXi', 'TongChuan', '铜川', '2');
INSERT INTO `crm_city` VALUES ('JiLin', 'TongHua', '通化', '5');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'TongLiao', '通辽', '5');
INSERT INTO `crm_city` VALUES ('AnHui', 'TongLing', '铜陵', '7');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'TongRen', '铜仁', '5');
INSERT INTO `crm_city` VALUES ('XinJiang', 'TuLuFan', '吐鲁番', '3');
INSERT INTO `crm_city` VALUES ('ShanDong', 'WeiFang', '潍坊', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'WeiHai', '威海', '8');
INSERT INTO `crm_city` VALUES ('SanXi', 'WeiNan', '渭南', '5');
INSERT INTO `crm_city` VALUES ('YunNan', 'WenShan', '文山', '9');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'WenZhou', '温州', '3');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'WuHai', '乌海', '3');
INSERT INTO `crm_city` VALUES ('HuBei', 'WuHan', '武汉', '1');
INSERT INTO `crm_city` VALUES ('AnHui', 'WuHu', '芜湖', '2');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'WuLanChaBu', '乌兰察布', '9');
INSERT INTO `crm_city` VALUES ('XinJiang', 'WuLuMuQi', '乌鲁木齐', '1');
INSERT INTO `crm_city` VALUES ('GanSu', 'WuWei', '武威', '6');
INSERT INTO `crm_city` VALUES ('JiangSu', 'WuXi', '无锡', '2');
INSERT INTO `crm_city` VALUES ('NingXia', 'WuZhong', '吴忠', '3');
INSERT INTO `crm_city` VALUES ('GuangXi', 'WuZhou', '梧州', '4');
INSERT INTO `crm_city` VALUES ('FuJian', 'XiaMen', '厦门', '2');
INSERT INTO `crm_city` VALUES ('SanXi', 'XiAn', '西安', '1');
INSERT INTO `crm_city` VALUES ('HuBei', 'XiangFan', '襄樊', '3');
INSERT INTO `crm_city` VALUES ('HuNan', 'XiangTan', '湘潭', '3');
INSERT INTO `crm_city` VALUES ('HuNan', 'XiangXi', '湘西', '14');
INSERT INTO `crm_city` VALUES ('HuBei', 'XianNing', '咸宁', '11');
INSERT INTO `crm_city` VALUES ('SanXi', 'XianYang', '咸阳', '4');
INSERT INTO `crm_city` VALUES ('HuBei', 'XiaoGan', '孝感', '9');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'XiLinGuoLe', '锡林郭勒', '11');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'XingAn', '兴安', '10');
INSERT INTO `crm_city` VALUES ('HeBei', 'XingTan', '邢台', '5');
INSERT INTO `crm_city` VALUES ('QingHai', 'XiNing', '西宁', '1');
INSERT INTO `crm_city` VALUES ('HeNan', 'XinXiang', '新乡', '7');
INSERT INTO `crm_city` VALUES ('HeNan', 'XinYang', '信阳', '15');
INSERT INTO `crm_city` VALUES ('JiangXi', 'XinYu', '新余', '5');
INSERT INTO `crm_city` VALUES ('Shanxi', 'XinZhou', '忻州', '9');
INSERT INTO `crm_city` VALUES ('TaiWan', 'XinZhu', '新竹', '6');
INSERT INTO `crm_city` VALUES ('YunNan', 'XiShungBanNa', '西双版纳', '11');
INSERT INTO `crm_city` VALUES ('AnHui', 'XuanCheng', '宣城', '17');
INSERT INTO `crm_city` VALUES ('HeNan', 'XvChang', '许昌', '10');
INSERT INTO `crm_city` VALUES ('JiangSu', 'XvZhou', '徐州', '3');
INSERT INTO `crm_city` VALUES ('SiChuan', 'YaAn', '雅安', '16');
INSERT INTO `crm_city` VALUES ('SanXi', 'YanAn', '延安', '6');
INSERT INTO `crm_city` VALUES ('JiLin', 'YanBian', '延边', '9');
INSERT INTO `crm_city` VALUES ('JiangSu', 'YanCheng', '盐城', '9');
INSERT INTO `crm_city` VALUES ('GuangDong', 'YangJiang', '阳江', '15');
INSERT INTO `crm_city` VALUES ('Shanxi', 'YangQuan', '阳泉', '3');
INSERT INTO `crm_city` VALUES ('JiangSu', 'YangZhou', '扬州', '10');
INSERT INTO `crm_city` VALUES ('ShanDong', 'YanTai', '烟台', '6');
INSERT INTO `crm_city` VALUES ('SiChuan', 'YiBin', '宜宾', '12');
INSERT INTO `crm_city` VALUES ('HuBei', 'YiChang', '宜昌', '6');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'YiChun', '伊春', '7');
INSERT INTO `crm_city` VALUES ('JiangXi', 'YiChunShi', '宜春', '9');
INSERT INTO `crm_city` VALUES ('XinJiang', 'YiLiHaSaKe', '伊犁哈萨克', '12');
INSERT INTO `crm_city` VALUES ('NingXia', 'YinChuan', ' 银川', '1');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'YingKou', '营口', '8');
INSERT INTO `crm_city` VALUES ('JiangXi', 'YingTan', '鹰潭', '6');
INSERT INTO `crm_city` VALUES ('HuNan', 'YiYang', '益阳', '9');
INSERT INTO `crm_city` VALUES ('HuNan', 'YongZhou', '永州', '11');
INSERT INTO `crm_city` VALUES ('HuNan', 'YueYang', '岳阳', '6');
INSERT INTO `crm_city` VALUES ('GuangXi', 'YuLin', '玉林', '9');
INSERT INTO `crm_city` VALUES ('SanXi', 'YuLinShi', '榆林', '8');
INSERT INTO `crm_city` VALUES ('Shanxi', 'YunCheng', '运城', '8');
INSERT INTO `crm_city` VALUES ('GuangDong', 'YunFu', '云浮', '21');
INSERT INTO `crm_city` VALUES ('QingHai', 'YuShu', '玉树', '7');
INSERT INTO `crm_city` VALUES ('YunNan', 'YuXi', '玉溪', '3');
INSERT INTO `crm_city` VALUES ('ShanDong', 'ZaoZhuang', '枣庄', '4');
INSERT INTO `crm_city` VALUES ('HuNan', 'ZhangJiaJie', '张家界', '8');
INSERT INTO `crm_city` VALUES ('HeBei', 'ZhangJiaKou', '张家口', '7');
INSERT INTO `crm_city` VALUES ('GanSu', 'ZhangYe', '张掖', '7');
INSERT INTO `crm_city` VALUES ('FuJian', 'ZhangZhou', '漳州', '6');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhanJiang', '湛江', '8');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhaoQing', '肇庆', '10');
INSERT INTO `crm_city` VALUES ('YunNan', 'ZhaoTong', '昭通', '5');
INSERT INTO `crm_city` VALUES ('HeNan', 'ZhengZhou', '郑州', '1');
INSERT INTO `crm_city` VALUES ('JiangSu', 'ZhenJiang', '镇江', '11');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhongShan', '中山', '18');
INSERT INTO `crm_city` VALUES ('NingXia', 'ZhongWei', '中卫', '5');
INSERT INTO `crm_city` VALUES ('HeNan', 'ZhouKou', '周口', '16');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'ZhouShan', '舟山', '9');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhuHai', '珠海', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'ZhuMaDian', '驻马店', '17');
INSERT INTO `crm_city` VALUES ('HuNan', 'ZhuZhou', '株洲', '2');
INSERT INTO `crm_city` VALUES ('ShanDong', 'ZiBo', '淄博', '3');
INSERT INTO `crm_city` VALUES ('SiChuan', 'ZiGong', '自贡', '2');
INSERT INTO `crm_city` VALUES ('SiChuan', 'ZiYang', '资阳', '18');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'ZunYi', '遵义', '3');

-- ----------------------------
-- Table structure for crm_contact_person
-- ----------------------------
DROP TABLE IF EXISTS `crm_contact_person`;
CREATE TABLE `crm_contact_person` (
  `CONT_ID` char(36) NOT NULL,
  `CUST_ID` char(36) DEFAULT NULL,
  `CONT_JOB` varchar(32) DEFAULT NULL,
  `CONT_NAME` varchar(32) DEFAULT NULL,
  `CONT_SEX` varchar(32) DEFAULT NULL,
  `CONT_PHONE` varchar(32) DEFAULT NULL,
  `CONT_EMAIL` varchar(32) DEFAULT NULL,
  `CONT_OTHER` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`CONT_ID`),
  KEY `FK_Reference_3` (`CUST_ID`),
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`CUST_ID`) REFERENCES `crm_customer_info` (`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_contact_person
-- ----------------------------
INSERT INTO `crm_contact_person` VALUES ('A30565FB-8A59-4089-ABE6-301F42930F97', '248881C2-4B31-4FB2-B957-88FF19676D71', '', '测试联系人', 'M', '', '', '');


-- ----------------------------
-- Table structure for crm_customer_group
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_group`;
CREATE TABLE `crm_customer_group` (
  `GRP_ID` char(36) NOT NULL,
  `GRP_SUP_ID` char(36) DEFAULT NULL,
  `GRP_CODE` varchar(32) DEFAULT NULL,
  `GRP_NAME` varchar(32) DEFAULT NULL,
  `GRP_SORT` int(11) DEFAULT NULL,
  `GRP_STATE` varchar(32) DEFAULT NULL,
  `GRP_DESC` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_group
-- ----------------------------
INSERT INTO `crm_customer_group` VALUES ('00000000-0000-0000-00000000000000000', '', 'Root', '组织名称', '0', '1', '');
INSERT INTO `crm_customer_group` VALUES ('2FCAAFDE-E2E8-41C2-9C1A-18248650F761', '00000000-0000-0000-00000000000000000', 'Government', '政务相关', '18', null, null);
INSERT INTO `crm_customer_group` VALUES ('338805DC-4095-4E00-B6DC-181F573D9090', '00000000-0000-0000-00000000000000000', 'HotelStores', '酒店卖场', '9', null, null);
INSERT INTO `crm_customer_group` VALUES ('5636C0A6-265D-4C25-A6C7-515A53577800', '00000000-0000-0000-00000000000000000', 'RetailIndustry', '零售行业', '10', null, null);
INSERT INTO `crm_customer_group` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '00000000-0000-0000-00000000000000000', 'TEMP', '临时分组', '5', null, null);
INSERT INTO `crm_customer_group` VALUES ('6EDF0BBE-7CCC-4423-9E68-4AD0A47C9B36', '00000000-0000-0000-00000000000000000', 'Heavymanufacturing', '制造重工', '19', null, null);
INSERT INTO `crm_customer_group` VALUES ('7BE8ED1C-784F-42BA-8C98-18D0741C4782', '00000000-0000-0000-00000000000000000', 'BankTelecom', '银行电信', '17', null, null);
INSERT INTO `crm_customer_group` VALUES ('877A3C17-603A-4D2B-A9DC-2EEC82BEDB9A', '00000000-0000-0000-00000000000000000', 'RelatedCase', '相关案例', '13', null, null);
INSERT INTO `crm_customer_group` VALUES ('932E1980-8078-4C15-95A7-9C8E5F3D5CB6', '00000000-0000-0000-00000000000000000', 'Otherindustries', '其它行业', '12', null, null);
INSERT INTO `crm_customer_group` VALUES ('9AA7658E-31E5-42BB-860D-6B4EF70814CF', '00000000-0000-0000-00000000000000000', 'Pharmaceuticalindustry', '医药行业', '16', null, null);
INSERT INTO `crm_customer_group` VALUES ('BDB7F8CB-761D-47A6-A8B1-96222DA734C9', '00000000-0000-0000-00000000000000000', 'Transportation', '交通运输', '7', null, null);
INSERT INTO `crm_customer_group` VALUES ('D2133B90-3532-480C-9BC9-A9B9842C7375', '00000000-0000-0000-00000000000000000', 'EducationCampus', '教育校园', '8', null, null);
INSERT INTO `crm_customer_group` VALUES ('E18B9133-A846-4B38-A911-2C710BFADCD3', '00000000-0000-0000-00000000000000000', 'ConstructionProperty', '建筑地产', '6', null, null);
INSERT INTO `crm_customer_group` VALUES ('EFAE7889-AA26-48EC-A9FB-37C4FC10C88C', '00000000-0000-0000-00000000000000000', 'Healthcare', '医疗卫生', '15', null, null);
INSERT INTO `crm_customer_group` VALUES ('F3CEA72B-398A-42D2-A1D1-545867D768EF', '00000000-0000-0000-00000000000000000', 'CoalEnergy', '煤炭能源', '11', null, null);
INSERT INTO `crm_customer_group` VALUES ('FB30368E-E04C-4420-968C-F7E0A3A946B5', '00000000-0000-0000-00000000000000000', 'TobaccoIndustry', '烟草行业', '14', null, null);

-- ----------------------------
-- Table structure for crm_customer_grp_rel
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_grp_rel`;
CREATE TABLE `crm_customer_grp_rel` (
  `GRP_ID` char(36) NOT NULL,
  `CUST_ID` char(36) NOT NULL,
  PRIMARY KEY (`GRP_ID`,`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_grp_rel
-- ----------------------------
INSERT INTO `crm_customer_grp_rel` VALUES ('00000000-0000-0000-00000000000000000', '36A58A7C-95C3-4A53-83D5-1202D95489A6');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '06781DD0-783A-4418-A194-7455127EEE13');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '097F7F1C-EE13-4154-96B8-DEFA7E7A6850');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '0DE950F5-9A93-40B0-9783-04833701BDCA');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '30C71012-C10A-4F96-89EF-81D8E378FDF6');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '36AAB050-1391-4A3D-A6BD-66B9F70F2B7E');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '402D00E9-A866-453F-89ED-2A71A2F8F676');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '5AF4C62D-8139-4B44-B6A7-F0C504A517C8');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '6412E06D-C989-440A-ADDD-616A3EEB78C5');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '81CBC064-32BB-4896-BD44-F9A27E7CB8A9');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', 'AD0C4E5E-8D82-4AC5-B390-946CE6D14676');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', 'BADAFCAE-892B-4920-BAAF-FA96EC234E52');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', 'DB1EF4CE-7C27-475D-8EF7-353EF53A3DC1');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', 'F70868B3-9682-4AD9-9139-CEE431966973');
INSERT INTO `crm_customer_grp_rel` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', 'FC00A863-62A0-461B-9B8F-264C39BD46A5');
INSERT INTO `crm_customer_grp_rel` VALUES ('932E1980-8078-4C15-95A7-9C8E5F3D5CB6', '36AAB050-1391-4A3D-A6BD-66B9F70F2B7E');
INSERT INTO `crm_customer_grp_rel` VALUES ('E18B9133-A846-4B38-A911-2C710BFADCD3', '248881C2-4B31-4FB2-B957-88FF19676D71');
-- ----------------------------
-- Table structure for crm_customer_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_info`;
CREATE TABLE `crm_customer_info` (
  `CUST_ID` char(36) NOT NULL,
  `CUST_NAME` varchar(32) DEFAULT NULL,
  `CUST_INDUSTRY` varchar(32) DEFAULT NULL,
  `CUST_PROVINCE` varchar(32) DEFAULT NULL,
  `CUST_CITY` varchar(32) DEFAULT NULL,
  `CUST_ADDRESS` varchar(128) DEFAULT NULL,
  `CUST_SCALE` varchar(32) DEFAULT NULL,
  `CUST_NATURE` varchar(32) DEFAULT NULL,
  `CUST_INTRODUCE` varchar(256) DEFAULT NULL,
  `CUST_STATE` varchar(32) DEFAULT NULL,
  `CUST_CREATE_ID` char(36) DEFAULT NULL,
  `CUST_CREATE_TIME` datetime DEFAULT NULL,
  `CUST_SUBMIT_ID` char(36) DEFAULT NULL,
  `CUST_SUBMIT_TIME` datetime DEFAULT NULL,
  `CUST_CONFIRM_ID` char(36) DEFAULT NULL,
  `CUST_CONFIRM_TIME` datetime DEFAULT NULL,
  `CUST_PROGRESS_STATE` varchar(32) DEFAULT NULL,
  `CUST_LEVEL` varchar(32) DEFAULT NULL,
  `CUST_COMPANY_WEB` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_info
-- ----------------------------

INSERT INTO `crm_customer_info` VALUES ('640C23D1-F820-49B3-A563-3B36DF259B1F', 'test', '0', '123', null, '123', null, null, '123', 'init', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '2016-04-28 20:36:00', null, null, null, null, 'BUSINESS_NEGOTIATION', 'NON_PRIORITY', '123');

-- ----------------------------
-- Table structure for crm_customer_sales_rel
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_sales_rel`;
CREATE TABLE `crm_customer_sales_rel` (
  `CUST_ID` char(36) DEFAULT NULL,
  `USER_ID` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_sales_rel
-- ----------------------------
INSERT INTO `crm_customer_sales_rel` VALUES ('0989EE6F-0AF5-4DC5-81CB-9EBEE07A7919', '439D83A6-148E-483E-BF6C-9EF131C27ACF');
INSERT INTO `crm_customer_sales_rel` VALUES ('0989EE6F-0AF5-4DC5-81CB-9EBEE07A7919', 'B587A80F-1863-434C-999F-A92C240BD314');

-- ----------------------------
-- Table structure for crm_opp_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_opp_info`;
CREATE TABLE `crm_opp_info` (
  `OPP_ID` char(36) NOT NULL,
  `OPP_NAME` varchar(32) DEFAULT NULL,
  `CLUE_ID` char(36) DEFAULT NULL,
  `CUST_ID` char(36) DEFAULT NULL,
  `CONT_ID` char(36) DEFAULT NULL,
  `OPP_DES` varchar(256) DEFAULT NULL,
  `OPP_CONCERN_PRODUCT` varchar(32) DEFAULT NULL,
  `OPP_EXPECT_INVEST` decimal(10,2) DEFAULT NULL,
  `OPP_START_TIME` varchar(32) DEFAULT NULL,
  `OPP_STATE` varchar(32) DEFAULT NULL,
  `CLUE_SALESMAN` char(36) DEFAULT NULL,
  `OPP_CREATER` char(36) DEFAULT NULL,
  `OPP_CREATE_TIME` varchar(32) DEFAULT NULL,
  `OPP_LEVEL` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OPP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_opp_info
-- ----------------------------
INSERT INTO `crm_opp_info` VALUES ('E4BD6A6E-7AE7-4C9C-B40F-E6C93084CD4B', '测试商机', null, 'F6E4A2F6-05C4-4C5D-A0E2-E20270604A12', null, '123', 'crm', '123.10', null, '2', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '2016-04-18 13:58:38', 'STRONG');

-- ----------------------------
-- Table structure for crm_order_entry
-- ----------------------------
DROP TABLE IF EXISTS `crm_order_entry`;
CREATE TABLE `crm_order_entry` (
  `ENTRY_ID` char(36) NOT NULL,
  `ORDER_ID` char(36) DEFAULT NULL,
  `ENTRY_ORDER_PRODUCT` varchar(32) DEFAULT NULL,
  `ENTRY_PRODUCT_MODEL` varchar(32) DEFAULT NULL,
  `ENTRY_NUMBER` decimal(6,0) DEFAULT NULL,
  `ENTRY_UNIT_PRICE` decimal(10,2) DEFAULT NULL,
  `ENTRY_DISCOUNT` decimal(3,2) DEFAULT NULL,
  `ENTRY_REAL_PRICE` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_order_entry
-- ----------------------------
INSERT INTO `crm_order_entry` VALUES ('CBACBBC6-6AE9-42FD-8F48-C0EE9E974FEC', '641693C4-89A0-4BB6-B447-36E9E1261A3F', 'aeaiportal', 'aeaiportal', '1', '50000.00', '1.00', '50000.00');

-- ----------------------------
-- Table structure for crm_order_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_order_info`;
CREATE TABLE `crm_order_info` (
  `ORDER_ID` char(36) NOT NULL,
  `OPP_ID` char(36) DEFAULT NULL,
  `ORDER_CHIEF` varchar(32) DEFAULT NULL,
  `ORDER_DELIVERY_COST` decimal(10,2) DEFAULT NULL,
  `ORDER_COST` decimal(10,2) DEFAULT NULL,
  `ORDER_DES` varchar(200) DEFAULT NULL,
  `ORDER_STATE` varchar(32) DEFAULT NULL,
  `ORDER_CREATER` char(36) DEFAULT NULL,
  `ORDER_CREATE_TIME` varchar(32) DEFAULT NULL,
  `ORDER_CONFIRM_PERSON` char(36) DEFAULT NULL,
  `ORDER_CONFIRM_TIME` varchar(32) DEFAULT NULL,
  `CLUE_SALESMAN` char(36) DEFAULT NULL,
  PRIMARY KEY (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_order_info
-- ----------------------------
INSERT INTO `crm_order_info` VALUES ('641693C4-89A0-4BB6-B447-36E9E1261A3F', '526BB92A-1C05-45E6-A521-07981DC867CD', '赵小二', '1000000.00', '50000.00', '测试', '0', 'B587A80F-1863-434C-999F-A92C240BD314', '2016-04-18 18:00', null, null, '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');

-- ----------------------------
-- Table structure for crm_org_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_org_info`;
CREATE TABLE `crm_org_info` (
  `ORG_ID` char(36) NOT NULL,
  `ORG_NAME` varchar(32) DEFAULT NULL,
  `ORG_TYPE` varchar(32) DEFAULT NULL,
  `ORG_INTRODUCTION` varchar(1024) DEFAULT NULL,
  `ORG_LINKMAN_NAME` varchar(32) DEFAULT NULL,
  `ORG_EMAIL` varchar(32) DEFAULT NULL,
  `ORG_CONTACT_WAY` varchar(256) DEFAULT NULL,
  `ORG_STATE` varchar(32) DEFAULT NULL,
  `ORG_LABELS` varchar(32) DEFAULT NULL,
  `ORG_ADDRESS` varchar(256) DEFAULT NULL,
  `ORG_WEBSITE` varchar(128) DEFAULT NULL,
  `ORG_CREATER` char(36) DEFAULT NULL,
  `ORG_CREATE_TIME` varchar(32) DEFAULT NULL,
  `ORG_UPDATE_TIME` varchar(32) DEFAULT NULL,
  `ORG_CLASSIFICATION` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ORG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_org_info
-- ----------------------------
INSERT INTO `crm_org_info` VALUES ('B15FA222-8201-44E5-9DB9-019EF6BA063E', '孙小三的组织客户', '', '', '', '', 'QQ邮箱微信', '0', '', '', '', '439D83A6-148E-483E-BF6C-9EF131C27ACF', '2015-05-29 13:47', null, null);

-- ----------------------------
-- Table structure for crm_per_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_per_info`;
CREATE TABLE `crm_per_info` (
  `PER_ID` char(36) NOT NULL,
  `PER_NAME` varchar(32) DEFAULT NULL,
  `PER_SEX` varchar(10) DEFAULT NULL,
  `PER_EMAIL` varchar(32) DEFAULT NULL,
  `PER_CONTACT_WAY` varchar(256) DEFAULT NULL,
  `PER_STATE` varchar(32) DEFAULT NULL,
  `PER_LABELS` varchar(32) DEFAULT NULL,
  `PER_BELONG_ORG` varchar(32) DEFAULT NULL,
  `PER_CREATER` char(36) DEFAULT NULL,
  `PER_CREATE_TIME` varchar(32) DEFAULT NULL,
  `PER_UPDATE_TIME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`PER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_per_info
-- ----------------------------
INSERT INTO `crm_per_info` VALUES ('4295FF51-7772-4FCB-8BDA-04FFA638C3F1', '孙小三的潜在客户', 'UNKONW', '', 'QQ邮箱微信', '0', '', '', '439D83A6-148E-483E-BF6C-9EF131C27ACF', '2015-05-29 13:47', null);

-- ----------------------------
-- Table structure for crm_visit
-- ----------------------------
DROP TABLE IF EXISTS `crm_visit`;
CREATE TABLE `crm_visit` (
  `VISIT_ID` char(36) NOT NULL,
  `VISIT_TYPE` varchar(32) DEFAULT NULL,
  `VISIT_CUST_ID` char(36) DEFAULT NULL,
  `VISIT_RECEPTION_NAME` varchar(32) DEFAULT NULL,
  `VISIT_RECEPTION_SEX` varchar(32) DEFAULT NULL,
  `VISIT_RECEPTION_JOB` varchar(32) DEFAULT NULL,
  `VISIT_RECEPTION_PHONE` varchar(32) DEFAULT NULL,
  `VISIT_USER_ID` char(36) DEFAULT NULL,
  `VISIT_PEER_NAME` varchar(32) DEFAULT NULL,
  `VISIT_DATE` date DEFAULT NULL,
  `VISIT_CONTENT` varchar(256) DEFAULT NULL,
  `VISIT_CUST_FOCUS` varchar(32) DEFAULT NULL,
  `VISIT_EFFECT` varchar(32) DEFAULT NULL,
  `VISIT_IMPROVEMENT` varchar(32) DEFAULT NULL,
  `VISIT_COST` decimal(10,2) DEFAULT NULL,
  `VISIT_COST_EXPLAIN` varchar(256) DEFAULT NULL,
  `VISIT_STATE` varchar(32) DEFAULT NULL,
  `VISIT_FILL_ID` char(36) DEFAULT NULL,
  `VISIT_FILL_TIME` datetime DEFAULT NULL,
  `VISIT_CONFIRM_ID` char(36) DEFAULT NULL,
  `VISIT_CONFIRM_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`VISIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_visit
-- ----------------------------






-- ----------------------------
-- Table structure for security_group
-- ----------------------------
DROP TABLE IF EXISTS `security_group`;
CREATE TABLE `security_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_CODE` varchar(32) DEFAULT NULL,
  `GRP_NAME` varchar(32) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  `GRP_STATE` varchar(1) DEFAULT NULL,
  `GRP_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group
-- ----------------------------
INSERT INTO `security_group` VALUES ('00000000-0000-0000-00000000000000000', 'Root', '公司集团', null, null, '1', '0');
INSERT INTO `security_group` VALUES ('0BE47DDB-862E-45F3-8E43-0A4990847D8B', 'ITT', 'IT部', '00000000-0000-0000-00000000000000000', '', '1', '3');
INSERT INTO `security_group` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'IT', '信息部', '00000000-0000-0000-00000000000000000', '', '1', '2');

-- ----------------------------
-- Table structure for security_group_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_group_auth`;
CREATE TABLE `security_group_auth` (
  `GRP_AUTH_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GRP_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group_auth
-- ----------------------------
INSERT INTO `security_group_auth` VALUES ('4C20EDAC-0A0B-45BB-8B71-B557A94E7429', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'Navigater', '02');

-- ----------------------------
-- Table structure for security_role
-- ----------------------------
DROP TABLE IF EXISTS `security_role`;
CREATE TABLE `security_role` (
  `ROLE_ID` varchar(36) NOT NULL,
  `ROLE_CODE` varchar(32) DEFAULT NULL,
  `ROLE_NAME` varchar(32) DEFAULT NULL,
  `ROLE_PID` varchar(36) DEFAULT NULL,
  `ROLE_DESC` varchar(128) DEFAULT NULL,
  `ROLE_STATE` varchar(32) DEFAULT NULL,
  `ROLE_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role
-- ----------------------------
INSERT INTO `security_role` VALUES ('00000000-0000-0000-00000000000000000', 'System', '系统角色', null, null, '1', null);
INSERT INTO `security_role` VALUES ('0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'TEL_NET_SALESMAN', '电销网销', '00000000-0000-0000-00000000000000000', '', '1', '5');
INSERT INTO `security_role` VALUES ('6A79C287-E013-4A92-AAB2-B926F38144BB', 'INFO_COLLECTOR', '信息采集', '00000000-0000-0000-00000000000000000', '', '1', '4');
INSERT INTO `security_role` VALUES ('8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'SALESMAN', '销售人员', '00000000-0000-0000-00000000000000000', '', '1', '6');
INSERT INTO `security_role` VALUES ('DDA88E11-231C-43EC-9657-7AA55D842407', 'SALES_DIRECTOR', '销售总监', '00000000-0000-0000-00000000000000000', '', '1', '7');

-- ----------------------------
-- Table structure for security_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_role_auth`;
CREATE TABLE `security_role_auth` (
  `ROLE_AUTH_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ROLE_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_auth
-- ----------------------------
INSERT INTO `security_role_auth` VALUES ('0189942A-B628-4C3A-8BAE-433D054ED4A4', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'AD843A54-9F8B-4F34-A5E3-6C2BF0537381');
INSERT INTO `security_role_auth` VALUES ('0361DDF2-0F68-4326-8D15-89B79D754A91', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '093413BA-1ABC-464F-AEA0-13FF82591D22');
INSERT INTO `security_role_auth` VALUES ('036AA113-83F6-411D-8CEC-1D4BEDEDEDC7', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '9E812AE0-CFE4-4891-B7BF-22A22B643543');
INSERT INTO `security_role_auth` VALUES ('0578EB32-4232-4DBE-AD95-ECBC27B5F28B', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '1BBAA470-C306-4915-9ECC-EBCF72EDC38D');
INSERT INTO `security_role_auth` VALUES ('06A46DA8-CA9C-41E5-807A-D28E18A0BFA5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'C5D919FA-A812-4AB2-AED5-81BE5B31B79D');
INSERT INTO `security_role_auth` VALUES ('0A01E0D0-4DCC-4E7A-8C61-7F96EC5F928F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '9F4DFA03-9114-4268-87C6-26B8EE3606D3');
INSERT INTO `security_role_auth` VALUES ('0C839172-5308-46FC-9ED6-B6F75A1C5080', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'EC67A415-CC8F-45B5-A28D-797D84435841');
INSERT INTO `security_role_auth` VALUES ('0CAB8F08-B627-4BD7-856E-3EE5B00762A2', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('0CC962DF-F7AF-4AA6-AFC5-244BB811B33A', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '85705567-2B39-4661-8995-4013A7E80E76');
INSERT INTO `security_role_auth` VALUES ('0E854BF9-3B4F-4B95-83EC-173DFABE3E6E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '37FEF4ED-0F0A-404F-9107-2329D4440A48');
INSERT INTO `security_role_auth` VALUES ('0EE82AE7-6FA1-40DA-885C-2B7EF5D0534C', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'E6CFF307-AD95-42FA-8234-C551BDB7BFA1');
INSERT INTO `security_role_auth` VALUES ('0F1E1CE6-2930-4D4C-B181-69201865F02B', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'AEA07454-68B1-453D-ABC3-C6CD10026CFA');
INSERT INTO `security_role_auth` VALUES ('0FD821A0-00C2-4376-A8D1-E8B47ADDD25F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'AEEDFDAB-7802-492D-B369-2DEE350208B5');
INSERT INTO `security_role_auth` VALUES ('10877B4A-AFB3-4FB2-AC3A-455039F19C48', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '495A111E-EE16-452E-92D9-56CF97BD6388');
INSERT INTO `security_role_auth` VALUES ('113B0A4A-17FD-473C-BD15-D563268C2E9F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '85705567-2B39-4661-8995-4013A7E80E76');
INSERT INTO `security_role_auth` VALUES ('1186C4CA-3F2A-47E2-B9CD-34848E180BF3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '1FEEF11B-605F-432E-83BE-AA2173C0EC59');
INSERT INTO `security_role_auth` VALUES ('12C18802-7C8E-4028-99D4-F0142874D056', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '452593FA-A9D6-4F77-B880-21F97E9688D1');
INSERT INTO `security_role_auth` VALUES ('12D913FD-D7FD-495E-A540-632314E49063', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'AEEDFDAB-7802-492D-B369-2DEE350208B5');
INSERT INTO `security_role_auth` VALUES ('15619EBE-0BB9-4EC5-AB78-C64C3B9D5EF1', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '02E89343-F996-44EC-8347-6FB3C60BC9BB');
INSERT INTO `security_role_auth` VALUES ('165846DF-C481-457C-92AD-729F25B58C8D', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '4E2CAF22-3DCB-415D-ABC5-0E6BC2B50BFE');
INSERT INTO `security_role_auth` VALUES ('16F8CD76-FA66-42DF-AB32-E3A7F83BEB6D', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '579FCABC-B5D5-4157-91DD-D638943C3BCF');
INSERT INTO `security_role_auth` VALUES ('174C4AA2-0967-4C91-9BF5-B1DE91EE5A19', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `security_role_auth` VALUES ('17769E2A-A907-44F2-B42A-44C7BEF1D870', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('185D8307-6A9E-4C78-AA19-879865E31BC6', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FF12FE7A-BD11-448A-BCC9-E0D23FAA33A6');


INSERT INTO `security_role_auth` VALUES ('1B7256BC-B623-4228-A6C7-EBD03CBF42D3', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7');
INSERT INTO `security_role_auth` VALUES ('1B9AB8FD-5747-49AE-834B-AC5B3D58A22E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('1DD59D29-2172-4701-90F8-19C629B1646E', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '0831D3B9-7BD5-4581-A540-72982B899171');
INSERT INTO `security_role_auth` VALUES ('1DF5DF63-79E3-401B-A993-30EB335BF1B1', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '52C7239C-39B8-4CCC-94A9-1A2BFD7B48A0');
INSERT INTO `security_role_auth` VALUES ('1E827FE9-C6C2-43A7-8197-C71AEA443FA0', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '2321E4A1-2CB5-430C-8841-5129D02D2C38');
INSERT INTO `security_role_auth` VALUES ('1E93950A-FF7E-428B-905A-26EF19591B35', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'FEE4BDAE-A1ED-47E1-9065-22285EB8F57B');

INSERT INTO `security_role_auth` VALUES ('1FF5DC1D-34A2-4259-9915-944248B4CEBE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '00042A5A-FF5C-4EF2-89BB-5A6CB2D7EDA6');

INSERT INTO `security_role_auth` VALUES ('20DFEE49-FF2D-47CD-9A1B-7F9E1F6F7D2F', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '37FEF4ED-0F0A-404F-9107-2329D4440A48');
INSERT INTO `security_role_auth` VALUES ('21002270-8007-4D16-9716-AAB03631E200', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');
INSERT INTO `security_role_auth` VALUES ('227424B1-B4D4-4E76-B2E3-884802C3E908', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '6A6591AB-3717-4F50-A609-18B332413E86');
INSERT INTO `security_role_auth` VALUES ('25215598-B0A3-4263-977E-11DF52F13E77', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1BF7D254-304B-4719-BCB6-AB38BF8CA4E7');
INSERT INTO `security_role_auth` VALUES ('273B2506-5A40-4F6A-8941-3C259DBF6113', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FEE4BDAE-A1ED-47E1-9065-22285EB8F57B');
INSERT INTO `security_role_auth` VALUES ('27A3CD98-ED50-4212-9E6D-FF2572E516F0', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1BBAA470-C306-4915-9ECC-EBCF72EDC38D');

INSERT INTO `security_role_auth` VALUES ('28916250-722B-4B23-AB62-681EC9CE0027', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('2A245C6F-F5ED-40F3-A487-DDC52DC96815', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');

INSERT INTO `security_role_auth` VALUES ('2A6843F6-E2A4-4D3C-AC8A-7342B936A86E', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '02E89343-F996-44EC-8347-6FB3C60BC9BB');
INSERT INTO `security_role_auth` VALUES ('2AD16514-3C1F-4DFD-BB24-C2D8EBCD734F', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '3C03348B-D91C-4A35-B540-7BB67A153463');
INSERT INTO `security_role_auth` VALUES ('2AD28B8F-C321-42BC-A6B7-443258A33996', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '496E22A5-67BC-40C9-9F99-6FDB6193B441');



INSERT INTO `security_role_auth` VALUES ('2BE721EA-A7F8-4395-A153-5AA0443E315B', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '3B39B662-671E-4ECF-B14D-AE8F4FDC335D');
INSERT INTO `security_role_auth` VALUES ('2E240D78-60A4-48BB-8631-F6532E4F7CCE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '82384F1D-4CD1-4DF3-9121-B80EE0B15973');
INSERT INTO `security_role_auth` VALUES ('30A683EE-104A-49C9-A402-1D8C5A51C041', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '17FB686F-8580-4995-8494-EC42F9C71FC0');
INSERT INTO `security_role_auth` VALUES ('310AA2E8-9C66-4C2D-8CA7-97310E02A120', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '0831D3B9-7BD5-4581-A540-72982B899171');
INSERT INTO `security_role_auth` VALUES ('3193EF52-E1DB-419C-BC24-5E5CB98D3CFB', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'AD843A54-9F8B-4F34-A5E3-6C2BF0537381');
INSERT INTO `security_role_auth` VALUES ('3597DE6B-D190-47F3-AC0D-236709330946', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '6224A19C-0FC4-439F-B5C8-79E86557B29E');
INSERT INTO `security_role_auth` VALUES ('36363324-DBFB-4439-AA39-CB7F06A089C6', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('378C2F9C-07F6-49FB-B390-35288E21EF16', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'E9F11BB9-A6CC-47AB-B475-B7B54D898C8D');
INSERT INTO `security_role_auth` VALUES ('3886F0FB-6D69-4687-BCCE-1FA421A1844D', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '910D3B2D-0019-4FAD-8593-43710B416A3B');


INSERT INTO `security_role_auth` VALUES ('3B187E11-5194-4F57-9987-C400DBE62966', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'D28C6156-32D2-44C4-90D4-63DC05CD6407');
INSERT INTO `security_role_auth` VALUES ('3CB5CDB1-7405-4D7C-BDFE-A8F7E1DFBA6A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '2B49A370-5B78-4FFB-8792-1BB61DBA9478');
INSERT INTO `security_role_auth` VALUES ('3D5CA9B6-A6A9-41DE-AC26-CE9FA601986B', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57');
INSERT INTO `security_role_auth` VALUES ('3F1D25DB-A95C-471E-8763-B60F58E50C70', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('408932A2-1089-4E26-83E0-06A998884CE9', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '00000000-0000-0000-00000000000000001');


INSERT INTO `security_role_auth` VALUES ('44D70975-8A7E-4D7B-A601-DBB18530CBD0', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');

INSERT INTO `security_role_auth` VALUES ('46301688-59EF-454F-9BBB-130CC1FFC1BE', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '00042A5A-FF5C-4EF2-89BB-5A6CB2D7EDA6');
INSERT INTO `security_role_auth` VALUES ('4687C8BA-D9E0-4504-8E1F-9320D0DF159E', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22');
INSERT INTO `security_role_auth` VALUES ('4751932E-04BF-4B9A-926A-B83FCDD9D4CB', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '495A111E-EE16-452E-92D9-56CF97BD6388');
INSERT INTO `security_role_auth` VALUES ('47EC6ADC-0889-4951-9CAB-D2A3CD87F046', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '4E2CAF22-3DCB-415D-ABC5-0E6BC2B50BFE');

INSERT INTO `security_role_auth` VALUES ('4BC82B5F-4C09-4CF6-8E5B-AFD3184503CE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'D413878A-2092-40D8-B946-1B6F90D2F194');
INSERT INTO `security_role_auth` VALUES ('4E0294C3-FAFE-42CD-8503-5EBFF26BF1E2', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '91E10CCF-A520-4712-A82C-ADCB8CFA0921');
INSERT INTO `security_role_auth` VALUES ('4F26A747-B9C9-4871-8253-8B352DFC3BD2', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '3B39B662-671E-4ECF-B14D-AE8F4FDC335D');
INSERT INTO `security_role_auth` VALUES ('4F275F7B-AB45-493D-8E74-37D516126EC9', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '2BF95B4C-B823-402B-AB8C-284CE7EDE678');
INSERT INTO `security_role_auth` VALUES ('50AE623E-732A-4598-9E8B-ED69DF93CC8F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'C8BEFAFB-0E45-431A-BCBC-1AB2819BCA91');
INSERT INTO `security_role_auth` VALUES ('50F035D3-E612-426B-A5D8-D8476CF423F7', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7');
INSERT INTO `security_role_auth` VALUES ('51230009-0695-4129-A681-026050CE62B3', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('52EB63BC-B124-4CC9-A691-881293B23000', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('541DCB64-413F-4F0A-BD46-6684A7F5E1BD', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '567FBEE1-306B-4EC1-8688-906D5629C3D4');
INSERT INTO `security_role_auth` VALUES ('54680DD8-D52B-49E9-839C-BEF6E023C1E5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('5503EB23-3717-402C-90C5-C0D7279EB8AC', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '2B49A370-5B78-4FFB-8792-1BB61DBA9478');
INSERT INTO `security_role_auth` VALUES ('55376C93-13F1-442F-B779-2236AAA44D21', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'A4F18B90-AEBA-4CE1-AA52-B534C4F79CA2');
INSERT INTO `security_role_auth` VALUES ('5584F70A-5CB4-469A-A458-94DE62A53B75', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A80074A6-7BA6-479D-9CDD-768983177E06');
INSERT INTO `security_role_auth` VALUES ('55C51690-0435-4AB9-9ABA-8F4D9FB1BE37', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'F4C8FD10-7ED1-4912-B7A5-92DFA7070159');
INSERT INTO `security_role_auth` VALUES ('56A2E93A-A03B-459D-981B-CBE35EC4D5C9', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '9AA13920-43B2-4AEF-9D9C-9C2F71F34043');
INSERT INTO `security_role_auth` VALUES ('5801EF12-9EB7-49DE-ADB6-BB451AB779B1', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '5524CB68-469B-4163-99EF-1EDDE8E13AFD');
INSERT INTO `security_role_auth` VALUES ('5948DF31-C313-4CE3-8225-C4F3B8D8FC93', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'FFDB914C-B3F6-4871-8AEE-515A1081ACF0');
INSERT INTO `security_role_auth` VALUES ('594A7D4F-1769-4193-8A0A-A922434D7B14', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('59CF51CB-6560-496B-8933-B60CDD9C10D6', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '5524CB68-469B-4163-99EF-1EDDE8E13AFD');
INSERT INTO `security_role_auth` VALUES ('5A2B639F-6F9F-44E1-9764-C97E40BBB119', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FCD061B9-45BC-4E7A-86CB-B179E30B0A76');
INSERT INTO `security_role_auth` VALUES ('5A354008-952A-48B4-BBD7-32B3ED3C4DC4', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '91E10CCF-A520-4712-A82C-ADCB8CFA0921');
INSERT INTO `security_role_auth` VALUES ('5D0D7824-5579-4AAF-B027-B40514A487A5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'CBD4685F-AC93-4574-95CB-BFF7E900BD2E');
INSERT INTO `security_role_auth` VALUES ('5D802ADD-DEE5-43CF-BB91-3075FE88219D', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('5EE7FFE7-7157-4DA6-BAFD-05146A2F8E97', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1F173085-5E15-4E9A-8F06-922E3E92BE95');
INSERT INTO `security_role_auth` VALUES ('5F6919CD-F40E-458E-ABD5-03231A11E73E', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8');

INSERT INTO `security_role_auth` VALUES ('60BB37EE-7853-42F0-87EB-D7AD48833316', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '1FEEF11B-605F-432E-83BE-AA2173C0EC59');
INSERT INTO `security_role_auth` VALUES ('60FAB83C-BC75-483A-87CF-87F4E603A864', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '863E15B1-7DF1-4A73-8F19-1F9CD0B7EFAB');
INSERT INTO `security_role_auth` VALUES ('621669BD-BEB8-4293-900D-CC8E3D387F35', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'E723E562-9B05-4778-8850-4DF9518B8DD4');
INSERT INTO `security_role_auth` VALUES ('6354DBD0-9763-496C-B32E-0FE8CB05EA06', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('6387EBD7-D350-40F1-9A59-7F1D9C1AEEC8', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');

INSERT INTO `security_role_auth` VALUES ('67B01527-1687-45A4-BA09-CB81DB34A667', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('68CD1CD1-CEB0-41C0-9AC0-6578BEAB6334', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '894F3E00-FF69-494E-A072-5D63D185097E');

INSERT INTO `security_role_auth` VALUES ('69F6CDEA-417B-4793-81AC-AA454BEE6405', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '52C7239C-39B8-4CCC-94A9-1A2BFD7B48A0');
INSERT INTO `security_role_auth` VALUES ('6D1B721A-E602-4673-B384-6DF91713EFB7', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1F173085-5E15-4E9A-8F06-922E3E92BE95');
INSERT INTO `security_role_auth` VALUES ('6D245581-031F-4AB7-B736-00096D2810EA', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '199DCC87-DB0D-4F78-A0AE-CCE348B13516');
INSERT INTO `security_role_auth` VALUES ('6D4E0A9E-7C44-4168-9D13-4F15DE7022A2', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'B8022643-0966-4493-84BA-23A77B09AD2C');
INSERT INTO `security_role_auth` VALUES ('6D81D9FA-5609-4A70-B778-2879BEE3C3F8', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1720253F-2F2E-403B-9D24-B96287063081');
INSERT INTO `security_role_auth` VALUES ('727CCB0C-E0F1-41E5-8F4A-935EB4F00774', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1BF7D254-304B-4719-BCB6-AB38BF8CA4E7');
INSERT INTO `security_role_auth` VALUES ('73493AEF-F6C1-49D5-857A-14AFD54BC9D5', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'E723E562-9B05-4778-8850-4DF9518B8DD4');

INSERT INTO `security_role_auth` VALUES ('7493C7F8-3F86-4E6E-AA0E-DEE3FBEC9D7A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'E9F11BB9-A6CC-47AB-B475-B7B54D898C8D');
INSERT INTO `security_role_auth` VALUES ('769DF6CD-CE14-4D31-B70A-0370EA1BF63A', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '3C03348B-D91C-4A35-B540-7BB67A153463');
INSERT INTO `security_role_auth` VALUES ('77680B1E-380E-4E62-B263-DB44ECAC2675', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');
INSERT INTO `security_role_auth` VALUES ('779B8D8E-9897-4F94-A762-3D28F85F8BD0', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('7A1992FC-BD9D-4C9A-A22D-8338655334F5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A3D11759-6C21-4B36-9BA2-2CE232C39085');
INSERT INTO `security_role_auth` VALUES ('7B9C4D1D-C802-4F30-97E8-35A8764BEE1E', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A4F18B90-AEBA-4CE1-AA52-B534C4F79CA2');
INSERT INTO `security_role_auth` VALUES ('7BFE76F2-4098-4505-95AB-A9A2AA30E2DF', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '308FB9DB-AA23-4E5A-9D60-2D3BC0B28D7C');
INSERT INTO `security_role_auth` VALUES ('7CDC8203-CB66-4516-AD71-BA39C8E75FD7', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '3F844D55-E2AA-43C0-B795-88B5DE2440CF');
INSERT INTO `security_role_auth` VALUES ('7DA9B695-BD2E-489D-870E-E376B0D2C5D5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22');
INSERT INTO `security_role_auth` VALUES ('7DB3F4AE-E42C-46AA-8652-E0F4A8BB895B', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '8A2C52B7-7F4C-415D-8C46-48AAD36D01D6');
INSERT INTO `security_role_auth` VALUES ('801D8A7E-21C0-4DA0-A630-F5BF42E3F0BD', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'A3D11759-6C21-4B36-9BA2-2CE232C39085');
INSERT INTO `security_role_auth` VALUES ('821E8871-3C44-4CAB-AB78-3E176FB2FF46', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'E6CFF307-AD95-42FA-8234-C551BDB7BFA1');
INSERT INTO `security_role_auth` VALUES ('822DB257-EB1B-4EDC-BA47-E9414261CD93', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'CBD4685F-AC93-4574-95CB-BFF7E900BD2E');
INSERT INTO `security_role_auth` VALUES ('8237F411-686A-4BEC-98B2-FF1EC4B9FA34', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'D413878A-2092-40D8-B946-1B6F90D2F194');
INSERT INTO `security_role_auth` VALUES ('825059F0-BBD0-4706-A43C-1B0B0F67D376', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '82384F1D-4CD1-4DF3-9121-B80EE0B15973');
INSERT INTO `security_role_auth` VALUES ('82C6412C-D275-4BCE-844A-DD3BEF738ECE', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '28DFDD64-D05F-4D87-A997-11F29FA27042');

INSERT INTO `security_role_auth` VALUES ('85BEC33E-4A68-4B4A-9DA7-19248667FB4A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '3F844D55-E2AA-43C0-B795-88B5DE2440CF');
INSERT INTO `security_role_auth` VALUES ('87021316-B1B8-429B-A323-5012B685AC92', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('8875732E-86C0-41A2-BD3D-BF02E677296C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '01E6A560-0552-46BC-8A5F-0C2BEB987617');
INSERT INTO `security_role_auth` VALUES ('8954FA9E-E173-4F27-9C13-B7AC074F3D90', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '00042A5A-FF5C-4EF2-89BB-5A6CB2D7EDA6');
INSERT INTO `security_role_auth` VALUES ('897650B4-5F12-4180-99F3-8104A4A57560', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('89AB9B7E-C1E9-4F04-831B-772B784A6B84', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('8B5F57D1-1B73-4952-908F-A4AB935FFF8C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '4E2CAF22-3DCB-415D-ABC5-0E6BC2B50BFE');
INSERT INTO `security_role_auth` VALUES ('8BCC713E-5461-4027-B43E-015E1E1E33D5', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '01E6A560-0552-46BC-8A5F-0C2BEB987617');
INSERT INTO `security_role_auth` VALUES ('8C75CD55-33C0-4A6A-8FDD-8F4F232C999A', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'FCD061B9-45BC-4E7A-86CB-B179E30B0A76');
INSERT INTO `security_role_auth` VALUES ('90A12035-FFBE-42A3-A5E9-40096777A419', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('91F0E60A-B638-46EB-809B-5BCA9CAD6718', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '29596313-EBFD-4D56-A72D-16A91F6FA795');

INSERT INTO `security_role_auth` VALUES ('977AE16D-C9B8-495B-8592-5B50458E8F65', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1BBAA470-C306-4915-9ECC-EBCF72EDC38D');
INSERT INTO `security_role_auth` VALUES ('97D25C67-2427-4DDA-BA16-5B0690C7D46E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '90FCC64A-FFAD-4E8B-BBAE-EA0AE4C27CB9');
INSERT INTO `security_role_auth` VALUES ('9801A5B0-8FB9-42F3-B3C1-A7BC4FBA7E80', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('98867753-357B-477F-9C54-4D6F9FB6DF26', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '5EEFEF32-70DB-4763-857F-7935E418E570');
INSERT INTO `security_role_auth` VALUES ('9988A21F-A097-4C50-B99C-F78FCCC97208', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'EC67A415-CC8F-45B5-A28D-797D84435841');
INSERT INTO `security_role_auth` VALUES ('99E46F77-16A0-4D6F-AAA3-5EF2EB96B39F', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'CEA130F1-3BD5-47D9-84E5-2B2003D8F20B');
INSERT INTO `security_role_auth` VALUES ('9B268D2D-726B-4747-A02B-E108D05C4E67', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '894F3E00-FF69-494E-A072-5D63D185097E');

INSERT INTO `security_role_auth` VALUES ('A05F9B00-943D-4DD1-8F21-E410BF1D01DA', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('A07DCA35-5485-4832-9BD2-1D91B59BAD07', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'F4C8FD10-7ED1-4912-B7A5-92DFA7070159');
INSERT INTO `security_role_auth` VALUES ('A1854BAC-8B25-4F21-8B50-81B5968EA98A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '2BF95B4C-B823-402B-AB8C-284CE7EDE678');
INSERT INTO `security_role_auth` VALUES ('A3C0A870-A52D-4CA6-AF27-9799EC742E02', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');
INSERT INTO `security_role_auth` VALUES ('A3C3D8C6-FF38-4207-819C-94AAF2668AAD', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `security_role_auth` VALUES ('A5D89A39-294C-4FE1-AE35-285C8E688553', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '579FCABC-B5D5-4157-91DD-D638943C3BCF');
INSERT INTO `security_role_auth` VALUES ('A772121C-5BD0-42F1-BB55-C2BADADE2A7F', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'D28C6156-32D2-44C4-90D4-63DC05CD6407');


INSERT INTO `security_role_auth` VALUES ('A976AB64-36BD-45D6-95A6-10D2B4028AE4', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8');
INSERT INTO `security_role_auth` VALUES ('A97CD615-1987-4D5E-A835-26D26D1BD519', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'CC288486-F786-4815-8BFB-A6C1374A4BCB');

INSERT INTO `security_role_auth` VALUES ('AA1B2926-68A7-499B-A035-109832F8411F', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '02E89343-F996-44EC-8347-6FB3C60BC9BB');
INSERT INTO `security_role_auth` VALUES ('AD0C6CCD-FD07-4B99-A067-C10FD3F12A92', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');


INSERT INTO `security_role_auth` VALUES ('B1A475B6-F7C0-4079-9015-509E21780600', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'A3D11759-6C21-4B36-9BA2-2CE232C39085');
INSERT INTO `security_role_auth` VALUES ('B369A769-D39A-42B6-8A08-9E36F801BADB', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '00000000-0000-0000-00000000000000000');

INSERT INTO `security_role_auth` VALUES ('B62EF221-3E30-4CA6-A4A9-72AC4045D9FF', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `security_role_auth` VALUES ('B763774A-2D25-46E4-BCA7-79161665DCBF', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');
INSERT INTO `security_role_auth` VALUES ('B7EDEFEF-7DF1-46DA-93A8-562A3277C4C8', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57');
INSERT INTO `security_role_auth` VALUES ('B8053BCF-C268-41E7-9C2E-9885ADE35017', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '2321E4A1-2CB5-430C-8841-5129D02D2C38');
INSERT INTO `security_role_auth` VALUES ('B83B96EC-B0D1-4900-B162-10381FEA86DC', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'FEE4BDAE-A1ED-47E1-9065-22285EB8F57B');
INSERT INTO `security_role_auth` VALUES ('B989A14F-6766-4F94-AC99-490F5FEBBA4A', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'C5D919FA-A812-4AB2-AED5-81BE5B31B79D');
INSERT INTO `security_role_auth` VALUES ('BA619135-BC6F-4F61-BF89-CDE0B3FE5ABA', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'CEA130F1-3BD5-47D9-84E5-2B2003D8F20B');
INSERT INTO `security_role_auth` VALUES ('BA7101C5-319C-4B75-A50D-3BD2B99AD1B7', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');
INSERT INTO `security_role_auth` VALUES ('BB2750B7-71F8-434E-BFFA-0E2B3D2D8E94', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '88F99F91-384F-44B1-9EC8-E05F6724A9E8');
INSERT INTO `security_role_auth` VALUES ('BC9603EA-5A97-4B60-A706-0C4B11857637', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '33CF8E06-1181-453B-ADF4-1F18ABC026CF');
INSERT INTO `security_role_auth` VALUES ('BCDE4A5D-0DBA-4BE4-8352-7238459D9B3F', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'AEA07454-68B1-453D-ABC3-C6CD10026CFA');
INSERT INTO `security_role_auth` VALUES ('BD537B9B-DAD1-4EC2-BB1F-909E6943B384', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'FCD061B9-45BC-4E7A-86CB-B179E30B0A76');
INSERT INTO `security_role_auth` VALUES ('BD84B0E1-B953-446A-8521-31D8B5F7FC7C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');

INSERT INTO `security_role_auth` VALUES ('BF162BA2-5014-4AAB-8CFB-6693F67F5884', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('BFEE4DC3-8B5A-4B5E-ACAD-79F70D3D625A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'B8022643-0966-4493-84BA-23A77B09AD2C');
INSERT INTO `security_role_auth` VALUES ('C05C3CCE-4761-4240-AB73-FDA62FB62635', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('C0D4842D-416B-4600-8B5D-66E4B81B57D0', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '6224A19C-0FC4-439F-B5C8-79E86557B29E');
INSERT INTO `security_role_auth` VALUES ('C0F2FFD6-457E-42C1-AE6E-E048B7217313', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '24A26858-4628-472E-810B-964E817E7176');
INSERT INTO `security_role_auth` VALUES ('C120597A-7B2F-4F72-AFC0-64CD76A936DA', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '567FBEE1-306B-4EC1-8688-906D5629C3D4');
INSERT INTO `security_role_auth` VALUES ('C38A5E1A-958D-45B7-9760-D8215833D566', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '199DCC87-DB0D-4F78-A0AE-CCE348B13516');
INSERT INTO `security_role_auth` VALUES ('C4C478E6-9875-4972-B3D4-5A2F6D700668', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('C541BFCC-6AA5-4458-8124-522A0BEFDBD8', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '6224A19C-0FC4-439F-B5C8-79E86557B29E');
INSERT INTO `security_role_auth` VALUES ('CBB53DAF-FF3E-4DC2-BE7A-842EBF9E2434', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');
INSERT INTO `security_role_auth` VALUES ('CCDE498C-E22F-4E64-937D-F683C0750405', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '90FCC64A-FFAD-4E8B-BBAE-EA0AE4C27CB9');

INSERT INTO `security_role_auth` VALUES ('CD45E39E-9E15-4A00-9549-7348C177A3A9', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '24A26858-4628-472E-810B-964E817E7176');
INSERT INTO `security_role_auth` VALUES ('CD4CC7B6-F0E5-40CE-884D-33A4A4B0A747', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '4A64B8DC-7370-435D-8F10-612EC45FFCD8');
INSERT INTO `security_role_auth` VALUES ('CE5EF8D9-55A8-4605-B5B4-3A2D810566DD', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '910D3B2D-0019-4FAD-8593-43710B416A3B');
INSERT INTO `security_role_auth` VALUES ('CEC6A18A-D3E1-498A-AF07-BAADD5F40887', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('CF424F3D-810E-4655-996B-4EA23E686820', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '2321E4A1-2CB5-430C-8841-5129D02D2C38');

INSERT INTO `security_role_auth` VALUES ('D27AF497-A948-46F8-8BDF-BDC79CA323E3', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'AEA07454-68B1-453D-ABC3-C6CD10026CFA');
INSERT INTO `security_role_auth` VALUES ('D2C4AC31-5B32-418B-B388-498C3C77AAFE', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('D424F346-9D89-429E-8E61-3D765D76E696', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('D44C64FC-F54A-4850-95EE-1ECE7CE4EF9C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '7683F231-6534-4ADA-863E-F7637017351A');
INSERT INTO `security_role_auth` VALUES ('D568A397-933B-4A0A-81BD-120354A50D50', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('D58F2BB0-C709-421C-99DB-30C393AD1C54', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('D6A60A78-6118-476F-9E84-98DF72D4C606', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344');
INSERT INTO `security_role_auth` VALUES ('D701C5AE-167F-40D2-8D93-37A1E2CD6EFF', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '31F4B92B-FEB0-46FC-828E-ECB572AEC860');
INSERT INTO `security_role_auth` VALUES ('D879A022-E55D-4FDB-BF66-9CEC59D9EF3F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FFDB914C-B3F6-4871-8AEE-515A1081ACF0');
INSERT INTO `security_role_auth` VALUES ('D8DB1F27-9E48-4ABF-916E-DC1FDCC5E601', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '894F3E00-FF69-494E-A072-5D63D185097E');

INSERT INTO `security_role_auth` VALUES ('D9611240-7738-4035-AC4E-D9A46BB178D4', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '1BF7D254-304B-4719-BCB6-AB38BF8CA4E7');
INSERT INTO `security_role_auth` VALUES ('D98FE66E-7228-441F-96AF-7E25C5C74701', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'D28C6156-32D2-44C4-90D4-63DC05CD6407');
INSERT INTO `security_role_auth` VALUES ('D9ED2AFC-5FAD-4DF9-983E-C46D7DE0D464', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('DAC84D4F-D803-466D-9B10-5ADA9FDD8954', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '2BF95B4C-B823-402B-AB8C-284CE7EDE678');
INSERT INTO `security_role_auth` VALUES ('DB264284-D24E-4611-8DA7-55BAD19203F2', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '30E6EF99-55BD-4B4F-82E1-D2E4344D8ABD');
INSERT INTO `security_role_auth` VALUES ('DC4D13C8-CE94-4C9B-A9C7-71F7BB004A0D', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '863E15B1-7DF1-4A73-8F19-1F9CD0B7EFAB');
INSERT INTO `security_role_auth` VALUES ('DCF0BE92-775F-4D86-8292-A4AEE1691838', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22');
INSERT INTO `security_role_auth` VALUES ('DCF522CE-EA4F-4637-98DE-0F2983CB151D', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '093413BA-1ABC-464F-AEA0-13FF82591D22');
INSERT INTO `security_role_auth` VALUES ('DD1928B7-62C8-4D02-A8FA-63A16C873741', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '0831D3B9-7BD5-4581-A540-72982B899171');
INSERT INTO `security_role_auth` VALUES ('DE2DBD9F-7DE0-4C4C-853B-07A255779174', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '34D36130-6300-425B-8820-E05C1EF2A2E6');
INSERT INTO `security_role_auth` VALUES ('DF5B72E1-716B-43FE-A485-752D68CBEEFB', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '4AE7E5CB-848D-483C-A59A-BCD1117A7356');

INSERT INTO `security_role_auth` VALUES ('E0B4FCA8-2852-488C-B97A-F2E991CDD748', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'DCC185BC-660A-4172-A5D4-C2B40EE0833D');
INSERT INTO `security_role_auth` VALUES ('E10034CC-8211-4429-AA5F-C317104EE98E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('E195D9D7-C256-401A-B1B1-2006F2969054', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8');
INSERT INTO `security_role_auth` VALUES ('E1B8DB41-BFBE-40AD-97AC-1EA63CA41B41', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');

INSERT INTO `security_role_auth` VALUES ('E283AC11-BD0D-4207-BC62-CBC8DF24D75B', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'DCC185BC-660A-4172-A5D4-C2B40EE0833D');
INSERT INTO `security_role_auth` VALUES ('E2FC726D-A57F-4315-8371-AA3B41E588BE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('E30A4AAA-AA43-4C34-AE4A-9112C48C878C', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '567FBEE1-306B-4EC1-8688-906D5629C3D4');
INSERT INTO `security_role_auth` VALUES ('E38F916D-88AC-4423-B269-55F611DDCD76', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('E627ABF2-6D97-45E0-964E-1FDC6BD28FE3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('E6CE5472-85D8-4616-88E3-4E2661A187C5', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `security_role_auth` VALUES ('E6D496E3-B3B0-45D4-8526-F168723568B3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '29596313-EBFD-4D56-A72D-16A91F6FA795');

INSERT INTO `security_role_auth` VALUES ('E7E0F21C-EEDB-46B6-B795-22A427A08402', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('E90043CA-556D-4580-83E6-FB0A201009F6', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('E997E305-62DF-4362-BFBB-D36302782725', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '34D36130-6300-425B-8820-E05C1EF2A2E6');
INSERT INTO `security_role_auth` VALUES ('EA16DBC1-6FE9-48E3-A378-6A6878FF545F', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '199DCC87-DB0D-4F78-A0AE-CCE348B13516');
INSERT INTO `security_role_auth` VALUES ('EC76BE3A-76B6-46E0-958D-93EDCCC9F724', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '863E15B1-7DF1-4A73-8F19-1F9CD0B7EFAB');
INSERT INTO `security_role_auth` VALUES ('EC7A485B-EC7A-4481-9A6E-43A339CDDD36', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('ED9718D2-5F5B-4C96-8A87-703A259FE9BD', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('EE384CF9-BE58-4469-86BE-499069694903', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');

INSERT INTO `security_role_auth` VALUES ('EF6FF974-D228-4B56-9901-12B42E952412', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('F0DBFC70-EB77-486E-B5F1-D8AD29BCC6A1', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '5524CB68-469B-4163-99EF-1EDDE8E13AFD');
INSERT INTO `security_role_auth` VALUES ('F0E6FCAE-451C-45AD-ACA8-F748F97395D5', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '34D36130-6300-425B-8820-E05C1EF2A2E6');

INSERT INTO `security_role_auth` VALUES ('F2F831E8-5E1E-4F15-836A-891572927C21', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '6C24D134-9C82-4516-BBBD-1EDB00CBA951');


INSERT INTO `security_role_auth` VALUES ('F81CE2E1-247D-4A26-A3E9-D639A3EB26C3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '0A92F33C-5E91-4401-99C2-1FB41C8BE0B9');
INSERT INTO `security_role_auth` VALUES ('F8525553-DF6C-43CA-BA6E-D02B9DDDECEA', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('F98E6C7B-7D5C-4450-879B-22B8157B3FE6', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '9E812AE0-CFE4-4891-B7BF-22A22B643543');
INSERT INTO `security_role_auth` VALUES ('F9C50CAF-4D58-4E9B-96D1-DAEAFE61736D', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'AEEDFDAB-7802-492D-B369-2DEE350208B5');
INSERT INTO `security_role_auth` VALUES ('F9FE2568-93FE-455A-88FB-346CEF9265DD', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'DCC185BC-660A-4172-A5D4-C2B40EE0833D');
INSERT INTO `security_role_auth` VALUES ('FB1028FF-0992-4E6D-9C66-B85FF9572599', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '24A26858-4628-472E-810B-964E817E7176');
INSERT INTO `security_role_auth` VALUES ('FB2AC7A9-37C5-47C9-B32E-F0582873D179', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('FB90E01B-1503-458E-AF70-1A4EC8ED2823', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '01E6A560-0552-46BC-8A5F-0C2BEB987617');

INSERT INTO `security_role_auth` VALUES ('FD2CB60B-F4A0-424C-86FB-7900E757CFA2', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '4AE7E5CB-848D-483C-A59A-BCD1117A7356');
INSERT INTO `security_role_auth` VALUES ('FE4E7AFD-4DE2-40B9-8BD9-63F0924FBCF4', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('FECD25CF-2A22-46E6-B2D4-C6EFD33FB851', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344');

-- ----------------------------
-- Table structure for security_role_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_role_group_rel`;
CREATE TABLE `security_role_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`GRP_ID`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_group_rel
-- ----------------------------
INSERT INTO `security_role_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO `security_role_group_rel` VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO `security_role_group_rel` VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_group_rel` VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6');
INSERT INTO `security_role_group_rel` VALUES ('BBD420A2-68AE-49C2-B3D8-78DC166F511F', '00000000-0000-0000-00000000000000000');

-- ----------------------------
-- Table structure for security_user
-- ----------------------------
DROP TABLE IF EXISTS `security_user`;
CREATE TABLE `security_user` (
  `USER_ID` varchar(36) NOT NULL,
  `USER_CODE` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `USER_PWD` varchar(32) DEFAULT NULL,
  `USER_SEX` varchar(1) DEFAULT NULL,
  `USER_DESC` varchar(128) DEFAULT NULL,
  `USER_STATE` varchar(32) DEFAULT NULL,
  `USER_SORT` int(11) DEFAULT NULL,
  `USER_MAIL` varchar(64) DEFAULT NULL,
  `USER_PHONE` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user
-- ----------------------------
INSERT INTO `security_user` VALUES ('2CE11AC8-0E03-488F-BF7C-9568DFDD9C7A', 'CS04', '赵小四', 'FBAF749E6A085DCBBE41FF5C030EBF98', 'M', '', '1', '4', '', '');
INSERT INTO `security_user` VALUES ('439D83A6-148E-483E-BF6C-9EF131C27ACF', 'CS01', '张老大', 'DAE6CDC2B49F7C32164BE2AF1A7916AA', 'M', '', '1', '1', '', '');
INSERT INTO `security_user` VALUES ('542798C0-9D66-4DF2-B5B1-70D9323B954F', 'CS03', '孙小三', '25EC07853BDC7B87DD021F5580C70855', 'M', '', '1', '3', '', '');
INSERT INTO `security_user` VALUES ('7ACD7AD5-9A6F-4616-8122-A17D42288BE1', 'userr', 'user', 'EE11CBB19052E40B07AAC0CA060C23EE', 'M', '', '1', '1', '', '');
INSERT INTO `security_user` VALUES ('7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'admin', '管理员', '21232F297A57A5A743894A0E4A801FC3', 'M', '内置账户，勿删！！', '1', '1', null, null);
INSERT INTO `security_user` VALUES ('B587A80F-1863-434C-999F-A92C240BD314', 'CS02', '赵晓二', '39661AF3C6AFE19E95700A0E7373446A', 'F', '', '1', '2', '', '');

-- ----------------------------
-- Table structure for security_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_user_auth`;
CREATE TABLE `security_user_auth` (
  `USER_AUTH_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`USER_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_auth
-- ----------------------------

-- ----------------------------
-- Table structure for security_user_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_group_rel`;
CREATE TABLE `security_user_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`GRP_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_group_rel
-- ----------------------------
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '2CE11AC8-0E03-488F-BF7C-9568DFDD9C7A');
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '439D83A6-148E-483E-BF6C-9EF131C27ACF');
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '542798C0-9D66-4DF2-B5B1-70D9323B954F');
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', 'B587A80F-1863-434C-999F-A92C240BD314');
INSERT INTO `security_user_group_rel` VALUES ('0BE47DDB-862E-45F3-8E43-0A4990847D8B', '7ACD7AD5-9A6F-4616-8122-A17D42288BE1');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '7ACD7AD5-9A6F-4616-8122-A17D42288BE1');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '9A0A9DE7-608A-4B2B-B1C7-F6C11FD1A94E');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC');

-- ----------------------------
-- Table structure for security_user_role_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_role_rel`;
CREATE TABLE `security_user_role_rel` (
  `ROLE_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_role_rel
-- ----------------------------
INSERT INTO `security_user_role_rel` VALUES ('', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO `security_user_role_rel` VALUES ('00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO `security_user_role_rel` VALUES ('00000000-0000-0000-00000000000000000', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC');
INSERT INTO `security_user_role_rel` VALUES ('0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'B587A80F-1863-434C-999F-A92C240BD314');
INSERT INTO `security_user_role_rel` VALUES ('6A79C287-E013-4A92-AAB2-B926F38144BB', '2CE11AC8-0E03-488F-BF7C-9568DFDD9C7A');
INSERT INTO `security_user_role_rel` VALUES ('6A79C287-E013-4A92-AAB2-B926F38144BB', '439D83A6-148E-483E-BF6C-9EF131C27ACF');
INSERT INTO `security_user_role_rel` VALUES ('6A79C287-E013-4A92-AAB2-B926F38144BB', 'B587A80F-1863-434C-999F-A92C240BD314');

INSERT INTO `security_user_role_rel` VALUES ('8AAF1DCF-114D-49D4-B691-A46F77288AF0', '325F3A78-4671-485C-A9E9-D0581A635E77');
INSERT INTO `security_user_role_rel` VALUES ('8AAF1DCF-114D-49D4-B691-A46F77288AF0', '439D83A6-148E-483E-BF6C-9EF131C27ACF');
INSERT INTO `security_user_role_rel` VALUES ('8AAF1DCF-114D-49D4-B691-A46F77288AF0', '542798C0-9D66-4DF2-B5B1-70D9323B954F');
INSERT INTO `security_user_role_rel` VALUES ('8AAF1DCF-114D-49D4-B691-A46F77288AF0', '94AC9607-92D5-4FDD-A1FA-52799BB9EB1C');
INSERT INTO `security_user_role_rel` VALUES ('8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'B587A80F-1863-434C-999F-A92C240BD314');
INSERT INTO `security_user_role_rel` VALUES ('DDA88E11-231C-43EC-9657-7AA55D842407', '439D83A6-148E-483E-BF6C-9EF131C27ACF');
INSERT INTO `security_user_role_rel` VALUES ('DDA88E11-231C-43EC-9657-7AA55D842407', '94AC9607-92D5-4FDD-A1FA-52799BB9EB1C');

-- ----------------------------
-- Table structure for sys_codelist
-- ----------------------------
DROP TABLE IF EXISTS `sys_codelist`;
CREATE TABLE `sys_codelist` (
  `TYPE_ID` varchar(32) NOT NULL,
  `CODE_ID` varchar(32) NOT NULL,
  `CODE_NAME` varchar(32) DEFAULT NULL,
  `CODE_DESC` varchar(128) DEFAULT NULL,
  `CODE_SORT` int(11) DEFAULT NULL,
  `CODE_FLAG` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`,`CODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codelist
-- ----------------------------
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Bottom', 'BottomHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Building', 'BuildingHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Homepage', 'HomepageHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Logo', 'LogoHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MainWin', 'MainWinHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MenuTree', 'MenuTreeHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Navigater', 'NavigaterHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'N', '否', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'Y', '是', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '0', '邮件推广', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '1', '电话推广', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '2', '网上搜索', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '3', '朋友介绍', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '4', '线下拜访', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '5', '其他', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '0', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '1', '已分配', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '2', '已认领', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '3', '已搁置', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '4', '已关闭', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'app_code_define', '应用编码', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'sys_code_define', '系统编码', '系统编码123a1', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '0', '计算机 | 互联网 | 通信 | 电子', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '1', '会计/金融/银行/保险', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '10', '政府/非赢利机构/其他', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '2', '贸易/消费/制造/营运', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '3', '制药/医疗', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '4', '广告/媒体', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '5', '房地产/建筑', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '6', '专业服务/教育/培训', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '7', '服务业', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '8', '物流/运输', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '9', '能源/原材料', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_LEVEL', 'KEY', '重点', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_LEVEL', 'NON_PRIORITY', '非优先', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_LEVEL', 'ORDINARY', '普通', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '0', '未知', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '1', '外资(欧美)', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '10', '非营利机构', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '11', '其他性质', '', '12', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '2', '外资(非欧美)', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '3', '合资', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '4', '国企', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '5', '民营公司', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '6', '国内上市公司', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '7', '外企代表处', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '8', '政府机关', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '9', '事业单位', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'BUSINESS_NEGOTIATION', '商务洽谈', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'DETERMINED_INTENTION', '确定意向', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'PRELIMINARY', '初步进展', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'SIGNING_DEAL', '签约成交', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'STAGNANT_LOSS', '停滞流失', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '1000-5000', '1000-5000人', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '10000', '10000人以上', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '150-500', '150-500人', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '50', '少于50人', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '50-150', '50-150人', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '500-1000', '500-1000人', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '5000-10000', '5000-10000人', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_STATE', 'Confirm', '确认', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_STATE', 'init', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_STATE', 'Submit', '提交', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcmenu', '功能菜单', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcnode', '功能节点', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('GRP_STATE', '0', '无效', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('GRP_STATE', '1', '有效', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'MAIN', '主处理器', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'OTHER', '其他处理器', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '0', '关闭', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '1', '展开', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'disableMode', '不能操作', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'hiddenMode', '隐藏按钮', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'crm', 'AEAI CRM', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'dp', 'AEAI DP', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'esb', 'AEAI ESB', '', '0', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'hr', 'AEAI HR', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'portal', 'AEAI PORTAL', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'wm', 'AEAI WM', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_LEVEL', 'INTERMEDIATE', '中', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_LEVEL', 'STRONG', '强', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_LEVEL', 'WEAK', '弱', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '0', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '1', '已提交', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '2', '已确认', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '3', '已关闭', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORDER_STATE', '0', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORDER_STATE', '1', '已确认', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'ENTERPRISE_ENTITY', '企业实体', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'SOFTWARE_AGENTS', '软件代理商', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'SOFTWARE_DEVELOPERS', '软件开发商', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'SYSTEM_INTEGRATOR', '系统集成商', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'UNKNOWN', '未知', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '1', '错误号码', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '2', '有意向', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '3', '态度恶劣', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '4', '态度良好', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '5', '不需要', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '6', '无人接听', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '7', '后续联系', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '0', '未知', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '1', '外资（欧美）', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '10', '非营利机构', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '11', '其他性质', '', '12', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '2', '外资（非欧美）', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '3', '合资', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '4', '国企', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '5', '民营公司', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '6', '国内上市公司', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '7', '外企代表处', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '8', '政府机关', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '9', '事业单位', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('PER_LABELS', '1', '不需要', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PER_LABELS', '2', '有意向', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('PER_LABELS', '3', '错误信息', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('PER_SEX', 'F', '女', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('PER_SEX', 'M', '男', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('PER_SEX', 'UNKONW', '未知', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PER_STATE', '0', '有效', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PER_STATE', '1', '无效', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'dummy_postion', '虚拟岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'real_postion', '实际岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'AnHui', '安徽', '', '13', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'AoMen', '澳门', '', '33', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'BeiJing', '北京', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ChongQing', '重庆', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'FuJian', '福建', '', '14', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GanSu', '甘肃', '', '28', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GuangDong', '广东', '', '20', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GuangXi', '广西', '', '21', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GuiZhou', '贵州', '', '24', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HaiNan', '海南', '', '22', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HeBei', '河北', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HeiLongJiang', '黑龙江', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HeNan', '河南', '', '17', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HuBei', '湖北', '', '18', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HuNan', '湖南', '', '19', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'JiangSu', '江苏', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'JiangXi', '江西', '', '15', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'JiLin', '吉林', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'LiaoNing', '辽宁', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'NeiMengGu', '内蒙古', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'NingXia', '宁夏', '', '30', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'QingHai', '青海', '', '29', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'SanXi', '陕西', '', '27', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ShanDong', '山东', '', '16', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ShangHai', '上海', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ShanXi', '山西', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'SiChuan', '四川', '', '23', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'TaiWan', '台湾', '', '34', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'TianJin', '天津', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'XiangGang', '香港', '', '32', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'XinJiang', '新疆', '', '31', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'XiZang', '西藏', '', '26', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'YunNan', '云南', '', '25', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ZheJiang', '浙江', '', '12', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'IMAGE', '图片文件', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'ISO', '镜像文件', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'VIDEO', '视频文件', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '0', '无效', 'null', '2', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '1', '有效', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'dept', '部门', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'org', '机构', '', '20', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'post', '岗位', '', '30', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'F', '女', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'M', '男', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_EFFECT', 'Follow_up', '可跟进', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_EFFECT', 'Have_intention', '有意向', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_EFFECT', 'Unwanted', '不需要', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '0', '预约拜访', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '1', '电话拜访', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '2', '邮件拜访', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '3', '陌生拜访', '', '2', '1');








-- ----------------------------
-- Table structure for sys_codetype
-- ----------------------------
DROP TABLE IF EXISTS `sys_codetype`;
CREATE TABLE `sys_codetype` (
  `TYPE_ID` varchar(32) NOT NULL,
  `TYPE_NAME` varchar(32) DEFAULT NULL,
  `TYPE_GROUP` varchar(32) DEFAULT NULL,
  `TYPE_DESC` varchar(128) DEFAULT NULL,
  `IS_CACHED` char(1) DEFAULT NULL,
  `IS_UNITEADMIN` char(1) DEFAULT NULL,
  `IS_EDITABLE` char(1) DEFAULT NULL,
  `LEGNTT_LIMIT` varchar(6) DEFAULT NULL,
  `CHARACTER_LIMIT` char(1) DEFAULT NULL,
  `EXTEND_SQL` char(1) DEFAULT NULL,
  `SQL_BODY` varchar(512) DEFAULT NULL,
  `SQL_COND` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codetype
-- ----------------------------
INSERT INTO `sys_codetype` VALUES ('AuthedHandlerId', '认证Handler定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '', 'B', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('BOOL_DEFINE', '布尔定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'C', '', '', '');
INSERT INTO `sys_codetype` VALUES ('CLUE_SOURCE', '来源方式', 'sys_code_define', null, 'Y', 'Y', 'Y', '', null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('CLUE_STATE', '线索状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '12', '', null, null, null);
INSERT INTO `sys_codetype` VALUES ('CODE_TYPE_GROUP', '编码类型分组', 'app_code_define', '编码类型分组', null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('CUST_INDUSTRY', '行业', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_LEVEL', '客户等级', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_NATURE', '性质', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_PROGRESS_STATE', '客户进展状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_SCALE', '规模', 'sys_code_define', '', 'Y', 'Y', 'Y', '24', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_STATE', '状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('FUNCTION_TYPE', '功能类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('GRP_STATE', '客户分组状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('HANDLER_TYPE', '控制器类型', 'sys_code_define', '', 'N', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('MENUTREE_CASCADE', '是否展开', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'N', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPER_CTR_TYPE', '操作控制类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('OPP_CONCERN_PRODUCT', '产品类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPP_LEVEL', '商机级别', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPP_STATE', '商机状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORDER_STATE', '订单状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORG_CLASSIFICATION', '企业分类', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORG_LABELS', '潜在客户标签', 'sys_code_define', '', 'Y', 'Y', 'Y', '32', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORG_TYPE', '组织资源', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('PER_LABELS', '潜在个体客户标签', 'sys_code_define', '', 'Y', 'Y', 'Y', '32', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('PER_SEX', '个人性别类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('PER_STATE', '潜在用户状态', 'sys_code_define', null, 'Y', 'Y', 'Y', '', null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('POSITION_TYPE', '岗位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('PROVINCE', '省份', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('RES_TYPE', '资源类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('SYS_VALID_TYPE', '有效标识符', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('UNIT_TYPE', '单位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('USER_SEX', '性别类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('VISIT_EFFECT', '沟通效果', 'sys_code_define', '', 'Y', 'Y', 'Y', '32', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('VISIT_TYPE', '拜访类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');


-- ----------------------------
-- Table structure for sys_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_function`;
CREATE TABLE `sys_function` (
  `FUNC_ID` varchar(36) NOT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `FUNC_TYPE` varchar(32) DEFAULT NULL,
  `MAIN_HANDLER` varchar(36) DEFAULT NULL,
  `FUNC_PID` varchar(36) DEFAULT NULL,
  `FUNC_STATE` char(1) DEFAULT NULL,
  `FUNC_SORT` int(11) DEFAULT NULL,
  `FUNC_DESC` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_function
-- ----------------------------
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000000', '客户管理系统', 'funcmenu', null, null, '1', null, null);
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000001', '系统管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '101', '');
INSERT INTO `sys_function` VALUES ('029FB2F2-370F-46D5-A283-A22C8E49341C', '客户管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '99', '');
INSERT INTO `sys_function` VALUES ('1FEEF11B-605F-432E-83BE-AA2173C0EC59', '订单管理', 'funcnode', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '9', '');

INSERT INTO `sys_function` VALUES ('33CF8E06-1181-453B-ADF4-1F18ABC026CF', '客户分组', 'funcnode', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '2', '');
INSERT INTO `sys_function` VALUES ('3B1CB4FE-03E4-406B-99FA-B17520AA9C1E', '商机管理', 'funcnode', '093413BA-1ABC-464F-AEA0-13FF82591D22', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '8', '');
INSERT INTO `sys_function` VALUES ('5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6', '附件管理', 'funcnode', '1F617665-FC8B-4E8C-ABE4-540C363A17A8', '00000000-0000-0000-00000000000000001', '1', '7', '');
INSERT INTO `sys_function` VALUES ('67BA273A-DD31-48D0-B78C-1D60D5316074', '系统日志', 'funcnode', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', '00000000-0000-0000-00000000000000001', '1', '6', null);
INSERT INTO `sys_function` VALUES ('692B0D37-2E66-4E82-92B4-E59BCF76EE76', '编码管理', 'funcnode', 'B4FE5722-9EA6-47D8-8770-D999A3F6A354', '00000000-0000-0000-00000000000000001', '1', '5', null);
INSERT INTO `sys_function` VALUES ('7FFE281D-6135-4180-84DD-8C2715E7EDC7', '潜在客户', 'funcnode', 'D7EB3E38-2988-4729-B810-62C539EAD445', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '1', '');
INSERT INTO `sys_function` VALUES ('8C84B439-2788-4608-89C4-8F5AA076D124', '组织机构', 'funcnode', '439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', '00000000-0000-0000-00000000000000001', '1', '1', null);
INSERT INTO `sys_function` VALUES ('A0334956-426E-4E49-831B-EB00E37285FD', '编码类型', 'funcnode', '9A16D554-F989-438A-B92D-C8C8AC6BF9B8', '00000000-0000-0000-00000000000000001', '1', '4', null);
INSERT INTO `sys_function` VALUES ('AA10D06A-C8FB-4C1F-8193-A81F1BCD438B', '拜访记录', 'funcnode', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '6', '');
INSERT INTO `sys_function` VALUES ('C977BC31-C78F-4B16-B0C6-769783E46A06', '功能管理', 'funcnode', '46C52D33-8797-4251-951F-F7CA23C76BD7', '00000000-0000-0000-00000000000000001', '1', '3', null);
INSERT INTO `sys_function` VALUES ('D16F9D22-079C-45A7-9C71-7B15A5BDFE21', '客户信息', 'funcnode', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '5', '');
INSERT INTO `sys_function` VALUES ('D3582A2A-3173-4F92-B1AD-2F999A2CBE18', '修改密码', 'funcnode', '88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', '00000000-0000-0000-00000000000000001', '1', '8', '');
INSERT INTO `sys_function` VALUES ('DFE8BE4C-4024-4A7B-8DF2-630003832AE9', '角色管理', 'funcnode', '0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', '00000000-0000-0000-00000000000000001', '1', '2', null);

-- ----------------------------
-- Table structure for sys_handler
-- ----------------------------
DROP TABLE IF EXISTS `sys_handler`;
CREATE TABLE `sys_handler` (
  `HANLER_ID` varchar(36) NOT NULL,
  `HANLER_CODE` varchar(64) DEFAULT NULL,
  `HANLER_TYPE` varchar(32) DEFAULT NULL,
  `HANLER_URL` varchar(128) DEFAULT NULL,
  `FUNC_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`HANLER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_handler
-- ----------------------------
INSERT INTO `sys_handler` VALUES ('04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'OrderInfoManageEdit', 'OTHER', '', '1FEEF11B-605F-432E-83BE-AA2173C0EC59');
INSERT INTO `sys_handler` VALUES ('093413BA-1ABC-464F-AEA0-13FF82591D22', 'OppInfoManageList', 'MAIN', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `sys_handler` VALUES ('0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', 'SecurityRoleTreeManage', 'MAIN', '', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('12691874-9834-440C-929A-87C699B7AA4E', 'PerInfoManageEdit', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('1B39CF16-2A87-4429-B3F4-34E6D8AFD8DF', 'ContListSelectList', 'OTHER', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `sys_handler` VALUES ('1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57', 'OrderInfoManageList', 'MAIN', '', '1FEEF11B-605F-432E-83BE-AA2173C0EC59');
INSERT INTO `sys_handler` VALUES ('1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'OppInfoManageEdit', 'OTHER', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `sys_handler` VALUES ('1F617665-FC8B-4E8C-ABE4-540C363A17A8', 'WcmGeneralGroup8ContentList', 'MAIN', null, '5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6');
INSERT INTO `sys_handler` VALUES ('2B0866CA-1264-4989-B178-3868EDC725A5', 'CustomerSalesInfo', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('37732F2D-71B0-4448-BB06-93ADDE5F1F3B', 'CustomerListSelectList', 'OTHER', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');

INSERT INTO `sys_handler` VALUES ('3890BD46-F782-4036-9FC3-E6DA46140C5D', 'PerInfoManageList', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', 'SecurityGroupList', 'MAIN', '', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('46C52D33-8797-4251-951F-F7CA23C76BD7', 'FunctionTreeManage', 'MAIN', null, 'C977BC31-C78F-4B16-B0C6-769783E46A06');
INSERT INTO `sys_handler` VALUES ('494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'SysLogQueryList', 'MAIN', null, '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO `sys_handler` VALUES ('49907EFA-315D-4D73-8775-D677118B88F9', 'LabelsTreeSelect', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('4D2E1F5C-8BE0-4592-90BD-7D4B7440B388', 'CustomerListSelectList', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');



INSERT INTO `sys_handler` VALUES ('613C474D-442A-4ED2-A426-F11E66CCD961', 'CustomerSalesPersonnelInfoSelect', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('61BD908F-3585-47F8-B9EC-AD7E3C6944D9', 'DataStatistics', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('624DC816-58E9-4B7E-949E-D806CE2E4E0D', 'CustomerGroupPick', 'OTHER', '', '33CF8E06-1181-453B-ADF4-1F18ABC026CF');
INSERT INTO `sys_handler` VALUES ('735F70FB-B15F-4379-85F0-BBF48FC4ED06', 'CustomerListSelectList', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');

INSERT INTO `sys_handler` VALUES ('763FBBC1-19D4-4E58-8BC2-423672948D42', 'VisitListSelectList', 'OTHER', '', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `sys_handler` VALUES ('76FF0E56-DA31-4399-82BF-8B1C9B253872', 'OrgInfoManageEdit', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('7CB942DC-6F48-478B-A496-ACCF0D699F53', 'OrderEntryEditBox', 'OTHER', '', '1FEEF11B-605F-432E-83BE-AA2173C0EC59');

INSERT INTO `sys_handler` VALUES ('880B1007-9A06-4A8D-9451-11DD12A7410E', 'OppCreateContEdit', 'OTHER', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `sys_handler` VALUES ('88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', 'ModifyPassword', 'MAIN', null, 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `sys_handler` VALUES ('894F3E00-FF69-494E-A072-5D63D185097E', 'CustomerInfoEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('9362C464-AD01-4D5C-9146-64D205EDA1D8', 'ProvinceListSelectList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('9A16D554-F989-438A-B92D-C8C8AC6BF9B8', 'CodeTypeManageList', 'MAIN', null, 'A0334956-426E-4E49-831B-EB00E37285FD');
INSERT INTO `sys_handler` VALUES ('9B2D4066-1BB1-42D6-8D6B-3AC9954D9A22', 'ProductTreeSelect', 'OTHER', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `sys_handler` VALUES ('9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'CustomerGroupManageList', 'MAIN', '', '33CF8E06-1181-453B-ADF4-1F18ABC026CF');

INSERT INTO `sys_handler` VALUES ('AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'CustomerGroup8ContentList', 'MAIN', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('B4790CBE-FB9F-4D2C-8243-7400AF03F1CA', 'OrgInfoManageList', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('B4FE5722-9EA6-47D8-8770-D999A3F6A354', 'CodeListManageList', 'MAIN', null, '692B0D37-2E66-4E82-92B4-E59BCF76EE76');


INSERT INTO `sys_handler` VALUES ('CB2467A4-50FC-42A7-A66A-A431DB719E00', 'OppCreateOrderInfoEdit', 'OTHER', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `sys_handler` VALUES ('CF361BBD-411A-4EED-A246-3D10B318F6F9', 'OrgInfoCreateCustomerEdit', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('D7EB3E38-2988-4729-B810-62C539EAD445', 'ProCustomer', 'MAIN', null, '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'VisitManageList', 'MAIN', '', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `sys_handler` VALUES ('E9F21041-5A09-45F6-8B06-5905A5400E69', 'OrgInfoCreatePhoneCallEdit', 'OTHER', '', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `sys_handler` VALUES ('EA9CD938-A197-4EBD-99CF-B0CF95F8756E', 'VisitManageEdit', 'OTHER', '', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `sys_handler` VALUES ('F5FFE7AF-18EE-41B1-BDFD-E6A28ABB493B', 'SalesmanListSelectList', 'OTHER', '', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `ID` char(36) DEFAULT NULL,
  `OPER_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP_ADDTRESS` varchar(32) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `ACTION_TYPE` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('659459F8-2CDA-4FE0-9A44-D83D3F02256F', '2015-07-03 11:29:42', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('754144F9-51B5-49CB-9971-27CDD8028A31', '2015-11-24 11:43:13', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('54D89879-811A-435A-B57E-381418997E39', '2015-11-24 11:44:08', '192.168.1.100', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('EFDB33E3-1707-41A4-BE4C-796097043F44', '2015-11-24 11:44:36', '192.168.1.103', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('567FCFD0-373F-4656-A89E-19A1881A4653', '2015-11-24 13:16:01', '192.168.1.103', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('E6DA41F7-2019-46BE-A521-6BE611068169', '2015-11-24 13:23:35', '192.168.1.103', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('C44308B0-FBFA-4CE1-A2D6-EE676E3A21D5', '2015-11-24 13:23:51', '192.168.1.103', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('6363A77B-D2E7-42C2-847B-CF4EE6D4EF21', '2015-11-24 13:23:57', '192.168.1.103', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('8A21DB9D-9EE8-4C75-9177-7CF2FBF67389', '2015-11-24 13:24:04', '192.168.1.103', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('1BA49A5D-1C25-4C8A-A7E3-C10B8F07E864', '2016-03-11 15:59:17', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('C59735B7-6FF5-4964-8D3D-2A1DEBACC28D', '2016-03-11 15:59:33', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('42D7A444-70C2-47A2-BB32-BEFB8D127887', '2016-03-11 15:59:43', '127.0.0.1', 'CS02', '赵晓二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('2E05FF58-9E47-4DF0-807B-17DD1440F265', '2016-03-11 15:59:51', '127.0.0.1', 'CS02', '赵晓二', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('FD889F8B-BC04-4D39-A6DE-2CF7FCC15E99', '2016-03-11 16:00:12', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('F804F336-09E2-4424-BD8C-E25847505B90', '2016-03-11 16:32:53', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('8209A568-D94D-481D-9206-87059162A894', '2016-03-11 16:32:57', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('38612DC4-4C03-4892-899B-20B760F364AC', '2016-03-14 17:33:44', '127.0.0.1', 'CS02', '赵晓二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('22F1D31E-4EB5-4F32-8E46-4EB4E8875CFB', '2016-03-14 17:37:34', '127.0.0.1', 'CS02', '赵晓二', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('8AC9857A-D5F5-46B8-ADF2-FCF451E43318', '2016-03-14 17:37:40', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('B2A4F09B-6E3A-4EC1-90F7-63E98ED6D9DD', '2016-03-15 08:47:10', '127.0.0.1', 'CS02', '赵晓二', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('E00B70F8-F42B-4E25-B01F-6E89666EAEBB', '2016-03-15 09:22:23', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('D48BCF2B-D465-4404-85B9-AF56F170C4BE', '2016-03-15 09:27:39', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('49669FB3-8987-4DC1-BCB3-3EDAE1841628', '2016-03-15 10:57:33', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('F8213F8A-4B38-40E7-8301-0AA1845BAB0C', '2016-03-15 13:15:48', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('FFA71D05-708B-4311-9A0E-B54D837C1A3F', '2016-03-15 14:22:58', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('81C8C688-764E-4584-86E9-2EE9542AA8E2', '2016-03-15 14:30:31', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('01785ACD-C2DC-4506-8FF2-771615A694AE', '2016-03-15 14:30:37', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('911D2096-52AA-4E79-AF8B-B8FD83AABEC8', '2016-03-15 14:30:51', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('77B59201-82FB-49AB-9755-6E373C247D49', '2016-03-15 14:30:56', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('27A40EC6-144E-459B-8707-2AAEC883E96D', '2016-03-15 15:53:56', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('697B2187-AD29-446E-A5BF-5FABBD73C2E2', '2016-03-15 16:24:49', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('6F19E49C-A316-4A2F-93D7-BC97F86CEF63', '2016-03-16 09:53:37', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('FAE55FE6-DDB4-45B4-8BEC-EA6A920640FA', '2016-03-16 09:54:00', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('7219AA53-40E0-463A-996D-4B8E0148BEE7', '2016-03-16 09:54:06', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('3076B238-95A6-40D2-9661-07EF26C1DBBF', '2016-03-16 09:58:39', '127.0.0.1', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('C7B7B311-1B08-4EC3-999E-37477BB2911B', '2016-03-16 09:58:45', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('CE09A444-4896-43AC-9B96-35539908CE3F', '2016-03-16 11:12:18', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('8D9EA428-F4BD-48BF-8F85-8DDF8B16DDDB', '2016-03-16 14:38:03', '127.0.0.1', 'CS01', '张老大', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('1981E7C3-0ACD-4574-9076-C6A602250D1C', '2016-03-21 16:43:40', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('49997DFE-75A2-4398-819C-CB9CFA0FDC25', '2016-03-21 16:49:57', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('8F0EEC2C-9033-4716-A050-A5B4F316D6A0', '2016-03-21 16:50:02', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('0FD97507-692A-4706-B249-D8ECDEC99319', '2016-03-21 16:50:21', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('60B4B72F-4431-4A7A-840C-7B692BB3396E', '2016-03-21 16:50:24', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('9CAA3619-0DC1-4494-989C-5270482311D3', '2016-03-21 17:47:03', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('220671D2-7829-4B12-B47C-B8A4EEB4F99D', '2016-03-21 19:27:58', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('F0E866D6-91DD-413D-B6C0-0F972E94612D', '2016-03-22 08:17:45', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('E9D5ECAB-062C-4E6D-9E88-751822EDAA49', '2016-03-22 10:52:09', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('655EF5A1-E3F4-4A24-82B4-FF5C7FCEE362', '2016-03-22 13:21:27', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('95F6BA18-C360-4B38-A25B-B20954AB1A46', '2016-03-22 16:56:46', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('84D37FAD-EC65-4586-B3EF-6DF387A8A352', '2016-03-23 09:17:29', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('1A481B82-BBE7-4CF9-9981-D0DB74202C5D', '2016-03-23 13:02:49', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('E0DF73B4-AC6E-4D34-BC6C-19C98CA189AF', '2016-03-23 14:49:24', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('298F57EC-78E8-4183-9327-263C407959B2', '2016-03-23 19:17:13', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('9278BFE1-2C46-43CD-9B60-F78D8E91554E', '2016-03-24 07:34:19', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('103BC422-71B4-490F-A971-D268E81BEA16', '2016-03-24 08:49:40', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('FE491F01-9159-464A-A492-2D2632CED7A7', '2016-03-24 09:40:13', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('8D5DE230-D44A-49E0-8402-FFFA77DB96D3', '2016-03-24 13:20:56', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('5FBB5749-6CB5-47FD-9870-E963805F0F21', '2016-03-24 13:21:16', '127.0.0.1', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('249CFCAF-99AD-49E7-9FF4-C7BB3CFB5925', '2016-03-25 10:37:49', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('6DEFD2F3-AE6A-4573-B1A1-B703A15E3CF2', '2016-03-25 13:25:16', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('79C92AB8-9029-4F83-B8F5-A0A19693976C', '2016-03-28 09:36:41', '127.0.0.1', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('7BDF0698-2C1C-4893-8611-2B4FA355D21C', '2016-03-30 09:52:29', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('59ADA05B-D5AA-4B68-A162-0A91A3C5755D', '2016-03-30 13:13:05', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B521676C-8E77-46F9-9208-96C40002C7D9', '2016-03-30 14:49:36', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('35AAD471-8805-436A-821B-77B9AA2A8185', '2016-03-30 15:15:43', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B631E261-53F5-4BBF-ACB5-913DDA1B7E9E', '2016-03-30 15:56:29', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('3F674299-D8ED-4250-B421-E26CC5114A10', '2016-03-30 15:59:31', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D3A61855-BFEF-4967-9C87-C73355D82B3F', '2016-03-30 18:18:07', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('474121CC-ACE0-46B3-BFE3-5FABBD71D841', '2016-03-31 09:25:46', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B501C4CE-EFC8-4956-9F86-5910ED74492B', '2016-03-31 10:26:10', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DE99AC01-0B1C-46C4-BB43-6606FDBA5B68', '2016-03-31 10:28:24', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('54E5051D-1252-4377-86A5-5F84BBA756FB', '2016-03-31 10:29:35', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('AD7B016A-E18D-4523-906C-A3A7F6D437A4', '2016-03-31 10:30:27', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('87EF56D5-2274-4844-B5D6-93C016DE6673', '2016-03-31 10:35:58', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('591D2A15-E01C-41B0-A3EF-641ADAE4592D', '2016-03-31 10:38:11', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('53971090-6F9F-4E6B-AED6-C66CD7112329', '2016-03-31 10:40:16', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('1087902A-EBC5-4F68-8E0E-E22312B72ED3', '2016-03-31 10:41:12', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('2512FD15-48EA-4F1F-AAA3-6430EA622DA5', '2016-03-31 10:41:46', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F2C4331F-DC58-4F4E-8B2C-4DED8283A099', '2016-03-31 10:44:29', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A5CFD9E0-46AC-461C-9F46-1404FE2B837B', '2016-03-31 11:44:40', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DC29C4DF-F9B3-401E-A923-BD08DEEB543D', '2016-03-31 13:27:31', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BD8C97A4-832D-4143-BDC8-26A341217C45', '2016-03-31 16:13:03', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('2264C523-36F8-42DA-9854-B6F9CCDC9E7C', '2016-03-31 20:34:21', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('26F64D88-BA94-4598-AC89-19318C74796C', '2016-03-31 21:13:28', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DAC55FAC-61A6-402F-9DE3-FCBF817F1AF5', '2016-03-31 21:37:51', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('61F661A8-5A6C-431C-8E20-127C62604025', '2016-03-31 21:37:51', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('7A13080F-BAA7-4741-A06E-E7E9F7C1AFEB', '2016-03-31 21:38:27', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C6BDBD7F-C3BC-40D0-B0F7-26219ECA9FA1', '2016-03-31 21:38:27', '192.168.1.102', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('55D7BB92-CEB0-482F-8DAC-D4E26BB0D46F', '2016-04-01 08:54:18', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('98BB9A6E-92CB-4EDE-B7C2-EABE2A8DA9E9', '2016-04-01 08:54:19', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0E70EA19-E998-4042-87CD-A49B14326020', '2016-04-01 08:59:17', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0DEB3984-F52B-4266-BEDB-A952A86C02FB', '2016-04-01 08:59:17', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('4D229201-9750-4CB8-96B9-639DE8C316EA', '2016-04-01 09:19:53', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F2F7C630-7A3D-4A69-9890-6D01D2493EFE', '2016-04-01 09:19:53', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('CC416C14-A81A-4A92-909F-6584C1B8CEC5', '2016-04-01 09:23:36', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('933C5CBE-4C7E-4F6E-9B3C-BEE42BC8AA37', '2016-04-01 09:23:37', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('07C72497-F608-41B3-AC67-5B59A2897B32', '2016-04-01 10:03:38', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A8C050B8-27E5-4612-BF23-363E10D1B66A', '2016-04-01 10:03:38', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0557F10D-EB10-452F-9697-F0147688922C', '2016-04-01 11:44:59', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BDF85439-BFAB-40E2-AFD1-7D5C9BCFBE1E', '2016-04-01 11:44:59', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('9321ADC2-3528-49ED-A1A1-55929F1FBA11', '2016-04-01 12:53:25', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('E9546539-18F6-4C0C-BA11-72566B2C3744', '2016-04-01 12:53:26', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F400273B-05A5-48F5-8387-4BB5D83295AD', '2016-04-01 12:57:56', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('9DE6D2EB-C34E-4A11-9BE9-7411420EE81E', '2016-04-01 13:10:56', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('34601294-9A28-450D-8728-26A48EC5738B', '2016-04-01 13:10:56', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('442F052F-C4D6-4491-8075-76BD2AC22765', '2016-04-01 13:24:13', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('CDB6A363-77F8-4208-BFDA-165EBF55FB56', '2016-04-01 13:24:13', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('15300615-B922-4166-BC28-A58CCA9F8901', '2016-04-01 13:46:38', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('040F9621-A4E5-451D-BEED-03A8DD6B6E88', '2016-04-01 13:46:38', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('70EC7E3A-6C10-4B54-BE70-59D4434A6FC1', '2016-04-01 14:04:15', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('284FF2ED-02A4-49DE-A325-C1129738AF70', '2016-04-01 14:04:15', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('4D7BF1CC-2F5D-4D1C-A2AB-92214A9F10E6', '2016-04-01 15:22:10', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DE4E2607-8F2A-4695-89EB-54FA4E2C83F3', '2016-04-01 15:22:10', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('4829C7CC-8097-4EB2-9CF4-78C9827556B2', '2016-04-01 16:19:33', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A4E8B73E-7318-467D-AF68-E7304E8C9CF2', '2016-04-01 16:19:33', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C7F2F205-605E-42BE-BFAE-D00BEC7CFE19', '2016-04-01 16:20:11', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C1BF282B-AAAE-4EE2-AF23-E385B95FB5AB', '2016-04-01 16:20:11', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('482C0892-738D-405A-B2D8-B8F75B32888F', '2016-04-01 16:20:19', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('5E62FE25-3712-4E13-89C2-EE7F60EBC64C', '2016-04-01 16:20:19', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('295D8AA3-53EB-4633-91DE-BC0F2C622EC0', '2016-04-01 16:21:37', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('756298FD-5695-4D68-A816-EF3ED2D0F8AF', '2016-04-01 16:21:37', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A6BA5048-C199-4577-A4D9-85342097777D', '2016-04-05 09:30:36', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('354DA465-03BE-4044-826B-1EBFE2C1598F', '2016-04-05 09:30:36', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('845EAF0F-DA97-48FE-8DEB-9E68B281F742', '2016-04-05 11:42:58', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FF01B59A-6496-4132-AD24-C0138B3DEAB7', '2016-04-05 14:02:44', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('208A6CE2-3823-4CB8-9748-0F459FC12E83', '2016-04-05 14:02:44', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FD96D9F3-CC46-420C-826E-AE2A5DB17CA9', '2016-04-05 20:03:13', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('5B64A5F6-566C-4DEE-BEF0-EAA6A66ACDA6', '2016-04-06 09:42:34', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('461D1D1C-CB0B-401D-BB18-E2B45D72AE47', '2016-04-06 10:43:40', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FD2882A8-A29F-4170-BD37-1FBBCE98AFE6', '2016-04-06 10:43:40', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DA413971-C540-4072-8378-753416C1AB0E', '2016-04-06 10:50:16', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C943CABA-0B5F-4224-89F7-68F1B31A8186', '2016-04-06 10:50:16', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B8CA1798-9BD2-4095-9908-E400F381FB03', '2016-04-06 10:55:37', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('14E55E71-B415-4BFD-A1A5-F40C09CBDB24', '2016-04-06 10:55:37', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('ACB78AE0-92BD-4727-BEF7-39003E760DF4', '2016-04-06 11:13:02', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('9AD50151-68D2-47F2-A73B-87652835C786', '2016-04-06 11:13:02', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('E2AF702B-0486-4620-8101-1F311AAE7999', '2016-04-06 11:44:02', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('60461D96-C06D-467E-ADF3-5508FFA5E31B', '2016-04-06 11:45:04', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FF5816E9-E593-4EB5-8D87-76E7C7070F73', '2016-04-06 13:58:08', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D8EC205C-A2FB-4AE1-A69B-E09010074615', '2016-04-06 13:58:08', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('155E48B4-F9CA-42E9-B367-F9BD9C1D5326', '2016-04-06 14:54:13', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('2DB71E8A-E0EB-4CFA-8E34-DBF6B47C9F1B', '2016-04-06 14:54:13', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D77A1654-6BC3-4636-A921-487460B68172', '2016-04-06 15:09:37', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DD130786-0151-428B-B7FC-AB764C279CCA', '2016-04-06 15:09:37', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('194B453C-6FD4-4351-9FD9-79F2E26B3AEE', '2016-04-06 15:11:43', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('EF5B1082-82C2-4644-A732-2A78EA00605F', '2016-04-06 15:11:43', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B548F047-FD0F-4C71-B9A8-8D0F2EA2014A', '2016-04-06 15:17:20', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('EAA669B7-20C2-42D6-92F8-68C31AD5B915', '2016-04-06 15:17:20', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('714EAE56-C990-45A1-A591-2DC8F35AF179', '2016-04-06 15:38:39', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('85233AE4-3663-4F66-B7B6-F331E2224FD3', '2016-04-06 15:38:39', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('420D77EB-5636-4C22-A189-778F4847675A', '2016-04-06 18:08:34', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A2FF9223-AB89-40B4-A272-28DC1CF12516', '2016-04-07 14:21:41', '192.168.1.106', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C236E6B0-9DF6-45CA-B219-89AAE909A2B1', '2016-04-07 14:21:41', '192.168.1.106', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('4B3353F2-2DA3-4A74-8B19-FD81BE3612FD', '2016-04-07 14:35:32', '192.168.1.106', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('64B31A6B-F564-466E-9B15-E923BE117C93', '2016-04-07 14:35:32', '192.168.1.106', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('08BC89EC-E0C6-47A5-8339-8F26C41EFEE9', '2016-04-07 15:15:48', '192.168.1.106', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('5EC6A173-75BD-403B-9F7C-DC6E61FF5329', '2016-04-07 15:15:48', '192.168.1.106', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F46CF2E4-0DF7-4709-B175-09C2509759E1', '2016-04-07 17:39:31', '192.168.1.106', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B7CBF287-97AB-452D-A918-970412CC9B9D', '2016-04-08 08:58:30', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('25E67BE5-D6F3-422B-B234-DCA0AD3D944E', '2016-04-08 09:07:49', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A4BF7CF5-C168-4B28-90BD-1B811D44995A', '2016-04-08 11:46:53', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('590604FF-9A62-4162-8E49-AF27238F71F1', '2016-04-08 11:46:53', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('8BF60C14-71F9-4B7A-AF5B-CD2752E165C7', '2016-04-11 08:44:22', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0A0BBB68-6659-452B-A5EF-408FB6A1BC02', '2016-04-11 11:38:33', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D700BFBE-8A71-4DF8-B9CC-E7FB5F37BFEF', '2016-04-11 11:38:33', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D7E1BA43-D5AB-4D7B-8BEE-1D344E8A1B89', '2016-04-12 09:06:00', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('EFD1F2CA-5B68-4501-A518-5EAD32F2A1F0', '2016-04-12 12:41:57', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('39FEF292-5AA4-4E2A-B215-9245636A5198', '2016-04-12 12:58:23', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('E81B9D7A-2874-4D3F-A3F6-E8EEF4EED4E0', '2016-04-12 13:01:11', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('70B774C7-42F1-4374-8E7F-A2428331C614', '2016-04-12 13:01:37', '192.168.1.100', 'CS04', '赵小四', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('CE945DFF-276E-41E6-9A91-18811CF03963', '2016-04-12 15:12:11', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DAF8EFB6-3395-470C-8F6E-F077AE882B5B', '2016-04-12 15:15:14', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('E66247C3-AC2A-4469-9583-08FD65B8564B', '2016-04-13 16:45:07', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FE887B6A-4E4C-4E6E-91CB-E017DA5440CA', '2016-04-13 17:10:03', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F23B0C66-5CC3-4C46-9CE8-84AE20B95104', '2016-04-15 13:14:50', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('1DC27BF0-2764-49DF-9070-6EDE12A552CE', '2016-04-15 13:20:21', '192.168.1.105', 'CS01', '张老大', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B3510364-1473-4039-8FBA-15E0D5167CE6', '2016-04-15 13:26:00', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('02F624DD-D047-41DB-8A1E-940CA920D221', '2016-04-15 13:37:55', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('EDD9E971-5A54-453E-A9A4-404C8224BE2B', '2016-04-15 14:21:38', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('388DADAA-9825-43D4-A2C4-925BA1A9363B', '2016-04-15 14:22:10', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('AB6F0970-DFF2-4B18-A102-BBF5F9AE8A30', '2016-04-15 14:22:10', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('5C0B55F6-DD86-4F87-927D-6625B2DB923B', '2016-04-15 16:06:35', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DD8FED07-CEA0-4FC2-BC2D-1D1FCEFB6BA5', '2016-04-15 16:06:35', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BDC34C11-AB56-4637-A922-D5CC5BE32F23', '2016-04-15 18:09:02', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('EE47F4C0-09F0-415E-8A53-D992A26894C7', '2016-04-18 08:33:04', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('571588A3-E378-4104-B797-A546A89E0AF5', '2016-04-18 08:36:30', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('87E5229A-38E0-4196-911B-F4BE212E7D64', '2016-04-18 09:00:23', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C236354B-B016-428D-B574-8B9685EB8714', '2016-04-18 09:28:46', '192.168.1.100', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('46B198F4-D813-4EFC-9433-7719B87EABE4', '2016-04-18 09:28:46', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('3DEE540A-7CDC-4D83-8D0E-BEA18375E40F', '2016-04-18 09:29:10', '192.168.1.100', 'CS01', '张老大', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BE6E3B37-6593-4EA9-8B2F-719A59806A57', '2016-04-18 09:57:10', '192.168.1.100', 'CS01', '张老大', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('5BF94A7B-03CE-4E24-A531-F5CD70D66546', '2016-04-18 09:57:11', '192.168.1.100', 'CS01', '张老大', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B9B30FAF-B01F-48FF-9B0F-6F942D544A19', '2016-04-18 09:57:40', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('097F9CF2-B9EC-4FE8-B7F0-056CB2D8F7E0', '2016-04-18 10:30:49', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BCE818AF-AC54-46C1-87F0-93E90BCE3635', '2016-04-18 11:47:52', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('89DFDC3D-0E65-4622-A6A6-B2361B530F4F', '2016-04-18 13:22:43', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('54C6D6A0-F0B2-4101-A849-969D10A28CA4', '2016-04-18 14:44:11', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('72C60ABF-6E5E-408E-95CB-108BF13518D4', '2016-04-18 15:57:03', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('2BA5243E-8827-4DC3-97EB-64C8BE4EB39B', '2016-04-18 16:16:16', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FAF41884-AD42-4D40-BCB5-C5CE18DBF5DC', '2016-04-18 16:37:45', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F8662F3B-E82E-4BC9-ACFB-2F122B545B34', '2016-04-18 16:55:39', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A8374EBE-D267-4396-B218-4D7A1B54F19C', '2016-04-18 16:59:55', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('60E08AB9-5084-4AA6-9C01-4FBA0159C6F6', '2016-04-18 17:58:42', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DF7DAF43-04E3-41C8-8F7B-7C70FAFA9610', '2016-04-18 17:58:42', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B6BEC9A9-2A7E-4996-866B-ED1804430CAF', '2016-04-18 17:58:43', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D1BC3965-ACEC-4877-AF95-FAFD3C878D03', '2016-04-18 17:58:43', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('6261BBAF-1AA3-47C6-BDE6-F857B1A077F1', '2016-04-18 17:58:43', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F2BC919C-9F31-4303-BE6F-A54FE8BD4CE3', '2016-04-18 18:28:16', '192.168.1.100', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('CDEFE36A-0142-47FE-8D51-14C1F1AA89C3', '2016-04-19 10:22:52', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('578A7E01-1533-43F1-ACD6-2722E261DFC6', '2016-04-19 10:32:24', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0ADD8E0C-6042-437C-B8CC-B2538804A528', '2016-04-19 14:25:52', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('AD503C0D-A1C1-4944-B425-8A2FC21F7F5F', '2016-04-19 14:31:55', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('23B63DE0-FB52-47C7-8AE2-9969FE343EE4', '2016-04-19 20:05:54', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A416EB14-1E11-4B74-ABA0-34ECCCA8A1B6', '2016-04-20 15:52:25', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('55F094F2-9295-419B-B633-7AB880F0DEB2', '2016-04-20 15:56:24', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B75DAB59-EE03-4233-8E6D-7A9CB33A9CCC', '2016-04-20 16:50:06', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A4E45D4D-0F42-4E6E-8698-990249D6E4E9', '2016-04-20 17:30:40', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('8DD4EC46-CCF5-46F7-9DAB-E1C80DEB9A67', '2016-04-20 18:29:55', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('6671857A-64A5-4151-B091-B63734401C12', '2016-04-20 18:29:55', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D2673076-C64D-4709-9C22-5FD1D186A4CD', '2016-04-20 20:20:01', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('609B368B-4E03-4C1E-8C8F-CE04A9B61496', '2016-04-21 08:43:34', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('894A355D-76AC-4EFC-B71F-55434A12E878', '2016-04-21 09:31:27', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D0FC74A5-BD08-44F4-A840-414FE1FE43C7', '2016-04-21 09:35:21', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('1B5D6AA2-6B86-453D-8B79-BAE2785276C3', '2016-04-21 16:08:17', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('AB711692-527C-4236-AE15-3C4C92832BBD', '2016-04-21 20:29:17', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('41AE3C93-34AE-4998-B15B-686AE2C3652D', '2016-04-21 20:45:27', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('EE95427D-70F3-4921-BE57-1A1ED6FC62B9', '2016-04-21 20:57:47', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('4A371852-2874-4584-8555-FCFE5D9183A0', '2016-04-21 22:32:10', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('476023EB-FA22-43C4-AE26-EFB7A08A8314', '2016-04-21 22:47:27', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0FFACEE9-5310-4B6C-A700-A7F2487AF832', '2016-04-22 09:21:27', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('04ECAABA-4E71-45AA-BF18-E971B063E3AA', '2016-04-22 09:21:27', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('18896739-7200-4017-9209-49BC2CF2764C', '2016-04-22 10:16:25', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('3F6EFE24-6778-440B-8E57-32454D351604', '2016-04-22 13:15:51', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('CA99E912-A206-4682-A11B-839121D5F328', '2016-04-22 13:18:21', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('009D1806-C711-4D36-A0A2-CFFB883BE6FB', '2016-04-22 13:22:57', '192.168.1.103', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('DBFDC3F0-4FBE-4EC2-8927-11E9905533AB', '2016-04-22 13:22:57', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('AEE11175-422E-4D1E-8D53-F12527F4573F', '2016-04-22 13:23:06', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D743CEBC-CD3C-44D7-923D-A8CED2501088', '2016-04-22 13:32:57', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('62542C15-D275-4F2D-B336-1CDDC335CEA7', '2016-04-22 13:39:49', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('38D5ED00-214F-408A-93C0-D860A3BFAFBA', '2016-04-25 10:57:59', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('2A5E05CF-A417-4043-AE3D-AB49B59A7AE2', '2016-04-25 11:01:53', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F111BDA0-F759-45DA-BC83-A8CAFC99B54D', '2016-04-25 11:26:15', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('8BE13017-96C2-43E3-972F-4DE96BD798D4', '2016-04-25 13:05:10', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('782AB40F-32AD-4FC7-BFCC-1F0211D8A276', '2016-04-25 13:26:06', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B8550B63-683A-46C1-BDEF-4D349E3BBC82', '2016-04-25 13:32:27', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('7A06473B-ED04-4A4D-AE0E-12F6CE8C12F6', '2016-04-25 16:38:44', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('5F8CD7AA-6800-4666-89A9-7CAA60BC9A13', '2016-04-25 16:41:33', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F67D15F1-181A-4D20-B114-DFB18C0C9AB8', '2016-04-25 18:00:50', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FBF918E5-C1AF-4DA0-BCB5-E4A3B880C249', '2016-04-25 18:01:21', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('6708CC8C-96D9-45B9-8ACF-CDF66C1E5000', '2016-04-25 18:15:11', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A630244C-7F53-4486-974F-FFF2229AECC9', '2016-04-25 18:27:18', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('5ACFE8EE-4B12-46AB-992F-AEE9CDDA194F', '2016-04-25 18:39:25', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DED7539D-C423-4022-9352-C4EF3FDE9B20', '2016-04-25 19:27:25', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('CCC082D8-2723-4C29-81F9-6321D152A38D', '2016-04-25 19:28:05', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BDA1CAD7-8283-4CF9-A553-BA64C9426707', '2016-04-25 19:48:08', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('664EF57D-F7C8-449C-B060-1B6656091AD3', '2016-04-25 20:14:01', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B4649D1C-9F6A-4BC2-8524-5BCB285F3061', '2016-04-25 20:35:20', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('833510C2-C5AD-49C2-985E-A0F063C658D7', '2016-04-26 08:38:12', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D6A75131-8199-475D-835B-96983F7A71DC', '2016-04-26 08:47:04', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('E47B17FC-C605-4A48-A0B2-038FAB568E56', '2016-04-26 09:37:41', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('FC52F2B5-416D-4900-8CF5-160CCAE0D578', '2016-04-26 09:46:42', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('673F668E-6BE9-4788-B20A-78EC478B4770', '2016-04-26 11:58:10', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('F0B4E92B-3F8E-428B-BF7E-225C4EB8FE7C', '2016-04-26 12:00:10', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('E0B9893B-68C2-4F53-A243-EACB15C79215', '2016-04-26 13:10:26', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('4F67E0AC-3649-4810-98B8-F3B185103777', '2016-04-26 13:33:57', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('9A960FF4-AB6A-4FC7-A889-5448FA5454E7', '2016-04-26 14:08:50', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('011C9BF2-8E6B-420C-9BB4-DF1247CCC34D', '2016-04-26 14:44:13', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B4900EE1-E2FD-4ADC-A540-0F2C773D332E', '2016-04-26 14:50:01', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('28C464EE-81A7-4CCF-B293-B7B394585FE9', '2016-04-26 15:22:21', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B1B9D57E-075E-4AEC-97FE-3C44929CEAD6', '2016-04-27 10:14:32', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A9F5A59F-11F8-417B-8721-8985E8E56E84', '2016-04-27 13:22:34', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('1642433D-7E53-46EE-A078-285601D764DB', '2016-04-27 14:22:44', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('30FF9F5D-4EB4-4F84-A320-B01BB1D4A452', '2016-04-27 17:51:26', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0125D690-9561-4D04-8BD0-3C36D3508D97', '2016-04-27 17:57:29', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('38F07A7C-E9F1-4EEB-9598-CD89AC222233', '2016-04-27 17:57:29', '192.168.1.107', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('41F5B76E-1F81-4A7C-947E-A01943B39E3C', '2016-04-28 15:33:06', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('92AA6C12-B622-462A-9352-CB90F553EC0A', '2016-04-28 15:34:49', '192.168.1.103', 'admin', '管理员', '退出系统', 'logout');
INSERT INTO `sys_log` VALUES ('84FD1C5C-7423-48CE-BD09-A35E32915936', '2016-04-28 15:34:49', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('39BA951A-6B3F-4CD7-A5AB-27C696B8342D', '2016-04-28 15:35:21', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DEB2F526-89F6-48CF-AEE0-6047F1BF50E2', '2016-04-28 15:36:13', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('DB8E65C4-F8E8-4665-9845-489A4DFFBE47', '2016-04-28 16:34:15', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('48888242-0296-4878-BDCF-914D61FFDEB9', '2016-04-28 16:34:15', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('92221045-7B2A-4D71-91DA-701F8912044B', '2016-04-28 16:34:15', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B748CCC2-8A6C-4F81-ABBF-326DD29F499E', '2016-04-28 16:34:15', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('6CF33EFE-DD69-4EAF-B2F8-11986589157A', '2016-04-28 17:06:59', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('77B24F20-7DCF-4D5E-BECE-DCB0C07CF592', '2016-04-28 17:06:59', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('04EC5436-BE2F-4D57-B3E5-A469AB5054A2', '2016-04-28 17:18:42', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('56E9E70A-44C2-430B-84DD-419CE1E4C0DD', '2016-04-28 17:18:42', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('00842259-0232-4954-956B-41918B8AA5CE', '2016-04-28 19:36:32', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('40DD1FB8-2BC3-4D45-A40B-10CF11E415CB', '2016-04-28 19:36:32', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C5A93C8C-0002-4CE5-97BB-2469DAFCE12D', '2016-04-28 19:37:12', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('3F043857-7FDB-4A93-BBA2-2A3FC8A14E61', '2016-04-28 19:37:12', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('8AFC989B-CA52-4D18-AC4D-3E36FD138DED', '2016-04-28 20:17:27', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('177A7632-01C0-4E3B-B8BE-072AFB665B29', '2016-04-28 20:17:27', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BD5C69C2-B389-48D9-95C5-978C8EC1448A', '2016-04-28 20:27:19', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('ABC611BE-EC5C-400B-A72F-E90BF711B0B8', '2016-04-28 20:27:19', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('E40FCE5A-4967-4141-952B-0C4C45D11025', '2016-04-28 20:31:32', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('42C61529-33F8-4D02-92B8-EE8F2FDDB96C', '2016-04-28 20:31:32', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('B6A18E9E-1510-4A4F-B3A0-DFA63E9A3CC3', '2016-04-28 20:35:53', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('A0B37E00-3DE4-4385-838A-7647BD3E4403', '2016-04-29 09:23:40', '192.168.1.103', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0F26A544-C87C-4F83-8254-A0DBBDE6E480', '2016-05-04 09:47:44', '192.168.1.101', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('83CA5E8B-E7FE-41BC-B0E0-F36A44682720', '2016-05-04 10:11:19', '192.168.1.101', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('08FEF3C6-A4E0-44C4-A273-16B290E965CA', '2016-05-04 13:49:09', '192.168.1.101', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('6FEE5D72-93A8-43CC-997C-72E6BA284347', '2016-05-04 14:52:32', '192.168.1.101', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('BA893500-0CA7-451F-AE0D-0A03C1C85836', '2016-05-04 15:33:45', '192.168.1.101', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('099B2D87-9744-4AD8-B4EB-F9514D703E49', '2016-05-04 17:01:58', '192.168.1.101', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('9285089A-4FDB-4488-B953-C8201F0A15A6', '2016-05-05 08:43:47', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('EEAD317F-3F28-4DD3-B9BC-A06FE4C536E2', '2016-05-05 09:23:59', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('0FB52035-1B5D-41A7-B48C-DAE7FF8D5F2B', '2016-05-05 11:32:45', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('D4DED87F-A475-4588-B4AA-EC89BBB1C8C0', '2016-05-05 19:19:06', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('82A3C12B-936C-477B-B9DE-9624CD5F8148', '2016-05-05 19:19:06', '192.168.1.105', 'admin', '管理员', 'CAS认证权限初始化', 'initAuth');
INSERT INTO `sys_log` VALUES ('C836E92A-FB01-444B-8934-5E9E7B1D4663', '2016-05-11 19:07:50', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');
INSERT INTO `sys_log` VALUES ('DB96CEEC-15EE-4101-8A3F-36E673D82F71', '2016-05-11 19:12:28', '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');

-- ----------------------------
-- Table structure for sys_onlinecount
-- ----------------------------
DROP TABLE IF EXISTS `sys_onlinecount`;
CREATE TABLE `sys_onlinecount` (
  `IPADDRRESS` varchar(64) NOT NULL,
  `ONLINECOUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`IPADDRRESS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_onlinecount
-- ----------------------------

-- ----------------------------
-- Table structure for sys_operation
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation`;
CREATE TABLE `sys_operation` (
  `OPER_ID` char(36) NOT NULL,
  `HANLER_ID` varchar(36) DEFAULT NULL,
  `OPER_CODE` varchar(64) DEFAULT NULL,
  `OPER_NAME` varchar(64) DEFAULT NULL,
  `OPER_ACTIONTPYE` varchar(64) DEFAULT NULL,
  `OPER_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`OPER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_operation
-- ----------------------------
INSERT INTO `sys_operation` VALUES ('00042A5A-FF5C-4EF2-89BB-5A6CB2D7EDA6', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'create', '新增', 'create', '3');
INSERT INTO `sys_operation` VALUES ('01E6A560-0552-46BC-8A5F-0C2BEB987617', '894F3E00-FF69-494E-A072-5D63D185097E', 'cancelBtn', '取消从', 'cancelBtn', '11');
INSERT INTO `sys_operation` VALUES ('020285E6-214C-45E5-8E6F-FDF45325F23F', '7CB942DC-6F48-478B-A496-ACCF0D699F53', 'close', '关闭', 'close', '3');
INSERT INTO `sys_operation` VALUES ('02E89343-F996-44EC-8347-6FB3C60BC9BB', '894F3E00-FF69-494E-A072-5D63D185097E', 'submit', '提交', 'submit', '4');
INSERT INTO `sys_operation` VALUES ('0831D3B9-7BD5-4581-A540-72982B899171', '894F3E00-FF69-494E-A072-5D63D185097E', 'save', '保存', 'save', '3');
INSERT INTO `sys_operation` VALUES ('0A92F33C-5E91-4401-99C2-1FB41C8BE0B9', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'cancelBtn', '取消从', 'cancelBtn', '1');
INSERT INTO `sys_operation` VALUES ('0BD650EB-8619-4886-95AA-D87436859B93', 'EA9CD938-A197-4EBD-99CF-B0CF95F8756E', 'generateClue', '生成线索', 'generateClue', '3');
INSERT INTO `sys_operation` VALUES ('138DE440-4C51-413B-B5A9-0205F6FBE806', '4D2E1F5C-8BE0-4592-90BD-7D4B7440B388', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('1720253F-2F2E-403B-9D24-B96287063081', '093413BA-1ABC-464F-AEA0-13FF82591D22', 'confirm', '确认', 'confirm', '4');
INSERT INTO `sys_operation` VALUES ('17FB686F-8580-4995-8494-EC42F9C71FC0', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'delete', '删除', 'delete', '3');
INSERT INTO `sys_operation` VALUES ('184ED581-2AE5-411C-903C-737B79359E5D', '1B39CF16-2A87-4429-B3F4-34E6D8AFD8DF', 'choice', '选择', 'choice', '1');
INSERT INTO `sys_operation` VALUES ('199DCC87-DB0D-4F78-A0AE-CCE348B13516', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'delivery', '分发', 'delivery', '10');
INSERT INTO `sys_operation` VALUES ('1BBAA470-C306-4915-9ECC-EBCF72EDC38D', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'edit', '编辑', 'edit', '4');
INSERT INTO `sys_operation` VALUES ('1BF7D254-304B-4719-BCB6-AB38BF8CA4E7', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'edit', '编辑', 'edit', '5');
INSERT INTO `sys_operation` VALUES ('1DC1E347-7C48-4BE8-A271-9FDCE5A074BB', '3890BD46-F782-4036-9FC3-E6DA46140C5D', 'detail', '查看', 'detail', '3');
INSERT INTO `sys_operation` VALUES ('1EF2E5A8-ACE8-4769-B54F-6E0CB55BFB4E', '880B1007-9A06-4A8D-9451-11DD12A7410E', 'save', '保存', 'save', '1');
INSERT INTO `sys_operation` VALUES ('1F173085-5E15-4E9A-8F06-922E3E92BE95', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'back', '返回', 'back', '1');
INSERT INTO `sys_operation` VALUES ('2321E4A1-2CB5-430C-8841-5129D02D2C38', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'delete', '删除', 'delete', '1');
INSERT INTO `sys_operation` VALUES ('24A26858-4628-472E-810B-964E817E7176', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'detail', '查看', 'detail', '9');
INSERT INTO `sys_operation` VALUES ('262F2106-D50E-4BDF-9FFD-562012596CC7', '2B0866CA-1264-4989-B178-3868EDC725A5', 'refresh', '刷新', 'refresh', '3');
INSERT INTO `sys_operation` VALUES ('28DFDD64-D05F-4D87-A997-11F29FA27042', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'up', '上移', 'up', '5');
INSERT INTO `sys_operation` VALUES ('29596313-EBFD-4D56-A72D-16A91F6FA795', '76FF0E56-DA31-4399-82BF-8B1C9B253872', 'phoneCall', '电话拜访', 'phoneCall', '3');
INSERT INTO `sys_operation` VALUES ('2B49A370-5B78-4FFB-8792-1BB61DBA9478', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('2BF95B4C-B823-402B-AB8C-284CE7EDE678', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'visitAgain', '再次拜访', 'visitAgain', '5');
INSERT INTO `sys_operation` VALUES ('308FB9DB-AA23-4E5A-9D60-2D3BC0B28D7C', '093413BA-1ABC-464F-AEA0-13FF82591D22', 'disposal', '处置', 'disposal', '5');
INSERT INTO `sys_operation` VALUES ('30E6EF99-55BD-4B4F-82E1-D2E4344D8ABD', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'confirm', '确认', 'confirm', '3');
INSERT INTO `sys_operation` VALUES ('31F4B92B-FEB0-46FC-828E-ECB572AEC860', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'reConfirm', '反确认', 'reConfirm', '8');
INSERT INTO `sys_operation` VALUES ('34D36130-6300-425B-8820-E05C1EF2A2E6', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'filter', '过滤', 'filter', '2');
INSERT INTO `sys_operation` VALUES ('37FEF4ED-0F0A-404F-9107-2329D4440A48', '3890BD46-F782-4036-9FC3-E6DA46140C5D', 'create', '新增', 'create', '1');
INSERT INTO `sys_operation` VALUES ('3940F1D5-E0AF-4BEE-9471-E11C7CABBF5D', 'CF361BBD-411A-4EED-A246-3D10B318F6F9', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('3B39B662-671E-4ECF-B14D-AE8F4FDC335D', '76FF0E56-DA31-4399-82BF-8B1C9B253872', 'save', '保存', 'save', '2');
INSERT INTO `sys_operation` VALUES ('3C03348B-D91C-4A35-B540-7BB67A153463', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA', 'edit', '编辑', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('3EC41FB8-ED98-41F3-B86D-F7473AEDD002', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'viewDetail', '查看', 'viewDetail', '1');
INSERT INTO `sys_operation` VALUES ('3F844D55-E2AA-43C0-B795-88B5DE2440CF', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57', 'detail', '查看', 'detail', '2');
INSERT INTO `sys_operation` VALUES ('452593FA-A9D6-4F77-B880-21F97E9688D1', '894F3E00-FF69-494E-A072-5D63D185097E', 'confirm', '确认', 'confirm', '6');
INSERT INTO `sys_operation` VALUES ('495A111E-EE16-452E-92D9-56CF97BD6388', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'close', '关闭', 'close', '7');
INSERT INTO `sys_operation` VALUES ('496E22A5-67BC-40C9-9F99-6FDB6193B441', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'save', '保存', 'save', '2');
INSERT INTO `sys_operation` VALUES ('4A64B8DC-7370-435D-8F10-612EC45FFCD8', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'reConfirm', '反确认', 'reConfirm', '6');
INSERT INTO `sys_operation` VALUES ('4AE7E5CB-848D-483C-A59A-BCD1117A7356', '093413BA-1ABC-464F-AEA0-13FF82591D22', 'detail', '查看', 'detail', '3');
INSERT INTO `sys_operation` VALUES ('4B67525B-3AAA-4B03-83DA-826727087253', '880B1007-9A06-4A8D-9451-11DD12A7410E', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('4E2CAF22-3DCB-415D-ABC5-0E6BC2B50BFE', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'remove', '移除', 'remove', '3');
INSERT INTO `sys_operation` VALUES ('50119419-32C8-4C57-A4CA-639EE149119E', 'CF361BBD-411A-4EED-A246-3D10B318F6F9', 'save', '保存', 'save', '1');
INSERT INTO `sys_operation` VALUES ('52C7239C-39B8-4CCC-94A9-1A2BFD7B48A0', '093413BA-1ABC-464F-AEA0-13FF82591D22', 'delete', '删除', 'delete', '6');
INSERT INTO `sys_operation` VALUES ('5524CB68-469B-4163-99EF-1EDDE8E13AFD', '894F3E00-FF69-494E-A072-5D63D185097E', 'deleteBtn', '删除从', 'deleteBtn', '10');
INSERT INTO `sys_operation` VALUES ('567FBEE1-306B-4EC1-8688-906D5629C3D4', '894F3E00-FF69-494E-A072-5D63D185097E', 'createBtn', '新增从', 'createBtn', '8');
INSERT INTO `sys_operation` VALUES ('579FCABC-B5D5-4157-91DD-D638943C3BCF', '12691874-9834-440C-929A-87C699B7AA4E', 'save', '保存', 'save', '2');

INSERT INTO `sys_operation` VALUES ('5EEFEF32-70DB-4763-857F-7935E418E570', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'reConfirm', '反确认', 'reConfirm', '4');
INSERT INTO `sys_operation` VALUES ('5F66D00B-C5F3-4D1E-8134-9F437582C987', 'F5FFE7AF-18EE-41B1-BDFD-E6A28ABB493B', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('6224A19C-0FC4-439F-B5C8-79E86557B29E', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'transfer', '迁移', 'transfer', '11');
INSERT INTO `sys_operation` VALUES ('64973A6C-3E46-42B9-97B4-C56A8B49BB7F', 'EA9CD938-A197-4EBD-99CF-B0CF95F8756E', 'save', '保存', 'save', '2');
INSERT INTO `sys_operation` VALUES ('6A6591AB-3717-4F50-A609-18B332413E86', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'reConfirm', '反确认', 'reConfirm', '7');
INSERT INTO `sys_operation` VALUES ('6BF7A157-D333-4F40-8612-F61D3FD4D258', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'refreshImgBtn', '刷新', 'refresh', '2');
INSERT INTO `sys_operation` VALUES ('6C24D134-9C82-4516-BBBD-1EDB00CBA951', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'confirm', '确认', 'confirm', '7');
INSERT INTO `sys_operation` VALUES ('6D723C6A-139A-4AB1-A016-FC5150AA99A9', '613C474D-442A-4ED2-A426-F11E66CCD961', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('7683F231-6534-4ADA-863E-F7637017351A', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57', 'confirm', '确认', 'confirm', '3');
INSERT INTO `sys_operation` VALUES ('7FD6B3B9-C6D1-4979-BF3A-922FC1DC01B7', 'EA9CD938-A197-4EBD-99CF-B0CF95F8756E', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('814E1493-C816-4DAD-9C8E-4367DC3A233B', 'CB2467A4-50FC-42A7-A66A-A431DB719E00', 'save', '保存', 'save', '1');
INSERT INTO `sys_operation` VALUES ('82384F1D-4CD1-4DF3-9121-B80EE0B15973', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('85705567-2B39-4661-8995-4013A7E80E76', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA', 'phoneCall', '电话拜访', 'phoneCall', '3');
INSERT INTO `sys_operation` VALUES ('863E15B1-7DF1-4A73-8F19-1F9CD0B7EFAB', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'copy', '复制', 'copy', '6');
INSERT INTO `sys_operation` VALUES ('88857AB5-D08A-4C50-B84E-4B9112B59CF6', 'CB2467A4-50FC-42A7-A66A-A431DB719E00', 'back', '返回', 'back', '2');
INSERT INTO `sys_operation` VALUES ('88F99F91-384F-44B1-9EC8-E05F6724A9E8', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'transfer', '迁移', 'transfer', '4');
INSERT INTO `sys_operation` VALUES ('8A2C52B7-7F4C-415D-8C46-48AAD36D01D6', '894F3E00-FF69-494E-A072-5D63D185097E', 'resubmit', '反提交', 'resubmit', '5');
INSERT INTO `sys_operation` VALUES ('8B67392D-4126-404C-B7F3-338767327D3E', 'EA9CD938-A197-4EBD-99CF-B0CF95F8756E', 'confirm', '确认', 'confirm', '4');

INSERT INTO `sys_operation` VALUES ('90FC7016-B3DA-489D-A2B4-5DBCB0AD2047', 'EA9CD938-A197-4EBD-99CF-B0CF95F8756E', 'back', '返回', 'back', '6');
INSERT INTO `sys_operation` VALUES ('90FCC64A-FFAD-4E8B-BBAE-EA0AE4C27CB9', '76FF0E56-DA31-4399-82BF-8B1C9B253872', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('910D3B2D-0019-4FAD-8593-43710B416A3B', '093413BA-1ABC-464F-AEA0-13FF82591D22', 'generateOrder', '生成订单', 'generateOrder', '2');
INSERT INTO `sys_operation` VALUES ('91E10CCF-A520-4712-A82C-ADCB8CFA0921', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA', 'delete', '删除', 'delete', '5');
INSERT INTO `sys_operation` VALUES ('9AA13920-43B2-4AEF-9D9C-9C2F71F34043', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'down', '下移', 'down', '6');
INSERT INTO `sys_operation` VALUES ('9E812AE0-CFE4-4891-B7BF-22A22B643543', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'save', '保存', 'save', '3');
INSERT INTO `sys_operation` VALUES ('A2334908-E3F6-4896-B209-5856B541BC82', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA', 'detail', '查看', 'detail', '4');
INSERT INTO `sys_operation` VALUES ('A2CD34E9-9D24-4258-9623-BB22B184AEFB', '1B39CF16-2A87-4429-B3F4-34E6D8AFD8DF', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('A3D11759-6C21-4B36-9BA2-2CE232C39085', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'generateClue', '生成线索', 'generateClue', '6');
INSERT INTO `sys_operation` VALUES ('A4F18B90-AEBA-4CE1-AA52-B534C4F79CA2', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'save', '保存', 'save', '2');
INSERT INTO `sys_operation` VALUES ('A80074A6-7BA6-479D-9CDD-768983177E06', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'confirm', '确认', 'confirm', '8');
INSERT INTO `sys_operation` VALUES ('AD843A54-9F8B-4F34-A5E3-6C2BF0537381', '12691874-9834-440C-929A-87C699B7AA4E', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('AEA07454-68B1-453D-ABC3-C6CD10026CFA', '894F3E00-FF69-494E-A072-5D63D185097E', 'close', '关闭', 'close', '1');
INSERT INTO `sys_operation` VALUES ('AEEDFDAB-7802-492D-B369-2DEE350208B5', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'detail', '查看', 'detail', '1');
INSERT INTO `sys_operation` VALUES ('AFEEC524-3AAB-41B8-A4DA-281B67EC3820', '613C474D-442A-4ED2-A426-F11E66CCD961', 'sure', '确定', 'sure', '1');
INSERT INTO `sys_operation` VALUES ('B32A83C9-7AE1-4C7E-B46C-4B0222CDDCAA', '7CB942DC-6F48-478B-A496-ACCF0D699F53', 'save', '保存', 'save', '2');
INSERT INTO `sys_operation` VALUES ('B43973AC-D639-4B90-A547-C2B9D90488FF', 'F5FFE7AF-18EE-41B1-BDFD-E6A28ABB493B', 'choice', '选择', 'choice', '1');
INSERT INTO `sys_operation` VALUES ('B8022643-0966-4493-84BA-23A77B09AD2C', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'editBtn', '编辑从', 'editBtn', '6');
INSERT INTO `sys_operation` VALUES ('C3958897-FFC1-45EE-99D3-EC1240D08DE1', 'EA9CD938-A197-4EBD-99CF-B0CF95F8756E', 'reConfirm', '反确认', 'reConfirm', '5');
INSERT INTO `sys_operation` VALUES ('C3CD32E7-9B48-4DEF-8563-68128F632570', '49907EFA-315D-4D73-8775-D677118B88F9', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('C566F2F1-28AE-4A61-9361-0F990327900E', '7CB942DC-6F48-478B-A496-ACCF0D699F53', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('C5D919FA-A812-4AB2-AED5-81BE5B31B79D', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'edit', '编辑', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('C8BEFAFB-0E45-431A-BCBC-1AB2819BCA91', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'createBtn', '新增从', 'createBtn', '7');
INSERT INTO `sys_operation` VALUES ('CBD4685F-AC93-4574-95CB-BFF7E900BD2E', '093413BA-1ABC-464F-AEA0-13FF82591D22', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('CC288486-F786-4815-8BFB-A6C1374A4BCB', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'confirm', '确认', 'confirm', '5');
INSERT INTO `sys_operation` VALUES ('CC9F68D6-81BB-4EBF-8BDC-2E8CA781D254', '763FBBC1-19D4-4E58-8BC2-423672948D42', 'choice', '选择', 'choice', '1');
INSERT INTO `sys_operation` VALUES ('CEA130F1-3BD5-47D9-84E5-2B2003D8F20B', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA', 'create', '新增', 'create', '1');
INSERT INTO `sys_operation` VALUES ('CF4DB6EE-7AD3-481D-90C0-125CA6329E4C', '76FF0E56-DA31-4399-82BF-8B1C9B253872', 'back', '返回', 'back', '4');
INSERT INTO `sys_operation` VALUES ('D03779DE-4B3A-4B60-9C1A-E7A5DB7EEB76', '2B0866CA-1264-4989-B178-3868EDC725A5', 'add', '添加', 'add', '1');
INSERT INTO `sys_operation` VALUES ('D1D74810-6844-44A8-B2E3-9EC50BD33848', 'E9F21041-5A09-45F6-8B06-5905A5400E69', 'save', '保存', 'save', '1');
INSERT INTO `sys_operation` VALUES ('D269A98B-6887-4298-98A2-7B1E06BC164E', '4D2E1F5C-8BE0-4592-90BD-7D4B7440B388', 'choice', '选择', 'choice', '1');
INSERT INTO `sys_operation` VALUES ('D28C6156-32D2-44C4-90D4-63DC05CD6407', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'create', '新增', 'create', '4');
INSERT INTO `sys_operation` VALUES ('D413878A-2092-40D8-B946-1B6F90D2F194', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'back', '返回', 'back', '5');
INSERT INTO `sys_operation` VALUES ('D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D', '12691874-9834-440C-929A-87C699B7AA4E', 'back', '返回', 'back', '3');
INSERT INTO `sys_operation` VALUES ('DCC185BC-660A-4172-A5D4-C2B40EE0833D', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8', 'delete', '删除', 'delete', '2');
INSERT INTO `sys_operation` VALUES ('E3498CDE-B7C0-4E04-BB61-76D5AD49019C', '61BD908F-3585-47F8-B9EC-AD7E3C6944D9', 'refresh', '刷新', 'refresh', '1');
INSERT INTO `sys_operation` VALUES ('E4EFE717-04BB-4141-BC93-25B349B8AE28', '763FBBC1-19D4-4E58-8BC2-423672948D42', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('E6CFF307-AD95-42FA-8234-C551BDB7BFA1', '3890BD46-F782-4036-9FC3-E6DA46140C5D', 'edit', '编辑', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('E723E562-9B05-4778-8850-4DF9518B8DD4', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'deleteBtn', '删除从', 'deleteBtn', '7');
INSERT INTO `sys_operation` VALUES ('E9F11BB9-A6CC-47AB-B475-B7B54D898C8D', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57', 'delete', '删除', 'delete', '4');
INSERT INTO `sys_operation` VALUES ('EC67A415-CC8F-45B5-A28D-797D84435841', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344', 'submit', '提交', 'submit', '4');
INSERT INTO `sys_operation` VALUES ('EE485410-75AA-43E4-83F6-631412DC411F', 'E9F21041-5A09-45F6-8B06-5905A5400E69', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('F057D60B-CE29-42B9-8CF6-80E0DC83EBF7', '9B2D4066-1BB1-42D6-8D6B-3AC9954D9A22', 'sure', '确定', 'sure', '1');
INSERT INTO `sys_operation` VALUES ('F1BDCB3B-BED2-4B69-9037-3C22C288D949', '49907EFA-315D-4D73-8775-D677118B88F9', 'sure', '确定', 'sure', '1');
INSERT INTO `sys_operation` VALUES ('F4C8FD10-7ED1-4912-B7A5-92DFA7070159', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7', 'createBtn', '新增从', 'createBtn', '8');
INSERT INTO `sys_operation` VALUES ('FC94D31D-8BF6-4413-BCA9-C2B817905E6A', '9B2D4066-1BB1-42D6-8D6B-3AC9954D9A22', 'close', '关闭', 'close', '2');
INSERT INTO `sys_operation` VALUES ('FCD061B9-45BC-4E7A-86CB-B179E30B0A76', '894F3E00-FF69-494E-A072-5D63D185097E', 'edit', '编辑', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('FDF88E39-2896-4E6D-B4CF-B107C6B869E6', '2B0866CA-1264-4989-B178-3868EDC725A5', 'delete', '删除', 'delete', '2');
INSERT INTO `sys_operation` VALUES ('FEE4BDAE-A1ED-47E1-9065-22285EB8F57B', '894F3E00-FF69-494E-A072-5D63D185097E', 'saveBtn', '保存从', 'saveBtn', '9');
INSERT INTO `sys_operation` VALUES ('FF12FE7A-BD11-448A-BCC9-E0D23FAA33A6', '894F3E00-FF69-494E-A072-5D63D185097E', 'reConfirm', '反确认', 'reConfirm', '7');
INSERT INTO `sys_operation` VALUES ('FFDB914C-B3F6-4871-8AEE-515A1081ACF0', '3890BD46-F782-4036-9FC3-E6DA46140C5D', 'delete', '删除', 'delete', '4');

-- ----------------------------
-- Table structure for wcm_general_group
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_group`;
CREATE TABLE `wcm_general_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_NAME` varchar(64) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_ORDERNO` int(11) DEFAULT NULL,
  `GRP_IS_SYSTEM` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_DESC` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_EXTS` varchar(128) DEFAULT NULL,
  `GRP_RES_SIZE_LIMIT` varchar(32) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_group
-- ----------------------------
INSERT INTO `wcm_general_group` VALUES ('77777777-7777-7777-7777-777777777777', '附件目录', '', null, '', '', '', '', '');
INSERT INTO `wcm_general_group` VALUES ('A6018D88-8345-46EE-A452-CE362FAC72E2', '视频文件', '77777777-7777-7777-7777-777777777777', '1', 'Y', '视频', '*.mp4;*.3gp;*.wmv;*.avi;*.rm;*.rmvb;*.flv', '100MB', '');
INSERT INTO `wcm_general_group` VALUES ('CF35D1E6-102E-428A-B39C-0072D491D5B1', '业务附件', '77777777-7777-7777-7777-777777777777', '2', 'Y', '所有资源', '*.*', '2MB', '');

-- ----------------------------
-- Table structure for wcm_general_resource
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_resource`;
CREATE TABLE `wcm_general_resource` (
  `RES_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_NAME` varchar(64) DEFAULT NULL,
  `RES_SHAREABLE` varchar(32) DEFAULT NULL,
  `RES_LOCATION` varchar(256) DEFAULT NULL,
  `RES_SIZE` varchar(64) DEFAULT NULL,
  `RES_SUFFIX` varchar(32) DEFAULT NULL,
  `RES_DESCRIPTION` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_resource
-- ----------------------------
