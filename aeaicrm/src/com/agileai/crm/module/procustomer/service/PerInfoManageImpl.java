package com.agileai.crm.module.procustomer.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class PerInfoManageImpl
        extends StandardServiceImpl
        implements PerInfoManage {
    public PerInfoManageImpl() {
        super();
        
    }

	@Override
	public List<DataRow> findPerLabelsRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findPerLabelsRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
