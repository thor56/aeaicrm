package com.agileai.crm.module.procustomer.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface OrgInfoManage
        extends StandardService {
	void createVisitRecord(DataParam param);
	void createCustomerRecord(DataParam param);
	void changeStateRecord(DataParam param);
	List<DataRow> findCustomerRecords(DataParam param);
	List<DataRow> findOrgLabelsRecords(DataParam param);
	
}
