package com.agileai.crm.module.procustomer.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface PerInfoManage
        extends StandardService {
	List<DataRow> findPerLabelsRecords(DataParam param);
}
