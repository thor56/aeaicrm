package com.agileai.crm.module.procustomer.handler;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class ProCustomerHandler extends BaseHandler{
    public ProCustomerHandler() {
        super();
    }
	public ViewRenderer prepareDisplay(DataParam param){
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
    	this.setAttribute("currentTabId", param.get("currentTabId"));
    	setAttribute("currentTabId",getAttributeValue("currentTabId", "0"));
    }
}
