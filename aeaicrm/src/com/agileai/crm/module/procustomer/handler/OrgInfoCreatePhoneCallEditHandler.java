package com.agileai.crm.module.procustomer.handler;

import java.util.Date;
import java.util.List;

import com.agileai.crm.module.procustomer.service.OrgInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class OrgInfoCreatePhoneCallEditHandler extends StandardEditHandler {
	public OrgInfoCreatePhoneCallEditHandler() {
		super();
		this.serviceId = buildServiceId(OrgInfoManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);	
		}
		if("doCreatePhoneCallAction".equals(operaType) || "doCreatePhoneCallAction,detail".equals(operaType)){
			User user = (User) this.getUser();
			this.setAttribute("ORG_ID",param.get("ORG_ID"));
			this.setAttribute("VISIT_USER_ID",user.getUserId());
			this.setAttribute("VISIT_FILL_ID",user.getUserId());
			this.setAttribute("VISIT_FILL_NAME",user.getUserName());
			String date = DateUtil.getDateByType(
					DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
			this.setAttribute("VISIT_FILL_TIME",
					this.getAttribute("VISIT_FILL_TIME",date));
			this.setAttribute("VISIT_RECEPTION_PHONE", param.get("ORG_CONTACT_WAY"));
			Date visitDate = (Date) new Date();
			setAttribute("VISIT_DATE",visitDate);
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
	 	setAttribute("VISIT_TYPE",
                FormSelectFactory.create("VISIT_TYPE")
                                 .addSelectedValue(getAttributeValue("VISIT_TYPE",
                                                                          "1")));
	 	setAttribute("VISIT_EFFECT", FormSelectFactory.create("VISIT_EFFECT")
				.addSelectedValue(getAttributeValue("VISIT_EFFECT", "")));
	 	setAttribute(
				"VISIT_STATE",
				FormSelectFactory.create("CUST_STATE").addSelectedValue(
						getAttributeValue("VISIT_STATE", "init")));
		setAttribute(
				"VISIT_RECEPTION_SEX",
				FormSelectFactory.create("USER_SEX").addSelectedValue(
						getAttributeValue("VISIT_RECEPTION_SEX", "M")));
		User user = (User) this.getUser();
		this.setAttribute("CLUE_CREATE_NAME",
				this.getAttribute("CLUE_CREATE_NAME", user.getUserName()));
		this.setAttribute("CLUE_CREATE_MAN",
				this.getAttribute("CLUE_CREATE_MAN", user.getUserId()));
		String ccrtDate = (String) this.getAttribute("CLUE_CREATE_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));
		this.setAttribute("CLUE_CREATE_TIME", ccrtDate);
		setAttribute("CLUE_SOURCE", FormSelectFactory.create("CLUE_SOURCE")
				.addSelectedValue(getOperaAttributeValue("CLUE_SOURCE", "")));
	}

	@PageAction
	public ViewRenderer createVisit(DataParam param) {
		if(param.get("VISIT_RECEPTION_NAME").equals("")){
			param.put("VISIT_RECEPTION_NAME", "未知");
		}
		((OrgInfoManage) getService()).createVisitRecord(param);
		return new RedirectRenderer(getHandlerURL("OrgInfoManageList"));
	}
	
	@PageAction
	public ViewRenderer doCreateClueCustomerAction(DataParam param) {
			storeParam(param);
			String a = "ClueInfoCreateCustomerEdit";
			return new DispatchRenderer(getHandlerURL(a) + "&"
					+ OperaType.KEY + "=doCreateClueCustomerAction&comeFrome=doCreateClueCustomerAction");
	}
	
	@PageAction
	public ViewRenderer check(DataParam param) {
		String responseText = SUCCESS;
		DataParam orgParam =  new DataParam("ORG_ID", param.get("ORG_ID"));
		List<DataRow> customerRecords = ((OrgInfoManage) getService()).findCustomerRecords(orgParam);
		if(customerRecords.size() > 0){
			responseText = FAIL;
		}
		return new AjaxRenderer(responseText);
	}
	
	public ViewRenderer doBackAction(DataParam param) {
		return new RedirectRenderer(getHandlerURL("OrgInfoManageList"));
	}

	protected StandardService getService() {
		return (StandardService) this.lookupService(this.getServiceId());
	}
}
