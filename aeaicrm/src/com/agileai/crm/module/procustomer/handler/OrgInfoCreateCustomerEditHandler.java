package com.agileai.crm.module.procustomer.handler;

import java.util.Date;

import com.agileai.crm.module.procustomer.service.OrgInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class OrgInfoCreateCustomerEditHandler extends StandardEditHandler {
	public OrgInfoCreateCustomerEditHandler() {
		super();
		this.serviceId = buildServiceId(OrgInfoManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		param.put("ORG_ID", param.get("orgId"));
		DataRow record = getService().getRecord(param);
		DataParam showParam = new DataParam();
		showParam.put("CUST_NAME",record.get("ORG_NAME"));
		showParam.put("CUST_INTRODUCE",record.get("ORG_INTRODUCTION"));
		record.putAll(showParam);
		this.setAttributes(record);	
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	protected void processPageAttributes(DataParam param) {
	 	setAttribute("CUST_STATE",
                FormSelectFactory.create("CUST_STATE")
                                 .addSelectedValue(getAttributeValue("CUST_STATE",
                                                                          "init")));
		User user = (User) this.getUser();
		this.setAttribute("CUST_CREATE_NAME",
				this.getAttribute("CUST_CREATE_NAME", user.getUserName()));
		this.setAttribute("CUST_CREATE_ID",
				this.getAttribute("CUST_CREATE_ID", user.getUserId()));
		String ccrtTime = (String) this.getAttribute("CUST_CREATE_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));
		this.setAttribute("CUST_CREATE_TIME", ccrtTime);
	}
	@PageAction
	public ViewRenderer createCustomer(DataParam param) {
		((OrgInfoManage) getService()).createCustomerRecord(param);
		String responseText = param.get("CUST_ID");
		return new AjaxRenderer(responseText);
	}
	protected StandardService getService() {
		return (StandardService) this.lookupService(this.getServiceId());
	}
	
	
}
