package com.agileai.crm.module.procustomer.handler;

import java.util.Date;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.module.procustomer.service.PerInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class PerInfoManageEditHandler extends StandardEditHandler {
	public PerInfoManageEditHandler() {
		super();
		this.listHandlerClass = PerInfoManageListHandler.class;
		this.serviceId = buildServiceId(PerInfoManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)) {
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);
		}
		if ("insert".equals(operaType)) {
			setAttribute("doEdit8Save", true);
		}
		if ("update".equals(operaType) || "detail".equals(operaType)) {
			DataRow record = getService().getRecord(param);
			if (record != null) {
				setAttribute("doEdit8Save", true);
			}
			this.setAttributes(record);
		}
		String date = DateUtil.getDateByType(
				DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
		this.setAttribute("PER_UPDATE_TIME", date);
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
		setAttribute("PER_SEX", FormSelectFactory.create("PER_SEX")
				.addSelectedValue(getOperaAttributeValue("PER_SEX", "UNKONW")));
		setAttribute("PER_STATE", FormSelectFactory.create("PER_STATE")
				.addSelectedValue(getOperaAttributeValue("PER_STATE", "0")));
		User user = (User) this.getUser();
		this.setAttribute("PER_CREATER_NAME",
				this.getAttribute("PER_CREATER_NAME", user.getUserName()));
		this.setAttribute("PER_CREATER",
				this.getAttribute("PER_CREATER", user.getUserId()));
		String pcrtDate = (String) this.getAttribute("PER_CREATE_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));
		if (this.getAttribute("PER_CREATE_TIME") == null) {
			this.setAttribute("PER_CREATE_TIME", pcrtDate);
		}
	}

	protected void initParameters(DataParam param) {
		initParamItem(param, "PER_NAME", "");
		initParamItem(param, "PER_BELONG_ORG", "");
	}

	public ViewRenderer doSaveAction(DataParam param) {
		String operateType = param.get(OperaType.KEY);
		String responseText = SUCCESS;
		String perName = param.get("PER_NAME");
		String perEmail = param.get("PER_EMAIL");
		if (OperaType.CREATE.equals(operateType)) {
			DataParam repeatParam = new DataParam("PER_NAME", perName);
			DataRow record = getService().getRecord(repeatParam);
			
			boolean repeat = false;
			if(record!=null){
				if(!record.get("PER_ID").equals(param.get("PER_ID"))){
					repeat = true;
					responseText = "repeatName";
				}
			}
			if(!repeat){
				if (!StringUtil.isNullOrEmpty(perEmail)) {
					DataParam repeatParam1 = new DataParam("PER_EMAIL", perEmail);
					DataRow record1 = getService().getRecord(repeatParam1);
					if(record1 != null){
						if(!record1.get("PER_ID").equals(param.get("PER_ID"))){
							repeat = true;
							responseText = "repeatMail";
						} 
					}
				} 
			}
			if (repeat){
				return new AjaxRenderer(responseText);
			}else{
				getService().createRecord(param);
			}
		} else if (OperaType.UPDATE.equals(operateType)) {
			DataParam repeatParam = new DataParam("PER_NAME", perName);
			DataRow record = getService().getRecord(repeatParam);
			
			boolean repeat = false;
			if(record != null){
				if(!record.get("PER_ID").equals(param.get("PER_ID"))){
					repeat = true;
					responseText = "repeatName";
				}
			}
			if (!repeat){
				if(!StringUtil.isNullOrEmpty(perEmail)){
					DataParam repeatParam1 = new DataParam("PER_EMAIL", perEmail);
					DataRow record1 = getService().getRecord(repeatParam1);
					if(record1 != null){
						if(!record1.get("PER_ID").equals(param.get("PER_ID"))){
							repeat = true;
							responseText = "repeatMail";
						}
					}	
				}
			}
			if (repeat){
				return new AjaxRenderer(responseText);
			}else{
				getService().updateRecord(param);
			}
		}
		return new AjaxRenderer(responseText);
	}

	protected PerInfoManage getService() {
		return (PerInfoManage) this.lookupService(this.getServiceId());
	}
}
