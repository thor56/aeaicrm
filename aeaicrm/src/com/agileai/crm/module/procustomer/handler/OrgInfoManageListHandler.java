package com.agileai.crm.module.procustomer.handler;

import java.util.Date;
import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.module.procustomer.service.OrgInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class OrgInfoManageListHandler extends StandardListHandler {
	public OrgInfoManageListHandler() {
		super();
		this.editHandlerClazz = OrgInfoManageEditHandler.class;
		this.serviceId = buildServiceId(OrgInfoManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		setAttribute("doEdit8Save", true);
		setAttribute("createPhoneCall", true);
		setAttribute("doDel", true);
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		String ORG_LABELS = param.get("ORG_LABELS");
		String sqlSyntax = " ";
		
		if (!StringUtil.isNullOrEmpty(ORG_LABELS)){
			if ("Unlabeled".equals(ORG_LABELS)){
				sqlSyntax = " and (a.ORG_LABELS is null or a.ORG_LABELS = '')";
			}
			else if ("Marked".equals(ORG_LABELS)){
				sqlSyntax = " and (a.ORG_LABELS is not null and a.ORG_LABELS != '')";
			}else{
				sqlSyntax = " and a.ORG_LABELS='"+ORG_LABELS+"'";
			}
		}
		param.put("sqlSyntax",sqlSyntax);
		
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
		initMappingItem("ORG_STATE", 
				FormSelectFactory.create("PER_STATE").getContent());
		initMappingItem("ORG_CLASSIFICATION", 
				FormSelectFactory.create("ORG_CLASSIFICATION").getContent());
		setAttribute("orgState", FormSelectFactory.create("PER_STATE")
				.addSelectedValue(param.get("orgState")));
		setAttribute("orgClassification", FormSelectFactory.create("ORG_CLASSIFICATION")
				.addSelectedValue(param.get("orgClassification")));
		
		FormSelect orgLavelsFormSelect = new FormSelect();
		List<DataRow> records = getService().findOrgLabelsRecords(param);
		DataRow unlabeled = new DataRow("CODE_ID","Unlabeled","CODE_NAME","未标记");
		DataRow marked = new DataRow("CODE_ID","Marked","CODE_NAME","已标记");
		records.add(0, unlabeled);
		records.add(1, marked);
		orgLavelsFormSelect.setKeyColumnName("CODE_ID");
		orgLavelsFormSelect.setValueColumnName("CODE_NAME");
		orgLavelsFormSelect.putValues(records);
		String selectedValue = this.getAttributeValue("ORG_LABELS","");
		setAttribute("ORG_LABELS", orgLavelsFormSelect.addSelectedValue(selectedValue));
	}

	protected void initParameters(DataParam param) {
		initParamItem(param, "orgState", "0");
		initParamItem(param, "ORG_TYPE", "");
		initParamItem(param, "ORG_CREATER_NAME", "");
		initParamItem(param, "sdate", "");
		initParamItem(
				param,
				"edate",
				DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,
						DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1)));
	}

	@PageAction
	public ViewRenderer doCreatePhoneCallAction(DataParam param) {
		storeParam(param);
		String a = "OrgInfoCreatePhoneCallEdit";
		return new DispatchRenderer(getHandlerURL(a) + "&" + OperaType.KEY
				+ "=doCreatePhoneCallAction&comeFrome=doCreatePhoneCallAction");
	}

	protected OrgInfoManage getService() {
		return (OrgInfoManage) this.lookupService(this.getServiceId());
	}

}
