package com.agileai.crm.module.procustomer.handler;

import java.util.Date;
import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.module.procustomer.service.PerInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class PerInfoManageListHandler
        extends StandardListHandler {
    public PerInfoManageListHandler() {
        super();
        this.editHandlerClazz = PerInfoManageEditHandler.class;
        this.serviceId = buildServiceId(PerInfoManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param){
    	setAttribute("doEdit8Save", true);
    	setAttribute("doDel", true);
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		String PER_LABELS = param.get("PER_LABELS");
		String sqlSyntax = " ";
		
		if (!StringUtil.isNullOrEmpty(PER_LABELS)){
			if ("Unlabeled".equals(PER_LABELS)){
				sqlSyntax = " and (a.PER_LABELS is null or a.PER_LABELS = '')";
			}
			else if ("Marked".equals(PER_LABELS)){
				sqlSyntax = " and (a.PER_LABELS is not null and a.PER_LABELS != '')";
			}else{
				sqlSyntax = " and a.PER_LABELS='"+PER_LABELS+"'";
			}
		}
		param.put("sqlSyntax",sqlSyntax);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
		initMappingItem("PER_SEX",
                   FormSelectFactory.create("PER_SEX").getContent());
		initMappingItem("PER_STATE",
           FormSelectFactory.create("PER_STATE").getContent());
		setAttribute("PER_STATE",
           FormSelectFactory.create("PER_STATE")
                            .addSelectedValue(param.get("PER_STATE")));
		FormSelect orgLavelsFormSelect = new FormSelect();
		List<DataRow> records = getService().findPerLabelsRecords(param);
		DataRow unlabeled = new DataRow("CODE_ID","Unlabeled","CODE_NAME","未标记");
		DataRow marked = new DataRow("CODE_ID","Marked","CODE_NAME","已标记");
		records.add(0, unlabeled);
		records.add(1, marked);
		orgLavelsFormSelect.setKeyColumnName("CODE_ID");
		orgLavelsFormSelect.setValueColumnName("CODE_NAME");
		orgLavelsFormSelect.putValues(records);
		String selectedValue = this.getAttributeValue("PER_LABELS","");
		setAttribute("PER_LABELS", orgLavelsFormSelect.addSelectedValue(selectedValue));
		this.setAttribute("currentTabId", param.get("currentTabId"));
		setAttribute("currentTabId",getAttributeValue("currentTabId", "0"));
    }

    protected void initParameters(DataParam param) {
    	initParamItem(param, "PER_STATE", "0");
        initParamItem(param, "PER_SEX", "");
        initParamItem(param, "PER_CREATER_NAME", "");
        initParamItem(param,"sdate","");
		initParamItem(param,"edate",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1)));
    }
    protected PerInfoManage getService() {
        return (PerInfoManage) this.lookupService(this.getServiceId());
    }
}
