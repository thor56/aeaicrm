package com.agileai.crm.module.orderinfo.service;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface OrderInfoManage
        extends MasterSubService {

	void computeTotalMoney(String masterRecordId,String entryId);
	void changeStateRecord(DataParam param);
}
