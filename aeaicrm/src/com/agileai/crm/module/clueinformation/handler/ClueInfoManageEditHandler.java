package com.agileai.crm.module.clueinformation.handler;

import java.util.Date;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.ClueInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class ClueInfoManageEditHandler extends StandardEditHandler {
	public ClueInfoManageEditHandler() {
		super();
		this.listHandlerClass = ClueInfoManageListHandler.class;
		this.serviceId = buildServiceId(ClueInfoManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		User user = (User) this.getUser();
		param.put("CLUE_CREATE_TIME", DateUtil.getDateByType(
				DateUtil.YYMMDDHHMI_HORIZONTAL, new Date()));
		param.put("CLUE_CREATE_MAN", user.getUserId());
		if (isReqRecordOperaType(operaType)) {
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);
		}
		if ("insert".equals(operaType)) {
			setAttribute("doEdit8Save", true);
			setAttribute("doAssign", false);
			setAttribute("readOnly", false);
			setAttribute("showSalesman",false);
			setAttribute("doDispose",false);
		}
		if ("update".equals(operaType)) {
			setAttribute("doDispose",false);
			DataRow record = getService().getRecord(param);
			PrivilegeHelper privilegehelper = new PrivilegeHelper(user);
			if(privilegehelper.isTelNetSalesman()){
				setAttribute("doEdit8Save", true);
				setAttribute("doAssign", false);
				setAttribute("readOnly", false);
				setAttribute("showSalesman",false);
				setAttribute("doClaim", false);
			}
			if(privilegehelper.isSalesMan()){
				if("0".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", false);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", false);
				}
				if("1".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", true);
				}
				if("2".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", false);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", false);
				}
				if("3".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", true);
					setAttribute("doDispose",true);
				}
				if("4".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", false);
					setAttribute("doDispose",true);
				}
			}
			if(privilegehelper.isSalesDirector()){
				setAttribute("doDispose",false);
				if("0".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", false);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
				}
				if("1".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", true);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
				}if("2".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
				}if("3".equals(record.get("CLUE_STATE"))||"4".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
					setAttribute("doDispose",true);
				}
			}
			this.setAttributes(record);
		}
		if ("detail".equals(operaType)) {
			setAttribute("showSalesman",false);
			DataRow record = getService().getRecord(param);
			PrivilegeHelper privilegehelper = new PrivilegeHelper(user);
			if(privilegehelper.isTelNetSalesman()){
				setAttribute("doDispose",false);
				if("0".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", true);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", true);
					setAttribute("doPause8Close", false);
				}else{
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", true);
					setAttribute("doPause8Close", false);
				}
			}
			if(privilegehelper.isSalesMan()){
				setAttribute("doDispose",false);
				if("0".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", false);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", false);
				}
				if("1".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",false);
					setAttribute("doClaim", true);
					if(record.get("CLUE_CREATE_MAN").equals(user.getUserId())){
						setAttribute("doClaim", false);
					}
				}
				if("2".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
				}
				if("3".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", true);
					setAttribute("doDispose",true);
				}if("4".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
					setAttribute("doDispose",true);
				}
			}
			if(privilegehelper.isSalesDirector()){
				setAttribute("doDispose",false);
				if("0".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", true);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
				}
				if("1".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", true);
					setAttribute("doAssign", true);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
				}if("2".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", false);
					setAttribute("readOnly", true);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
				}if("3".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", true);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
					setAttribute("doDispose",true);
				}
				if("4".equals(record.get("CLUE_STATE"))){
					setAttribute("doEdit8Save", false);
					setAttribute("doAssign", true);
					setAttribute("readOnly", false);
					setAttribute("showSalesman",true);
					setAttribute("doClaim", false);
					setAttribute("doDispose",true);
				}
			}
			this.setAttributes(record);
		}
		if("doDispose".equals(operaType)){
			DataRow record = getService().getRecord(param);
			setAttribute("doEdit8Save", false);
			setAttribute("readOnly", true);
			setAttribute("doClaim", false);
			setAttribute("doPause8Close",true);
			setAttribute("showSalesman",true);
			setAttribute("doDispose",true);
			this.setAttributes(record);
		}
		if ("doAssignRequestAction".equals(operaType)) {
			DataRow record = getService().getRecord(param);
			PrivilegeHelper privilegehelper = new PrivilegeHelper(user);
			if (privilegehelper.isSalesDirector()) {
				setAttribute("doAssign", true);
				setAttribute("showSalesman",true);
			}
			this.setAttributes(record);

		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
		User user = (User) this.getUser();
		this.setAttribute("CLUE_CREATE_NAME",
				this.getAttribute("CLUE_CREATE_NAME", user.getUserName()));
		this.setAttribute("CLUE_CREATE_MAN",
				this.getAttribute("CLUE_CREATE_MAN", user.getUserId()));
		String ccrtDate = (String) this.getAttribute("CLUE_CREAT_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));

		if (this.getAttribute("CLUE_CREATE_TIME") == null) {
			this.setAttribute("CLUE_CREATE_TIME", ccrtDate);
		}
		this.setAttribute(
				"CLUE_SOURCE",
				FormSelectFactory.create("CLUE_SOURCE").addSelectedValue(
						getOperaAttributeValue("CLUE_SOURCE", "")));
		this.setAttribute("CLUE_STATE", FormSelectFactory.create("CLUE_STATE")
				.addSelectedValue(getAttributeValue("CLUE_STATE", "0")));
	}

	public ViewRenderer doSaveAction(DataParam param) {
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)) {
			getService().createRecord(param);
		} else if (OperaType.UPDATE.equals(operateType)) {
			DataRow record = getService().getRecord(param);
			if(!record.get("CLUE_STATE").equals("0")){
			param.put("CLUE_SALESMAN",record.get("CLUE_SALESMAN"));
			param.put("CLUE_GET_TIME",record.get("CLUE_GET_TIME"));
			param.put("CLUE_DISPOSE_DEL",record.get("CLUE_DISPOSE_DEL"));
			}
			getService().updateRecord(param);
		} 
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}

	@PageAction
	public ViewRenderer doCreateClueCustomerAction(DataParam param) {
		storeParam(param);
		String handlerId = "ClueInfoCreateCustomerEdit";
		return new DispatchRenderer(
				getHandlerURL(handlerId)
						+ "&"
						+ OperaType.KEY
						+ "=doCreateClueCustomerAction&comeFrome=doCreateClueCustomerAction");
	}

	@PageAction
	public ViewRenderer doAssign(DataParam param) {
		param.put("CLUE_STATE", "1");
		param.put("CLUE_GET_TIME","");
		getService().assignRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}

	@PageAction
	public ViewRenderer doClaim(DataParam param) {
		String clueId = param.get("CLUE_ID");
		getService().doClaimRecord(clueId);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	@PageAction
	public ViewRenderer doPause(DataParam param) {
		param.put("CLUE_STATE", "3");
		getService().doDisposeRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	@PageAction
	public ViewRenderer doClose(DataParam param) {
		param.put("CLUE_STATE", "4");
		getService().doDisposeRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	protected ClueInfoManage getService() {
		return (ClueInfoManage) this.lookupService(this.getServiceId());
	}
}
