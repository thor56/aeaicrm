package com.agileai.crm.module.clueinformation.handler;

import java.util.Date;
import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.ClueInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class ClueInfoManageListHandler extends StandardListHandler {
	public ClueInfoManageListHandler() {
		super();
		this.editHandlerClazz = ClueInfoManageEditHandler.class;
		this.serviceId = buildServiceId(ClueInfoManage.class);
	}

	public ViewRenderer prepareDisplay(DataParam param) {

		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		setAttribute("currentUserId",user.getUserId());
		if (privilegeHelper.isSalesDirector()) {
			setAttribute("doEdit8Save", true);
			setAttribute("doCreateOpp", true);
			setAttribute("doAssign", true);
			setAttribute("doClaim8doPause8doClose", true);
			setAttribute("isSalesDirector",true);
		}else if (!privilegeHelper.isSalesDirector()) {
			param.put("currentUsersId",user.getUserId());
			if (privilegeHelper.isSalesMan()) {
				setAttribute("doEdit8Save", true);
				setAttribute("doCreateOpp", true);
				setAttribute("doAssign", false);
				setAttribute("doClaim8doPause8doClose", true);
			}else{
				setAttribute("doEdit8Save", false);
				setAttribute("doCreateOpp", false);
				setAttribute("doAssign", false);
				setAttribute("doClaim8doPause8doClose", false);
			}
		}
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
		setAttribute("CLUE_STATE", FormSelectFactory.create("CLUE_STATE")
				.addSelectedValue(param.get("CLUE_STATE")));
		initMappingItem("CLUE_SOURCE", FormSelectFactory.create("CLUE_SOURCE")
				.getContent());
		initMappingItem("CLUE_STATE", FormSelectFactory.create("CLUE_STATE")
				.getContent());
		setAttribute("CLUE_SOURCE", FormSelectFactory.create("CLUE_SOURCE")
				.addSelectedValue(param.get("CLUE_SOURCE")));

	}

	protected void initParameters(DataParam param) {
		initParamItem(param, "CLUE_STATE", "");
		initParamItem(param, "CLUE_SOURCE", "");
		initParamItem(param, "CUST_ID_NAME", "");
		initParamItem(param, "CLUE_LIKEMAN_NAME", "");
	}

	@PageAction
	public ViewRenderer doAssignRequestAction(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY
				+ "=doAssignRequestAction&comeFrome=doAssignRequestAction");
	}

	@PageAction
	public ViewRenderer doCreateOppAction(DataParam param) {
		storeParam(param);
		String hanlderId = "ClueInfoCreateOppEdit";
		return new DispatchRenderer(getHandlerURL(hanlderId) + "&" + OperaType.KEY
				+ "=doCreateOppAction&comeFrome=doCreateOppAction");
	}

	@PageAction
	public ViewRenderer doClaim(DataParam param) {
		String clueId = param.get("CLUE_ID");
		String date = (String) this.getAttribute("CLUE_GET_TIME", DateUtil
				.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date()));
		param.put("CLUE_GET_TIME", date);
		getService().doClaimRecord(clueId);
		return prepareDisplay(param);
	}

	@PageAction
	public ViewRenderer doDispose(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY
				+ "=doDispose&comeFrome=doDispose");
	}
	protected ClueInfoManage getService() {
		return (ClueInfoManage) this.lookupService(this.getServiceId());
	}
}
