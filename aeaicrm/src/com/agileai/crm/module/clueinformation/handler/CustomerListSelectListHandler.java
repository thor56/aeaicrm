package com.agileai.crm.module.clueinformation.handler;

import com.agileai.crm.cxmodule.CustomerListSelect;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.FormSelectFactory;

public class CustomerListSelectListHandler
        extends PickFillModelHandler {
    public CustomerListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(CustomerListSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    	setAttribute("CUST_INDUSTRY", FormSelectFactory.create("CUST_INDUSTRY")
				.addSelectedValue(param.get("CUST_INDUSTRY")));
    	setAttribute("CUST_PROVINCE", FormSelectFactory.create("PROVINCE")
				.addSelectedValue(param.get("CUST_PROVINCE")));
    	initMappingItem("CUST_INDUSTRY", FormSelectFactory.create("CUST_INDUSTRY")
				.getContent());
//		initMappingItem("CUST_PROVINCE", FormSelectFactory.create("PROVINCE")
//				.getContent());
    }

     protected CustomerListSelect getService() {
        return (CustomerListSelect) this.lookupService(this.getServiceId());
    }
}
