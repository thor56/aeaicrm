package com.agileai.crm.module.clueinformation.handler;

import java.util.Date;

import com.agileai.crm.cxmodule.ClueInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class ClueInfoCreateOppEditHandler extends StandardEditHandler {
	public ClueInfoCreateOppEditHandler() {
		super();
		this.serviceId = buildServiceId(ClueInfoManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if("doCreateOppAction".equals(operaType)){
			if (!isReqRecordOperaType(operaType)){
				DataRow record = getService().getRecord(param);
				DataParam showParam = new DataParam();
				showParam.put("CLUE_ID",param.get("CLUE_ID"));
				showParam.put("CUST_ID",param.get("CUST_ID"));
				showParam.put("CUST_ID_NAME", param.get("CLUE_NAME"));
				showParam.put("CONT_ID_NAME", param.get("CLUE_LIKEMAN_NAME"));
				showParam.put("CLUE_SALESMAN",param.get("CLUE_SALESMAN"));
				showParam.put("CLUE_SALESMAN_NAME", param.get("CLUE_SALESMAN_NAME"));
				showParam.putAll(record);
				this.setAttributes(record);	
			}
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	protected void processPageAttributes(DataParam param) {
		this.setAttribute("CUST_ID_NAME",
				this.getAttribute("CLUE_NAME", param.getString("CLUE_NAME")));
		this.setAttribute("CONT_ID_NAME",
				this.getAttribute("CLUE_LIKEMAN_NAME", param.getString("CLUE_LIKEMAN_NAME")));
		this.setAttribute("CLUE_SALESMAN",
				this.getAttribute("CLUE_SALESMAN", param.getString("CLUE_SALESMAN")));
		this.setAttribute("CLUE_SALESMAN_NAME",
				this.getAttribute("CLUE_SALESMAN_NAME", param.getString("CLUE_SALESMAN_NAME")));
		this.setAttribute("CONT_ID_NAME",
				this.getAttribute("CLUE_LIKEMAN_NAME", param.getString("CLUE_LIKEMAN_NAME")));
		 setAttribute("OPP_STATE",
                 FormSelectFactory.create("OPP_STATE")
                                  .addSelectedValue(getAttributeValue("OPP_STATE",
                                                                           "0")));
		 User user = (User) this.getUser();
			this.setAttribute("OPP_CREATER_NAME",
					this.getAttribute("OPP_CREATER_NAME", user.getUserName()));
			this.setAttribute("OPP_CREATER",
					this.getAttribute("OPP_CREATER", user.getUserId()));
			String ocrtDate = (String) this.getAttribute("OPP_CREATE_TIME",
					DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
							new Date()));
			this.setAttribute("OPP_CREATE_TIME", ocrtDate);
	}
	@PageAction
	public ViewRenderer createOpp(DataParam param) {
		((ClueInfoManage) getService()).createOppRecord(param);
		return new RedirectRenderer(getHandlerURL("ClueInfoManageList"));
	}
	public ViewRenderer doBackAction(DataParam param) {
		return new RedirectRenderer(getHandlerURL("ClueInfoManageList"));
	}
	protected ClueInfoManage getService() {
        return (ClueInfoManage) this.lookupService(this.getServiceId());
    }
	
}
