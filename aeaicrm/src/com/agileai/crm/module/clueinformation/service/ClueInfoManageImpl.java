package com.agileai.crm.module.clueinformation.service;

import java.util.Date;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.ClueInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.util.DateUtil;

public class ClueInfoManageImpl
        extends StandardServiceImpl
        implements ClueInfoManage {
    public ClueInfoManageImpl() {
        super();
    }

	@Override
	public void createCustomerRecord(DataParam param) {
			String statementId = sqlNameSpace+"."+"createCustomerRecord";
			String custId = KeyGenerator.instance().genKey();
	   		param.put("CUST_ID",custId);
			processDataType(param, tableName);
			processPrimaryKeys(param);
			this.daoHelper.insertRecord(statementId, param);
			
			statementId = sqlNameSpace+"."+"getGrpCodeRecord";
			param.put("GRP_CODE","TEMP");
			DataRow result = this.daoHelper.getRecord(statementId, param);
			String grpId = result.getString("GRP_ID");
			statementId = sqlNameSpace+"."+"createCustomerGrpRecord";
			DataParam newParam = new DataParam();
			newParam.put("GRP_ID",grpId);
			newParam.put("CUST_ID",param.get("CUST_ID"));
			this.daoHelper.insertRecord(statementId, newParam);
		}
	public void createOppRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"createOppRecord";
		String clueId = KeyGenerator.instance().genKey();
   		param.put("OPP_ID",clueId);
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
		
	}
	public void assignRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"assignRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
	@Override
	public void doClaimRecord(String clueId) {
		String date = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
		DataParam param = new DataParam("CLUE_ID", clueId,"CLUE_STATE","2","CLUE_GET_TIME",date);
		String statementId = sqlNameSpace+"."+"claimRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
	@Override
	public void doDisposeRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"disposeRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
		
	}
}
