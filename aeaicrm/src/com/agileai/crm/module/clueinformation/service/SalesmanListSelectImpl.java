package com.agileai.crm.module.clueinformation.service;

import java.util.List;

import com.agileai.crm.module.clueinformation.service.SalesmanListSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;

public class SalesmanListSelectImpl
        extends PickFillModelServiceImpl
        implements SalesmanListSelect {
	public static final String SALESMAN_ROLE = "SALESMAN";
	
    public SalesmanListSelectImpl() {
        super();
    }
    public List<DataRow> queryPickFillRecords(DataParam param) {
    	param.put("ROLE_CODE",SALESMAN_ROLE);
		String statementId = sqlNameSpace+"."+queryPickFillRecordsSQL;
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
