package com.agileai.crm.module.clueinformation.service;

import com.agileai.crm.cxmodule.CustomerListSelect;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;

public class CustomerListSelectImpl
        extends PickFillModelServiceImpl
        implements CustomerListSelect {
    public CustomerListSelectImpl() {
        super();
    }
}
