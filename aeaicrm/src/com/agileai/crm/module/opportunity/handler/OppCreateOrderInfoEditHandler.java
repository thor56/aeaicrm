package com.agileai.crm.module.opportunity.handler;

import java.util.Date;

import com.agileai.crm.module.opportunity.service.OppInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class OppCreateOrderInfoEditHandler extends StandardEditHandler{
	public OppCreateOrderInfoEditHandler() {
		super();
		this.listHandlerClass = OppInfoManageListHandler.class;
		this.serviceId = buildServiceId(OppInfoManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if("doCreateOrderAction".equals(operaType)){
			if (!isReqRecordOperaType(operaType)){
				DataRow record = getService().getRecord(param);
				DataParam showParam = new DataParam();
				showParam.put("OPP_ID",param.get("OPP_ID"));
				showParam.put("OPP_NAME",param.get("OPP_NAME"));
				showParam.put("CLUE_SALESMAN", param.get("CLUE_SALESMAN"));
				showParam.put("CLUE_SALESMAN_NAME", param.get("CLUE_SALESMAN_NAME"));
				showParam.putAll(record);
				this.setAttributes(record);	
			}
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	protected void processPageAttributes(DataParam param) {
		this.setAttribute("OPP_NAME",
				this.getAttribute("OPP_NAME", param.getString("OPP_NAME")));
		this.setAttribute("OPP_ID",
				this.getAttribute("OPP_ID", param.getString("OPP_ID")));
	 	setAttribute("ORDER_STATE",
                FormSelectFactory.create("ORDER_STATE")
                                 .addSelectedValue(getAttributeValue("ORDER_STATE",
                                                                          "0")));
	 	this.setAttribute("CLUE_SALESMAN",
				this.getAttribute("CLUE_SALESMAN", param.getString("CLUE_SALESMAN")));
		this.setAttribute("CLUE_SALESMAN_NAME",
				this.getAttribute("CLUE_SALESMAN_NAME", param.getString("CLUE_SALESMAN_NAME")));
		User user = (User) this.getUser();
		this.setAttribute("ORDER_CREATER_NAME",
				this.getAttribute("ORDER_CREATER_NAME", user.getUserName()));
		this.setAttribute("ORDER_CREATER",
				this.getAttribute("ORDER_CREATER", user.getUserId()));
		String ccrtDate = (String) this.getAttribute("ORDER_CREATE_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));
		this.setAttribute("ORDER_CREATE_TIME", ccrtDate);
	}
	@PageAction
	public ViewRenderer createOrder(DataParam param) {
		getService().createOrderRecord(param);
		return new RedirectRenderer(getHandlerURL("OppInfoManageList"));
	}
	public ViewRenderer doBackAction(DataParam param){
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	protected OppInfoManage getService() {
        return  this.lookupService(OppInfoManage.class);
    }

}
