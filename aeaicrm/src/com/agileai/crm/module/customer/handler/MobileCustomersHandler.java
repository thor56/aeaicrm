package com.agileai.crm.module.customer.handler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.module.customer.service.CustomerGroup8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class MobileCustomersHandler extends BaseHandler {

	@PageAction
	public ViewRenderer findMyCustomersList(DataParam param) {
		String responseText = null;
		try {
			if(param.get("searchWord") == null){
	    		String inputString = this.getInputString();
	    		if(!"".equals(inputString)){
	    			JSONObject searchObject = new JSONObject(inputString);
		        	param.put("level", searchObject.get("level"));
		        	param.put("progressState", searchObject.get("progressState"));
	    		}
			}
        	
			JSONObject jsonObject = new JSONObject();
			User user = (User) getUser();
			PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
			if (!privilegeHelper.isSalesDirector()) {
				param.put("userId", user.getUserId());
			} else {
				param.put("userId", "");
			}
			
			List<DataRow> reList = getService().findMyCustomersList(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("CUST_ID"));
				jsonObject1.put("name", dataRow.get("CUST_NAME"));
				jsonObject1.put("progressState", dataRow.get("CUST_PROGRESS_STATE"));
				jsonObject1.put("level", dataRow.get("CUST_LEVEL"));
				jsonObject1.put("createName", dataRow.get("CUST_CREATE_NAME"));
				DataParam salesParam = new DataParam();
				salesParam.put("custId", dataRow.get("CUST_ID"));
				List<DataRow> salesList = getService().findSalesList(salesParam);
				String saleNames = "";
				for(int j=0;j<salesList.size();j++){
					DataRow saleRow = salesList.get(j);
					saleNames = saleNames + saleRow.get("USER_NAME") + "|";
				}
				if("".equals(saleNames)){
					saleNames = "暂无销售";
				}
				jsonObject1.put("saleNames",saleNames);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getCustomersDetails(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomerInfoRecord(param);
			jsonObject.put("id", dataRow.get("CUST_ID"));
			jsonObject.put("name", dataRow.get("CUST_NAME"));
			jsonObject.put("level", dataRow.get("CUST_LEVEL"));
			jsonObject.put("progressState", dataRow.get("CUST_PROGRESS_STATE"));
			jsonObject.put("industry", dataRow.get("CUST_INDUSTRY"));
			jsonObject.put("address", dataRow.get("CUST_ADDRESS"));
			jsonObject.put("province", dataRow.get("CUST_PROVINCE"));
			jsonObject.put("web", dataRow.get("CUST_COMPANY_WEB"));
			jsonObject.put("introduce", dataRow.get("CUST_INTRODUCE"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
    @PageAction
    public ViewRenderer createCustomers(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	User user = (User) this.getUser();
        	String datetime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
        	String userId = user.getUserId();
        	
        	String custId = KeyGenerator.instance().genKey();
        	String name = jsonObject.get("name").toString();
        	String level = jsonObject.get("level").toString();
        	String progressState = jsonObject.get("progressState").toString();
        	String industry = jsonObject.get("industry").toString();
        	String address = jsonObject.get("address").toString();
        	String province = jsonObject.get("province").toString();
        	String web = jsonObject.get("web").toString();
        	String introduce = jsonObject.get("introduce").toString();
        	
        	DataParam createParam = new DataParam("CUST_ID",custId,"CUST_NAME",name,"CUST_LEVEL",level,"CUST_PROGRESS_STATE",progressState,"CUST_INDUSTRY",industry,"CUST_ADDRESS",address,"CUST_PROVINCE",province,"CUST_COMPANY_WEB",web,"CUST_INTRODUCE",introduce,"CUST_CREATE_ID",userId,"CUST_CREATE_TIME",datetime,"CUST_STATE","init");
        	getService().createCustomers(createParam);
        	
        	DataRow dataRow = getService().queryTempGrpId(createParam);
        	String grpId = dataRow.getString("GRP_ID");
        	DataParam createCustGrpRelParam = new DataParam("GRP_ID",grpId,"CUST_ID",custId);
        	getService().insertCustomerInfoRelation(createCustGrpRelParam);
        	
        	
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    @PageAction
    public ViewRenderer editCustomersDetails(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	User user = (User) this.getUser();
        	String datetime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
        	String userId = user.getUserId();
        	
        	String id = jsonObject.get("id").toString();
        	String name = jsonObject.get("name").toString();
        	String level = jsonObject.get("level").toString();
        	String progressState = jsonObject.get("progressState").toString();
        	String industry = jsonObject.get("industry").toString();
        	String address = jsonObject.get("address").toString();
        	String province = jsonObject.get("province").toString();
        	String web = jsonObject.get("web").toString();
        	String introduce = jsonObject.get("introduce").toString();
        	
        	DataParam createParam = new DataParam("CUST_ID",id,"CUST_NAME",name,"CUST_LEVEL",level,"CUST_PROGRESS_STATE",progressState,"CUST_INDUSTRY",industry,"CUST_ADDRESS",address,"CUST_PROVINCE",province,"CUST_COMPANY_WEB",web,"CUST_INTRODUCE",introduce,"CUST_CREATE_ID",userId,"CUST_CREATE_TIME",datetime,"CUST_STATE","init");
        	getService().editCustomersDetails(createParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    @PageAction
    public ViewRenderer deleteCustomersInfo(DataParam param){
    	String responseText = FAIL;
    	try {
        	getService().deletContentsInfo(param);
    		getService().deleteCustomersInfo(param);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
	@PageAction
	public ViewRenderer getCustomersInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersInfo(param);
			jsonObject.put("id", dataRow.get("CUST_ID"));
			jsonObject.put("name", dataRow.get("CUST_NAME"));
			jsonObject.put("progressState", dataRow.get("CUST_PROGRESS_STATE"));
			jsonObject.put("level", dataRow.get("CUST_LEVEL"));
			jsonObject.put("createName", dataRow.get("CUST_CREATE_NAME"));			
			DataParam salesParam = new DataParam();
			salesParam.put("custId", dataRow.get("CUST_ID"));
			List<DataRow> salesList = getService().findSalesList(salesParam);
			String saleNames = "";
			for(int j=0;j<salesList.size();j++){
				DataRow saleRow = salesList.get(j);
				saleNames = saleNames + saleRow.get("USER_NAME") + "|";
			}
			if("".equals(saleNames)){
				saleNames = "暂无销售";
			}
			jsonObject.put("saleNames",saleNames);			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}  
	
	@PageAction
	public ViewRenderer findCustomersVisitInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findCustomersVisitInfo(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("VISIT_ID"));
				jsonObject1.put("name", dataRow.get("VISIT_USER_NAME"));
				jsonObject1.put("date", dataRow.get("VISIT_DATE"));
				jsonObject1.put("effect", dataRow.get("VISIT_EFFECT"));
				jsonObject1.put("visitType", dataRow.get("VISIT_TYPE"));
				jsonObject1.put("receptionName", dataRow.get("VISIT_RECEPTION_NAME"));
				jsonObject1.put("receptionPhone", dataRow.get("VISIT_RECEPTION_PHONE"));
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
    @PageAction
    public ViewRenderer createCustomersVisitInfo(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String id = param.get("VISIT_CUST_ID");
        	String type = jsonObject.get("type").toString();
        	String receptionName = jsonObject.get("receptionName").toString();
        	String receptionSex = jsonObject.get("receptionSex").toString();
        	String receptionJob = jsonObject.get("receptionJob").toString();
        	String receptionPhone = jsonObject.get("receptionPhone").toString();
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        	String visitDateUTC = jsonObject.get("visitDate").toString().replace("Z", " UTC");
        	Date date = format.parse(visitDateUTC);
        	String visitDate = DateUtil.format(DateUtil.YYMMDD_HORIZONTAL, date);
        	
        	String effect = jsonObject.get("effect").toString();
        	
        	DataParam createParam = new DataParam
        			("VISIT_CUST_ID",id,"VISIT_TYPE",type,"VISIT_USER_ID",userId,"VISIT_RECEPTION_NAME",receptionName,"VISIT_RECEPTION_SEX",
        					receptionSex,"VISIT_RECEPTION_JOB",receptionJob,"VISIT_RECEPTION_PHONE",
        					receptionPhone,"VISIT_DATE",visitDate,"VISIT_EFFECT",effect,
        					"VISIT_ FILL_ID",userId,"VISIT_FILL_TIME",new Date(),"VISIT_STATE","init");
        	getService().createCustomersVisitInfo(createParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    @PageAction
    public ViewRenderer deleteCustomersVisitInfo(DataParam param){
    	String responseText = FAIL;
    	try {
        	getService().deleteCustomersVisitInfo(param);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }

    @PageAction
	public ViewRenderer getCustomersVisitInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersVisitInfo(param);
			jsonObject.put("id", dataRow.get("VISIT_ID"));
			jsonObject.put("custId", dataRow.get("VISIT_CUST_ID"));
			jsonObject.put("type", dataRow.get("VISIT_TYPE"));
			jsonObject.put("receptionName", dataRow.get("VISIT_RECEPTION_NAME"));
			jsonObject.put("receptionSex", dataRow.get("VISIT_RECEPTION_SEX"));
			jsonObject.put("receptionJob", dataRow.get("VISIT_RECEPTION_JOB"));
			jsonObject.put("receptionPhone", dataRow.get("VISIT_RECEPTION_PHONE"));
			jsonObject.put("visitDate", dataRow.get("VISIT_DATE"));
			jsonObject.put("effect", dataRow.get("VISIT_EFFECT"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
    
    @PageAction
    public ViewRenderer editCustomersVisitInfo(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String type = jsonObject.get("type").toString();
        	String receptionName = jsonObject.get("receptionName").toString();
        	String receptionSex = jsonObject.get("receptionSex").toString();
        	String receptionJob = jsonObject.get("receptionJob").toString();
        	String receptionPhone = jsonObject.get("receptionPhone").toString();
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        	String visitDateUTC = jsonObject.get("visitDate").toString().replace("Z", " UTC");
        	Date date = format.parse(visitDateUTC);
        	String visitDate = DateUtil.format(DateUtil.YYMMDD_HORIZONTAL, date);
        	
        	String effect = jsonObject.get("effect").toString();
        	
        	DataParam editParam = new DataParam
        			("VISIT_ID",param.get("VISIT_ID"),"VISIT_TYPE",type,"VISIT_USER_ID",userId,"VISIT_RECEPTION_NAME",receptionName,"VISIT_RECEPTION_SEX",
        					receptionSex,"VISIT_RECEPTION_JOB",receptionJob,"VISIT_RECEPTION_PHONE",
        					receptionPhone,"VISIT_DATE",visitDate,"VISIT_EFFECT",effect,
        					"VISIT_ FILL_ID",userId,"VISIT_FILL_TIME",new Date(),"VISIT_STATE","init");
        	getService().editCustomersVisitInfo(editParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
	@PageAction
	public ViewRenderer findCustomersOppInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findCustomersOppInfo(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("OPP_ID"));
				jsonObject1.put("name", dataRow.get("OPP_NAME"));
				jsonObject1.put("datetime", dataRow.get("OPP_CREATE_TIME"));
				jsonObject1.put("level", dataRow.get("OPP_LEVEL"));
				jsonObject1.put("concernProduct", dataRow.get("OPP_CONCERN_PRODUCT"));
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
    @PageAction
	public ViewRenderer getCustomersOppInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersOppInfo(param);
			
			jsonObject.put("id", dataRow.get("OPP_ID"));
			jsonObject.put("custId", dataRow.get("CUST_ID"));
			jsonObject.put("name", dataRow.get("OPP_NAME"));
			jsonObject.put("concernProduct", dataRow.get("OPP_CONCERN_PRODUCT"));
			jsonObject.put("expectInvest", dataRow.get("OPP_EXPECT_INVEST"));
			jsonObject.put("level", dataRow.get("OPP_LEVEL"));
			jsonObject.put("des", dataRow.get("OPP_DES"));
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
    
    @PageAction
    public ViewRenderer editCustomersOppInfo(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String id = param.get("OPP_ID");
        	String name = jsonObject.get("name").toString();
        	String concernProduct = jsonObject.get("concernProduct").toString();
        	String expectInvest = jsonObject.get("expectInvest").toString();
        	String level = jsonObject.get("level").toString();
        	String des = jsonObject.get("des").toString();
        	
        	DataParam editParam = new DataParam
        			("OPP_ID",id,"OPP_NAME",name,"OPP_CONCERN_PRODUCT",id,"OPP_CONCERN_PRODUCT",concernProduct,
        					"OPP_EXPECT_INVEST",expectInvest,"OPP_STATE","init","OPP_LEVEL",level,
        					"OPP_DES",des,"OPP_CREATER",userId,"OPP_CREATE_TIME",new Date(),"CLUE_SALESMAN",userId);
        	
        	getService().editCustomersOppInfo(editParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
	
    @PageAction
    public ViewRenderer createCustomersOppInfo(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String id = param.get("CUST_ID");
        	String name = jsonObject.get("name").toString();
        	String concernProduct = jsonObject.get("concernProduct").toString();
        	String expectInvest = jsonObject.get("expectInvest").toString();
        	String level = jsonObject.get("level").toString();
        	String des = jsonObject.get("des").toString();
        	
        	DataParam createParam = new DataParam
        			("CUST_ID",id,"OPP_NAME",name,"OPP_CONCERN_PRODUCT",id,"OPP_CONCERN_PRODUCT",concernProduct,
        					"OPP_EXPECT_INVEST",expectInvest,"OPP_STATE","init","OPP_LEVEL",level,
        					"OPP_DES",des,"OPP_CREATER",userId,"OPP_CREATE_TIME",new Date(),"CLUE_SALESMAN",userId);
        	
        	getService().createCustomersOppInfo(createParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    @PageAction
    public ViewRenderer deleteCustomersOppInfo(DataParam param){
    	String responseText = FAIL;
    	try {
        	getService().deleteCustomersOppInfo(param);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
	
	@PageAction
	public ViewRenderer findCustomersContactsInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findPersonRecords(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("CONT_ID"));
				jsonObject1.put("name", dataRow.get("CONT_NAME"));
				jsonObject1.put("sex", dataRow.get("CONT_SEX"));
				jsonObject1.put("job", dataRow.get("CONT_JOB"));
				jsonObject1.put("phone", dataRow.get("CONT_PHONE"));
				jsonObject1.put("email", dataRow.get("CONT_EMAIL"));
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getCustomersContInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersContInfo(param);
			
			jsonObject.put("id", dataRow.get("CONT_ID"));
			jsonObject.put("custId", dataRow.get("CUST_ID"));
			jsonObject.put("job", dataRow.get("CONT_JOB"));
			jsonObject.put("name", dataRow.get("CONT_NAME"));
			jsonObject.put("sex", dataRow.get("CONT_SEX"));
			jsonObject.put("phone", dataRow.get("CONT_PHONE"));
			jsonObject.put("email", dataRow.get("CONT_EMAIL"));
			jsonObject.put("other", dataRow.get("CONT_OTHER"));
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
    @PageAction
    public ViewRenderer createCustomersContactsInfo(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	
        	String id = param.get("CUST_ID");
        	String job = jsonObject.get("job").toString();
        	String name = jsonObject.get("name").toString();
        	String sex = jsonObject.get("sex").toString();
        	String phone = jsonObject.get("phone").toString();
        	String email = jsonObject.get("email").toString();
        	String other = jsonObject.get("other").toString();
        	
        	DataParam createParam = new DataParam
        			("CUST_ID",id,"CONT_JOB",job,"CONT_NAME",name,"CONT_SEX",sex,"CONT_PHONE",phone,"CONT_EMAIL",email,"CONT_OTHER",other);
        	
        	getService().createCustomersContactsInfo(createParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    @PageAction
    public ViewRenderer editCustomersContactsInfo(DataParam param){
    	String responseText = FAIL;
    	try {
    		String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	
        	String id = param.get("CONT_ID");
        	String job = jsonObject.get("job").toString();
        	String name = jsonObject.get("name").toString();
        	String sex = jsonObject.get("sex").toString();
        	String phone = jsonObject.get("phone").toString();
        	String email = jsonObject.get("email").toString();
        	String other = jsonObject.get("other").toString();
        	
        	DataParam editParam = new DataParam
        			("CONT_ID",id,"CONT_JOB",job,"CONT_NAME",name,"CONT_SEX",sex,"CONT_PHONE",phone,"CONT_EMAIL",email,"CONT_OTHER",other);
        	
        	getService().editCustomersContactsInfo(editParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    @PageAction
    public ViewRenderer deleteCustomersContactsInfo(DataParam param){
    	String responseText = FAIL;
    	try {
        	getService().deleteCustomersContactsInfo(param);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
	
	@PageAction
	public ViewRenderer findCustomersOrderInfo(DataParam param) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findCustomersOrderInfo(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("name", dataRow.get("OPP_NAME"));
				jsonObject1.put("chief", dataRow.get("ORDER_CHIEF"));
				jsonObject1.put("salesmanName", dataRow.get("CLUE_SALESMAN_NAME"));
				jsonObject1.put("cost", dataRow.get("ORDER_COST")+"元");
				jsonObject1.put("datetime", dataRow.get("ORDER_CREATE_TIME"));
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	} 
	
	protected CustomerGroup8ContentManage getService() {
		return (CustomerGroup8ContentManage) this.lookupService(CustomerGroup8ContentManage.class);
	}
}
