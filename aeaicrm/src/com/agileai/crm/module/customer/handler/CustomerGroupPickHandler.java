package com.agileai.crm.module.customer.handler;

import java.util.*;

import com.agileai.crm.module.customer.service.CustomerGroupManage;
import com.agileai.domain.*;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.*;
import com.agileai.util.*;

public class CustomerGroupPickHandler
        extends TreeSelectHandler {
    public CustomerGroupPickHandler() {
        super();
        this.serviceId = buildServiceId(CustomerGroupManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "GRP_ID",
                                                  "GRP_NAME", "GRP_SUP_ID");

        String excludeId = param.get("GRP_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected CustomerGroupManage getService() {
        return (CustomerGroupManage) this.lookupService(this.getServiceId());
    }
}
