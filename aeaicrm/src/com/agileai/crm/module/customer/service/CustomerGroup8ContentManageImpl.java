package com.agileai.crm.module.customer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class CustomerGroup8ContentManageImpl
        extends TreeAndContentManageImpl
        implements CustomerGroup8ContentManage {
	
	protected Map<String,String> subTableIdNameMapping = new HashMap<String,String>();
	protected Map<String,String> subTableIdSortFieldMapping = new HashMap<String,String>();
	
    public CustomerGroup8ContentManageImpl() {
        super();
        this.columnIdField = "GRP_ID";
        this.columnParentIdField = "GRP_SUP_ID";
        this.columnSortField = "GRP_SORT";
    }
    
    public String[] getTableIds() {
        List<String> temp = new ArrayList<String>();

        temp.add("_base");
        temp.add("ContactPerson");
        temp.add("CustomerInfoSales");

        return temp.toArray(new String[] {  });
    }
    public List<DataRow> findSubRecords(String subId, DataParam param) {
		List<DataRow> result = null;
		String statementId = sqlNameSpace+"."+"find"+StringUtil.upperFirst(subId)+"Records";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
    
    public DataRow getCustomerInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustomerInfoRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	  public void saveSubRecords(DataParam param, List<DataParam> insertRecords, List<DataParam> updateRecords) {
			String subId = param.get("currentSubTableId");
			String curTableName = "crm_contact_person";
			if (insertRecords != null && insertRecords.size() > 0){
				String sortField = subTableIdSortFieldMapping.get(subId);
				int sortValue = 0;
				if (!StringUtil.isNullOrEmpty(sortField)){
					sortValue = getNewMaxSort(subId,param);	
				}
				for (int i=0;i < insertRecords.size();i++){
					DataParam paramRow = insertRecords.get(i);
					paramRow.put("CUST_ID", param.get("CUST_ID"));
					processDataType(paramRow, curTableName);	
					processPrimaryKeys(curTableName,paramRow);
					
					if (!StringUtil.isNullOrEmpty(sortField)){
						paramRow.put(sortField,sortValue+i);
					}
				}
			}
			String statementId = sqlNameSpace+"."+"insert"+StringUtil.upperFirst(subId)+"Record";
			this.daoHelper.batchInsert(statementId, insertRecords);
			
			statementId = sqlNameSpace+"."+"update"+StringUtil.upperFirst(subId)+"Record";
			this.daoHelper.batchUpdate(statementId, updateRecords);
		}
	
	protected int getNewMaxSort(String subId,DataParam param){
		int result = 0;
		String sortField = subTableIdSortFieldMapping.get(subId);
		String statementId = sqlNameSpace+"."+"find"+StringUtil.upperFirst(subId)+"Records";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		if (records != null && records.size() > 0){
			DataRow row = records.get(records.size()-1);
			String maxMenuSort = row.stringValue(sortField);
			result = Integer.parseInt(maxMenuSort)+1;
		}else{
			result = 1;
		}
		return result;
	}
	
	public void deleteSubRecord(String subId, DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteContactPersonRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}

	public void updateSubmitStateInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateSubmitStateInfoRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
		
	}

	public void updateConfirmStateInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateConfirmStateInfoRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
		
	}
	
	@Override
	public void updatetConfirmRecord(String tabId, DataParam param) {
			String statementId = sqlNameSpace+"."+"updatetConfirmRecord";
			String curTableName = tabIdAndTableNameMapping.get(tabId);
			processDataType(param, curTableName);
			this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void addUserTreeRelation(String custId, String[] groupIds) {
		String statementId = sqlNameSpace+".addUserTreeRelation";
		List<DataParam> paramList = new ArrayList<DataParam>();
		for (int i=0;i < groupIds.length;i++){
			DataParam dataParam = new DataParam("custId",custId,"userId",groupIds[i]);
			paramList.add(dataParam);
		}
		this.daoHelper.batchInsert(statementId, paramList);
	}

	@Override
	public void delUserTreeRelation(String custId, String groupId) {
		String statementId = sqlNameSpace+".delUserTreeRelation";
		DataParam param = new DataParam("custId",custId,"userId",groupId);
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public List<DataRow> findRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"queryRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;		
	}

	@Override
	public List<DataRow> findCityRecords(String proviceCode) {
		DataParam param = new DataParam("proviceCode",proviceCode);
		String statementId = sqlNameSpace+"."+"findCityRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;	
	}

	@Override
	public DataRow getStatisticsRecords(DataParam param) {
		DataRow result = new DataRow();
		
		String statementId = sqlNameSpace + ".getVisitNumRecord";
		DataRow visitNum = this.daoHelper.getRecord(statementId, param);
		
		statementId = sqlNameSpace + ".getAppointmentVisitNumRecord";
		DataRow appointmentVisitNum = this.daoHelper.getRecord(statementId, param);
		
		statementId = sqlNameSpace + ".getPhoneVisitNumRecord";
		DataRow phoneVisitNum = this.daoHelper.getRecord(statementId, param);
		
		statementId = sqlNameSpace + ".getEmailVisitNumRecord";
		DataRow emailVisitNum = this.daoHelper.getRecord(statementId, param);
		
		statementId = sqlNameSpace + ".getStrangeVisitNumRecord";
		DataRow strangeVisitNum = this.daoHelper.getRecord(statementId, param);
		
		statementId = sqlNameSpace + ".getOppNumRecord";
		DataRow oppNum = this.daoHelper.getRecord(statementId, param);
		
		statementId = sqlNameSpace + ".getOrderNumRecord";
		DataRow orderNum = this.daoHelper.getRecord(statementId, param);
		
		
		result.put("STATIS_VISIT",visitNum.get("STATIS_VISIT"));
		
		result.put("STATIS_APPOINTMENT_VISIT",appointmentVisitNum.get("STATIS_APPOINTMENT_VISIT"));
		result.put("STATIS_PHONE_VISIT",phoneVisitNum.get("STATIS_PHONE_VISIT"));
		result.put("STATIS_EMAIL_VISIT",emailVisitNum.get("STATIS_EMAIL_VISIT"));
		result.put("STATIS_STRANGE_VISIT",strangeVisitNum.get("STATIS_STRANGE_VISIT"));
		
		result.put("STATIS_OPP",oppNum.get("STATIS_OPP"));
		result.put("STATIS_ORDER",orderNum.get("STATIS_ORDER"));
		
		return result;
	}

	@Override
	public List<DataRow> findPersonRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findPersonRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;	
	}

	@Override
	public List<DataRow> findSalesRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findDetail";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> findMyCustomersList(DataParam param) {
		String statementId = sqlNameSpace+"."+"findMyCustomersList";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	@Override
	public List<DataRow> findSalesList(DataParam param) {
		String statementId = sqlNameSpace+"."+"findSalesList";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	@Override
	public DataRow getCustomersInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustomersInfo";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	} 

	@Override
	public void createCustomers(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertCustomerInfoRecord";
		this.daoHelper.insertRecord(statementId, param);
	}
	
	@Override
	public void editCustomersDetails(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateCustomerInfoRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void deletContentsInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteContactPersonRecords";
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	@Override
	public void deleteCustomersInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteCustomerInfoRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	@Override
	public List<DataRow> findCustomersVisitInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"findCustomersVisitInfo";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	@Override
	public DataRow getCustomersVisitInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustomersVisitInfo";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
	
	@Override
	public void createCustomersVisitInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"createCustomersVisitInfo";
		param.put("VISIT_ID", KeyGenerator.instance().genKey());
		this.daoHelper.insertRecord(statementId, param);
	}
	
	@Override
	public void editCustomersVisitInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"editCustomersVisitInfo";
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public void deleteCustomersVisitInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteCustomersVisitInfo";
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	@Override
	public List<DataRow> findCustomersOppInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"findCustomersOppInfo";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	@Override
	public void createCustomersOppInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"createCustomersOppInfo";
		param.put("OPP_ID", KeyGenerator.instance().genKey());
		this.daoHelper.insertRecord(statementId, param);
	}
	
	@Override
	public void editCustomersOppInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"editCustomersOppInfo";
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public void deleteCustomersOppInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteCustomersOppInfo";
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public List<DataRow> findCustomersOrderInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"findCustomersOrderInfo";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	@Override
	public DataRow getCustomersOppInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"getCustomersOppInfo";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}
	
	@Override
	public DataRow getCustomersContInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"getContactPersonRecord";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void createCustomersContactsInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertContactPersonRecord";
		param.put("CONT_ID", KeyGenerator.instance().genKey());
		this.daoHelper.insertRecord(statementId, param);
	}
	
	@Override
	public void editCustomersContactsInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"editCustomersContactsInfo";
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public void deleteCustomersContactsInfo(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteContactPersonRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	@Override
	public List<DataRow> getStatInfo() {
		String statementId = sqlNameSpace+".findOppOrderNumsRecords";
		DataParam param = new DataParam();
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> getOppContentData(String date) {
		String statementId = sqlNameSpace+".getOppContentDataRecords";
		String startDate = null;
		String endDate = null;
		if(date.endsWith("12")){
			startDate =date+"-01";
			endDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(startDate),DateUtil.YEAR,1));
			endDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(endDate),DateUtil.MONTH,-11));
		}else{
			startDate =date+"-01";
			endDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(startDate),DateUtil.MONTH,1));	
		}
		DataParam param = new DataParam("startDate",startDate,"endDate",endDate);
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> getOrderContentData(String date) {
		String statementId = sqlNameSpace+".getOrderContentDataRecords";
		String startDate = null;
		String endDate = null;
		if(date.endsWith("12")){
			startDate =date+"-01";
			endDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(startDate),DateUtil.YEAR,1));
			endDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(endDate),DateUtil.MONTH,-11));
		}else{
			startDate =date+"-01";
			endDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getDate(startDate),DateUtil.MONTH,1));	
		}
		DataParam param = new DataParam("startDate",startDate,"endDate",endDate);
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> getOrderSubContentData(String id) {
		String statementId = sqlNameSpace+".getOrderSubContentDataRecords";
		DataParam param = new DataParam("ORDER_ID",id);
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow queryTempGrpId(DataParam param) {
		String statementId = sqlNameSpace+".queryTempGrpId";
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		return dataRow;
	}

	@Override
	public void insertCustomerInfoRelation(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertCustomerInfoRelation";
		this.daoHelper.insertRecord(statementId, param);
	}
}
