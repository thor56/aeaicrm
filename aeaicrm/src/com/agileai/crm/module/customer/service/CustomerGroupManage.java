package com.agileai.crm.module.customer.service;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface CustomerGroupManage
        extends TreeAndContentManage {
	public void insertChildRecord(DataParam param);	
	
	public void changeTreeSort(String currentId, boolean isUp);
	
	public DataRow getGroupRecord(DataParam param);	
}
