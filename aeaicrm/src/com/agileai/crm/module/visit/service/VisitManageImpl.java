package com.agileai.crm.module.visit.service;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.module.visit.service.VisitManage;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class VisitManageImpl
        extends StandardServiceImpl
        implements VisitManage {
    public VisitManageImpl() {
        super();
    }

	@Override
	public void updateFillInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateFillStateInfoRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}

	public void updateConfirmStateInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateConfirmStateInfoRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
		
	}

	@Override
	public void createClueRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"createCLueRecord";
		String clueId = KeyGenerator.instance().genKey();
   		param.put("OPP_ID",clueId);
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
	}
	
}
