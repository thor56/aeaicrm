package com.agileai.crm.module.visit.service;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface VisitManage
        extends StandardService {
	
	
	public void updateFillInfoRecord(DataParam param);
	
	public void updateConfirmStateInfoRecord(DataParam param);
	
	void createClueRecord(DataParam param);
}
