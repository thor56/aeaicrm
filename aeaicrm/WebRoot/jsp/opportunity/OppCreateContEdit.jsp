<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>新增联系人</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	postRequest('form1',{actionType:'createCont',onComplete:function(CONT_ID){
		parent.ele('CONT_ID_NAME').value= $('#CONT_NAME').val();
		parent.ele('CONT_ID').value= CONT_ID;
			hideSplash();
			parent.PopupBox.closeCurrent();
			parent.refreshPage();
	}});
}
var custIdBox;
function openCustIdBox(){
	var handlerId = "CustomerListSelectList"; 
	if (!custIdBox){
		custIdBox = new PopupBox('custIdBox','请选择客户',{size:'big',height:'600px',width:'700px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=CUST_ID&targetName=CUST_ID_NAME';
	custIdBox.sendRequest(url);
} 
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn"  title="保存" />保存</td></aeai:previlege>
   <aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent()"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>客户</th>
	<td><input id="CUST_ID_NAME" label="客户" name="CUST_ID_NAME" type="text" value="<%=pageBean.inputValue("CUST_ID_NAME")%>" readonly="readonly" size="24" class="text" />
	<input id="CUST_ID" label="客户ID"  name="CUST_ID" type="hidden"	value="<%=pageBean.inputValue("CUST_ID")%>" size="24" class="text" />
</tr>
<tr>
  <th width="100" nowrap>姓名</th>
  <td><input name="CONT_NAME" type="text" class="text"	id="CONT_NAME"	value="<%=pageBean.inputValue("CONT_NAME")%>" size="24"	 label="姓名" /> 
  </td>
</tr>
<tr>
	<th width="100" nowrap>性别</th>
	<td><select id="CONT_SEX" label="性别" name="CONT_SEX" style="width: 189px" class="select"><%=pageBean.selectValue("CONT_SEX")%></select>
</td>
</tr>
<tr>
  <th width="100" nowrap>职位</th>
  <td><input name="CONT_JOB" type="text" class="text"	id="CONT_JOB"	value="<%=pageBean.inputValue("CONT_JOB")%>" size="24"	 label="姓名" /> 
  </td>
</tr>
<tr>
  <th width="100" nowrap>电话</th>
  <td><input name="CONT_PHONE" type="text" class="text"	id="CONT_PHONE"	value="<%=pageBean.inputValue("CONT_PHONE")%>" size="24"	 label="姓名" /> 
  </td>
</tr>
<tr>
  <th width="100" nowrap>邮箱</th>
  <td><input name="CONT_EMAIL" type="text" class="text"	id="CONT_EMAIL"	value="<%=pageBean.inputValue("CONT_EMAIL")%>" size="24"	 label="姓名" /> 
  </td>
</tr>
<tr>
<th width="100" nowrap>备注</th>
	<td><textarea id="CONT_OTHER" label="备注" name="CONT_OTHER" cols="60" rows="5" class="textarea"><%=pageBean.inputValue("CONT_OTHER")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="CONT_ID" name="CONT_ID" value="<%=pageBean.inputValue("CONT_ID")%>" />
</form>
<script language="javascript">
$('#CONT_OTHER').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
requiredValidator.add("CUST_ID");
requiredValidator.add("CONT_NAME");
requiredValidator.add("CONT_ID_NAME");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>