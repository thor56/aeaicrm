<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户组织资源</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function controlUpdateBtn(stateResult){
	if(stateResult =='0'){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		enableButton("editImgBtn");
		<%}%>
		enableButton("createPhoneCallBtn");
		<%if(pageBean.getBoolValue("doDel")){ %>
		disableButton("delImgBtn");
		<%}%>
	}
	if(stateResult =='1'){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		disableButton("createPhoneCallBtn");
		<%if(pageBean.getBoolValue("doDel")){ %>
		enableButton("delImgBtn");
		<%}%>
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("doEdit8Save")){ %>
   <aeai:previlege code="create"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td></aeai:previlege>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input id="editImgBtn" value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td></aeai:previlege>
<%} %>   
<%if(pageBean.getBoolValue("createPhoneCall")){ %>
   <aeai:previlege code="phoneCall"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx"  align="center" onclick="doRequest('doCreatePhoneCallAction')" ><input id="createPhoneCallBtn" value="&nbsp;"type="button" class="telephonecallImgBtn"  title="电话拜访" />电话拜访</td></aeai:previlege>  
 <%}%>    
   <aeai:previlege code="detail"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td></aeai:previlege> 
<%if(pageBean.getBoolValue("doDel")){ %>     
   <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></aeai:previlege>
<%} %>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;状态<select id="orgState" label="状态" name="orgState" class="select" onchange="doQuery()"><%=pageBean.selectValue("orgState")%></select>
&nbsp;更新日期<input id="sdate" label="开始日期" name="sdate" type="text" value="<%=pageBean.inputDate("sdate")%>" size="8" class="text" readonly="readonly"/><img id="sdatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
-<input id="edate" label="截止日期" name="edate" type="text" value="<%=pageBean.inputDate("edate")%>" size="8" class="text"  readonly="readonly"/><img id="edatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;邮箱<input id="orgEmail" label="邮箱" name="orgEmail" type="text" value="<%=pageBean.inputValue("orgEmail")%>" size="16" class="text" ondblclick="emptyText('orgEmail')" />
&nbsp;组织名称<input id="orgName" label="组织名称" name="orgName" type="text" value="<%=pageBean.inputValue("orgName")%>" size="8" class="text" ondblclick="emptyText('orgName')" />
&nbsp;分类<select id="orgClassification" label="分类" name="orgClassification"  class="select" onchange="doQuery()"><%=pageBean.selectValue("orgClassification")%></select>
&nbsp;标签<select id="ORG_LABELS" label="标签" name="ORG_LABELS" class="select" onchange="doQuery()"><%=pageBean.selectValue("ORG_LABELS")%></select>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" 
retrieveRowsCallback="process" 
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{ORG_ID:'${row.ORG_ID}'});controlUpdateBtn('${row.ORG_STATE}');refreshConextmenu()" onclick="selectRow(this,{ORG_ID:'${row.ORG_ID}'});controlUpdateBtn('${row.ORG_STATE}');">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="120" property="ORG_NAME" title="组织名称"   />
	<ec:column width="80" property="ORG_CLASSIFICATION" title="分类"   mappingItem="ORG_CLASSIFICATION"/>
	<ec:column width="100" property="ORG_LINKMAN_NAME" title="联系人"   />
	<ec:column width="100" property="ORG_EMAIL" title="邮箱"  />
	<ec:column width="50" property="ORG_STATE" title="状态"   mappingItem="ORG_STATE"/>
	<ec:column width="80" property="ORG_CREATER_NAME" title="创建人"   />
	<ec:column width="100" property="ORG_CREATE_TIME" title="创建时间"   />
    <ec:column width="100" property="ORG_UPDATE_TIME" title="更新时间"   />
	<ec:column width="120" property="ORG_LABELS_NAME" title="标签"   />
</ec:row>
</ec:table>
<input type="hidden" name="ORG_ID" id="ORG_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('ORG_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
initCalendar('sdate','%Y-%m-%d','sdatePicker');
initCalendar('edate','%Y-%m-%d','edatePicker');
datetimeValidators[0].set("yyyy-MM-dd").add("sdate");
datetimeValidators[0].set("yyyy-MM-dd").add("edate");
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
