<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户组织资源</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doCreatePhoneCallAction(){
	doSubmit({actionType:'doCreatePhoneCallAction'});
}
function doApporve(){
	doSubmit({actionType:'doApporve'});
}
function doRevokeApporve(){
	doSubmit({actionType:'doRevokeApporve'});
}
function doSaveAction(){
	if(validate()){
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
			if ("success" == responseText){
					window.location.href = "index?OrgInfoManageList";
				}else if("repeatName" == responseText){
						hideSplash();
						writeErrorMsg('组织名称重复，请检查！');
					}else if("repeatMail" == responseText){
						hideSplash();
						writeErrorMsg('邮箱名重复，请检查！');
					}else{
						hideSplash();
						writeErrorMsg('保存操作出错啦！');					
				}
			}
		})}
		return;
}
var orgIdBox;
function openProIdBox(){
	var handlerId = "LabelsTreeSelect"; 
	if (!orgIdBox){
		orgIdBox = new PopupBox('orgIdBox','请选择标签',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetName=ORG_LABELS_NAME'+'&targetId=ORG_LABELS';
	orgIdBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("doEdit8Save")){ %>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSaveAction()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
<%} %>  
<%if(pageBean.getBoolValue("createPhoneCall")){ %>
   <aeai:previlege code="phoneCall"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCreatePhoneCallAction()" ><input value="&nbsp;"type="button" class="telephonecallImgBtn" id="revokeApproval" title="电话拜访" />电话拜访</td></aeai:previlege> 
<%} %>
<aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>组织名称</th>
	<td><input id="ORG_NAME" label="组织名称" name="ORG_NAME" type="text" value="<%=pageBean.inputValue("ORG_NAME")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分类</th>
	<td><select id="ORG_CLASSIFICATION" label="分类" name="ORG_CLASSIFICATION" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_CLASSIFICATION")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>组织性质</th>
	<td><select id="ORG_TYPE" label="组织性质" name="ORG_TYPE" style="width: 303px" class="select"><%=pageBean.selectValue("ORG_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>联系人</th>
	<td><input id="ORG_LINKMAN_NAME" label="联系人" name="ORG_LINKMAN_NAME" type="text" value="<%=pageBean.inputValue("ORG_LINKMAN_NAME")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
<th width="100" nowrap>简要介绍</th>
	<td><textarea id="ORG_INTRODUCTION" label="简要介绍" name="ORG_INTRODUCTION" style="width:600px" cols="60" rows="5" maxlength="256" class="textarea"><%=pageBean.inputValue("ORG_INTRODUCTION")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>邮箱</th>
	<td><input id="ORG_EMAIL" label="邮箱" name="ORG_EMAIL" type="text" value="<%=pageBean.inputValue("ORG_EMAIL")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>地址</th>
	<td><input id="ORG_ADDRESS" label="地址" name="ORG_ADDRESS" type="text" value="<%=pageBean.inputValue("ORG_ADDRESS")%>" size="24" style="width:600px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>公司网站</th>
	<td><input id="ORG_WEBSITE" label="公司网站" name="ORG_WEBSITE" type="text" value="<%=pageBean.inputValue("ORG_WEBSITE")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>联系方式</th>
	<td><input id="ORG_CONTACT_WAY" label="联系方式" name="ORG_CONTACT_WAY" type="text" value="<%=pageBean.inputValue("ORG_CONTACT_WAY")%>" size="24" style="width:600px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>标签</th>
	<td><input id="ORG_LABELS_NAME" name="ORG_LABELS_NAME" type="text" value="<%=pageBean.inputValue("ORG_LABELS_NAME")%>" size="24" style="width: 302px" class="text" readonly="readonly" />
    <input id="ORG_LABELS" name="ORG_LABELS" type="hidden" value="<%=pageBean.inputValue("ORG_LABELS")%>" size="24" style="width: 302px" class="text"/><img id="orgIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openProIdBox()"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td><%=pageBean.selectRadio("ORG_STATE")%>
   </td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="ORG_CREATER_NAME" type="text" class="text"	id="ORG_CREATER_NAME"	value="<%=pageBean.inputValue("ORG_CREATER_NAME")%>" style="width: 302px" size="24"	readonly="readonly" label="创建人" /> 
		<input id="ORG_CREATER" label="创建人"  name="ORG_CREATER" type="hidden"	value="<%=pageBean.inputValue("ORG_CREATER")%>"  size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="ORG_CREATE_TIME" label="创建时间" name="ORG_CREATE_TIME" type="text" value="<%=pageBean.inputTime("ORG_CREATE_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />	
</td>
</tr>
<tr>
	<th width="100" nowrap>更新时间</th>
	<td><input id="ORG_UPDATE_TIME" label="更新时间" name="ORG_UPDATE_TIME" type="text" value="<%=pageBean.inputTime("ORG_UPDATE_TIME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue4DetailOrUpdate("ORG_ID","")%>" />
</form>
<script language="javascript">
$('#ORG_INTRODUCTION').inputlimiter({
	limit: 500,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
requiredValidator.add("ORG_NAME");
requiredValidator.add("ORG_CONTACT_WAY");
initDetailOpertionImage();
new BlankTrimer("ORG_NAME");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
