<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户个人资源</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doSaveAction(){
	if(validate()){
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
			if ("success" == responseText){
					window.location.href = "index?PerInfoManageList";
				}else if("repeatName" == responseText){
					hideSplash();
					writeErrorMsg('姓名重复，请检查！');	
				}else if("repeatMail" == responseText){
					hideSplash();
					writeErrorMsg('邮箱名重复，请检查！');
				}else{
					hideSplash();
					writeErrorMsg('保存操作出错啦！');					
				}
			}
		})}
}
var perIdBox;
function openProIdBox(){
	var handlerId = "LabelsTreeSelect"; 
	if (!perIdBox){
		perIdBox = new PopupBox('perIdBox','请选择标签',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetName=PER_LABELS_NAME'+'&targetId=PER_LABELS';
	perIdBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("doEdit8Save")){ %>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSaveAction()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
<%} %>   
   <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>姓名</th>
	<td><input id="PER_NAME" label="姓名" name="PER_NAME" type="text" value="<%=pageBean.inputValue("PER_NAME")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>性别</th>
	<td><select id="PER_SEX" label="性别" name="PER_SEX"  class="select"><%=pageBean.selectValue("PER_SEX")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>邮箱</th>
	<td><input id="PER_EMAIL" label="邮箱" name="PER_EMAIL" type="text" value="<%=pageBean.inputValue("PER_EMAIL")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>联系方式</th>
	<td><input id="PER_CONTACT_WAY" label="联系方式" name="PER_CONTACT_WAY" type="text" value="<%=pageBean.inputValue("PER_CONTACT_WAY")%>" style="width:600px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>所属公司</th>
	<td><input id="PER_BELONG_ORG" label="所属公司" name="PER_BELONG_ORG" type="text" value="<%=pageBean.inputValue("PER_BELONG_ORG")%>" style="width: 302px" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>标签</th>
	<td><input id="PER_LABELS_NAME" label="标签" name="PER_LABELS_NAME" type="text" value="<%=pageBean.inputValue("PER_LABELS_NAME")%>" style="width: 302px" size="24" class="text" />
     <input id="PER_LABELS" name="PER_LABELS" type="hidden" value="<%=pageBean.inputValue("PER_LABELS")%>" size="24" style="width: 302px" class="text"/>
    <img id="orgIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openProIdBox()"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td><%=pageBean.selectRadio("PER_STATE")%>
   </td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="PER_CREATER_NAME" type="text" class="text"	id="PER_CREATER_NAME"	value="<%=pageBean.inputValue("PER_CREATER_NAME")%>" style="width: 302px" size="24"	readonly="readonly" label="创建人" /> 
		<input id="PER_CREATER" label="创建人"  name="PER_CREATER" type="hidden"	value="<%=pageBean.inputValue("PER_CREATER")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="PER_CREATE_TIME" label="创建时间" name="PER_CREATE_TIME" type="text" value="<%=pageBean.inputTime("PER_CREATE_TIME")%>" style="width: 302px" readonly="readonly" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>更新时间</th>
	<td><input id="PER_UPDATE_TIME" label="更新时间" name="PER_UPDATE_TIME" type="text" value="<%=pageBean.inputTime("PER_UPDATE_TIME")%>" style="width: 302px" readonly="readonly" size="24" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="PER_ID" name="PER_ID" value="<%=pageBean.inputValue4DetailOrUpdate("PER_ID","")%>" />
</form>
<script language="javascript">
requiredValidator.add("PER_NAME");
requiredValidator.add("PER_CONTACT_WAY");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
