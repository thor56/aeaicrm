<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>生成客户</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<script language="javascript">
function createCustomer(){
	postRequest('form1',{actionType:'createCustomer',onComplete:function(CUST_ID){
		parent.ele('CUST_ID_NAME').value= $('#CUST_NAME').val();
		parent.ele('VISIT_CUST_ID').value= CUST_ID;
			hideSplash();
			parent.PopupBox.closeCurrent();
			parent.refreshPage();
	}});
}
</script>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="createCustomer()"><input value="&nbsp;" type="button" class="saveImgBtn"  title="保存" />保存</td></aeai:previlege>
   <aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent()"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>客户名称</th>
	<td><input id="CUST_NAME" label="客户名称" name="CUST_NAME" type="text" value="<%=pageBean.inputValue("CUST_NAME")%>" size="24" class="text" />
    <input id="CUST_ID" label="客户ID"  name="CUST_ID" type="hidden"	value="<%=pageBean.inputValue("CUST_ID")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="CUST_STATE_TEXT" label="状态" name="CUST_STATE_TEXT" type="text" value="<%=pageBean.selectedText("CUST_STATE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="CUST_STATE" label="状态" name="CUST_STATE" type="hidden" value="<%=pageBean.selectedValue("CUST_STATE")%>" />
   </td>
</tr>
<tr>
  <th width="100" nowrap>创建人</th>
  <td><input name="CUST_CREATE_NAME" type="text" class="text"	id="CUST_CREATE_NAME"	value="<%=pageBean.inputValue("CUST_CREATE_NAME")%>" size="24"	readonly="readonly" label="创建人" /> 
    <input id="CUST_CREATE_ID" label="创建人"  name="CUST_CREATE_ID" type="hidden"	value="<%=pageBean.inputValue("CUST_CREATE_ID")%>" size="24" class="text" />
  </td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="CUST_CREATE_TIME" label="创建时间" name="CUST_CREATE_TIME" type="text" value="<%=pageBean.inputTime("CUST_CREATE_TIME")%>" readonly="readonly" size="24" class="text" />
</td>
</tr>
<tr>
<th width="100" nowrap>简要介绍</th>
	<td><textarea id="CUST_INTRODUCE" label="简要介绍" name="CUST_INTRODUCE" cols="60" rows="6" class="textarea"><%=pageBean.inputValue("CUST_INTRODUCE")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="CUST_ID" name="CUST_ID" value="<%=pageBean.inputValue("CUST_ID")%>" />

</form>
<script language="javascript">
$('#CUST_INTRODUCE').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
requiredValidator.add("CUST_NAME");
requiredValidator.add("CUST_INTRODUCE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
