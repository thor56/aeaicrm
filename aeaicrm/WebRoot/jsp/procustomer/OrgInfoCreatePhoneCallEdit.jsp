<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>生成线索</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style type="text/css">
.markable0{
	background-color:darkorchid;
	color:black;
}
</style>
<script language="javascript">
function showEditBox(){
	postRequest('form1',{actionType:'check',onComplete:function(responseText){
		var editBox;
		if("success" == responseText){
			clearSelection();
			var handlerId='OrgInfoCreateCustomerEdit';
			if (!editBox){
				editBox = new PopupBox('editBox','新增客户',{size:'big',height:'350px',width:'600px',top:'30px'});
			}
			var url = 'index?'+handlerId+'&orgId='+ele('ORG_ID').value+'&operaType=insert';
			editBox.sendRequest(url);
		}else{
			alert("该名称的客户已存在");
		}
	 }});
}
var custIdBox;
function openCustIdBox(){
	var handlerId = "CustomerListSelectList"; 
	if (!custIdBox){
		custIdBox = new PopupBox('custIdBox','请选择客户',{size:'big',height:'500px',width:'700px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=VISIT_CUST_ID&targetName=CUST_ID_NAME';
	custIdBox.sendRequest(url);
} 
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'createVisit'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
   <aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
<td width="100" colspan="2" style="text-align: center;height:30px;font-weight: bold;padding-left: 10px;">正在电话拜访</td>
</tr>
<tr>
	<th width="100" nowrap>客户名称</th>
	<td><input id="CUST_ID_NAME" label="客户名称" name="CUST_ID_NAME" type="text" value="<%=pageBean.inputValue("CUST_ID_NAME")%>" readonly="readonly" size="24" class="text" />
     <input name="VISIT_CUST_ID"  id="VISIT_CUST_ID" value="<%=pageBean.inputValue("VISIT_CUST_ID")%>" type="hidden" style="hidden" label="客户ID" />
		<img id="custIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openCustIdBox()" />
	  <label onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="showEditBox()"><img id="editImage" src="images/modify.gif" width="16" height="16" />新增</label>
    </td>
</tr>
<tr>
	<th width="100" nowrap>接待人</th>
	<td><input id="VISIT_RECEPTION_NAME" label="联系人" name="VISIT_RECEPTION_NAME" type="text" value="<%=pageBean.inputValue("VISIT_RECEPTION_NAME")%>" size="24" class="text" />
</td>
</tr>
	<th width="100" nowrap>接待人性别</th>
	<td><%=pageBean.selectRadio("VISIT_RECEPTION_SEX")%></td>
<tr>
	<th width="100" nowrap>接待人电话</th>
	<td><input id="VISIT_RECEPTION_PHONE" label="接待人电话" name="VISIT_RECEPTION_PHONE" type="text" value="<%=pageBean.inputValue("VISIT_RECEPTION_PHONE")%>" size="24" style="width:600px" class="text" />
</td>
</tr>
	<th width="100" nowrap>拜访日期</th>
	<td><input name="VISIT_DATE" type="text" class="text" id="VISIT_DATE" value="<%=pageBean.inputDate("VISIT_DATE")%>" size="24" readonly="readonly" label="拜访日期" />
	  <img id="VISIT_DATEPicker2" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" /></td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td colspan="3"><input id="VISIT_STATE_TEXT" label="状态" name="VISIT_STATE_TEXT" type="text" value="<%=pageBean.selectedText("VISIT_STATE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="VISIT_STATE" label="状态" name="VISIT_STATE" type="hidden" value="<%=pageBean.selectedValue("VISIT_STATE")%>" />
</td>
</tr>
<tr>
  <th width="100" nowrap>拜访类型</th>
  <td>
      <input id="VISIT_TYPE_TEXT" label="状态" name="VISIT_TYPE_TEXT" type="text" value="<%=pageBean.selectedText("VISIT_TYPE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="VISIT_TYPE" label="状态" name="VISIT_TYPE" type="hidden" value="<%=pageBean.selectedValue("VISIT_TYPE")%>" />
  </td>
</tr>
<tr>
	<th width="100" nowrap>填写人</th>
	<td><input name="VISIT_FILL_NAME" type="text" class="text"	id="VISIT_FILL_NAME"	value="<%=pageBean.inputValue("VISIT_FILL_NAME")%>" size="24"	readonly="readonly" label="创建人" /> 
		<input id="VISIT_FILL_ID" label="创建人"  name="VISIT_FILL_ID" type="hidden"	value="<%=pageBean.inputValue("VISIT_FILL_ID")%>" size="24" class="text" />
      <input id="VISIT_USER_ID" label="拜访人员" name="VISIT_USER_ID" type="hidden" value="<%=pageBean.inputValue("VISIT_USER_ID")%>" size="24" class="text" hidden="hidden" /></td>
</tr>
<tr>
	<th width="100" nowrap>填写时间</th>
	<td><input id="VISIT_FILL_TIME" label="创建时间" name="VISIT_FILL_TIME" type="text" value="<%=pageBean.inputTime("VISIT_FILL_TIME")%>" readonly="readonly" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>沟通效果</th>
	<td colspan="3"><select id="VISIT_EFFECT" label="拜访类型" name="VISIT_EFFECT" class="select"><%=pageBean.selectValue("VISIT_EFFECT")%></select></td>
</tr>
<th width="100" nowrap>客户关注点</th>
	<td colspan="3"><textarea id="VISIT_CUST_FOCUS" label="交互内容" name="VISIT_CUST_FOCUS" cols="60" rows="5" style="width: 450px" class="textarea"><%=pageBean.inputValue("VISIT_CUST_FOCUS")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
<input type="hidden" id="ORG_CONTACT_WAY" name="ORG_CONTACT_WAY" value="<%=pageBean.inputValue("ORG_CONTACT_WAY")%>" />
</form>
<script language="javascript">
$('#VISIT_CUST_FOCUS').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
requiredValidator.add("CUST_ID_NAME");
requiredValidator.add("CLUE_SOURCE");
requiredValidator.add("VISIT_EFFECT");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
