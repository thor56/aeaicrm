<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function changeTab(tabId){
	$('#currentTabId').val(tabId);
	doSubmit({actionType:'prepareDisplay'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div class="photobg1" id="tabHeader" >
       <div class="newarticle1" onclick="changeTab('0')">组织资源</div>
    <div class="newarticle1" onclick="changeTab('1')">个体资源</div>

</div>
<div class="photobox newarticlebox" id="Layer0" style="height:620px;">
<% 
if ("0".equals(pageBean.inputValue("currentTabId"))){
%>
<iframe id="OrgFrame" src="index?OrgInfoManageList" width="100%" height="500px" frameborder="0" scrolling="no"></iframe>
<%}%>
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:620px;">
<% 
if ("1".equals(pageBean.inputValue("currentTabId"))){
%>
<iframe id="OrgFrame" src="index?PerInfoManageList" width="100%" height="500px" frameborder="0" scrolling="no"></iframe>
<%}%>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="currentTabId" name="currentTabId" value="" />
</form>
</body>
</html>
<script language="JavaScript">
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(<%=pageBean.inputValue("currentTabId")%>);
$(function(){
	resetTabHeight(80);
	resetTreeHeight(80);
});
</script>
