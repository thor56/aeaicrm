<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveTreeRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if ('success' == responseText){
			parent.refreshTree();
			parent.PopupBox.closeCurrent();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveTreeRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
   <aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>分组编码</th>
	<td><input id="GRP_CODE" label="GRP_CODE" name="GRP_CODE" type="text" value="<%=pageBean.inputValue("GRP_CODE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组名称</th>
	<td><input id="GRP_NAME" label="GRP_NAME" name="GRP_NAME" type="text" value="<%=pageBean.inputValue("GRP_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组状态</th>
	<td><select id="GRP_STATE" label="GRP_STATE" name="GRP_STATE" class="select"><%=pageBean.selectValue("GRP_STATE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>分组描述</th>
	<td><textarea id="GRP_DESC" label="简要介绍" name="GRP_DESC" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("GRP_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="GRP_ID" name="GRP_ID" value="<%=pageBean.inputValue4DetailOrUpdate("GRP_ID","")%>" />
<input type="hidden" id="GRP_SUP_ID" name="GRP_SUP_ID" value="<%=pageBean.inputValue("GRP_SUP_ID")%>" />
<input type="hidden" id="GRP_SORT" name="GRP_SORT" value="<%=pageBean.inputValue("GRP_SORT")%>" />
</form>
<script language="javascript">
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
