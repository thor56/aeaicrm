<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>线索管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style type="text/css">
.markable{
	background-color:yellow;
	color:black;
}
</style>
<script language="javascript">
function doAssign(){
	requiredValidator.add("CLUE_SALESMAN");
	doSubmit({actionType:'doAssign'});
}
function doClaim(){
	doSubmit({actionType:'doClaim'});
}
function doPause(){
	requiredValidator.add("CLUE_DISPOSE_DEL");
	doSubmit({actionType:'doPause'});
}
function doClose(){
	requiredValidator.add("CLUE_DISPOSE_DEL");
	doSubmit({actionType:'doClose'});
}
var editBox;
function showEditBox(operType){
	clearSelection();
	if (!editBox){
		editBox = new PopupBox('editBox','新增客户',{size:'big',height:'400px',width:'550px',top:'30px'});
	}
	var url = "<%=pageBean.getHandlerURL()%>&actionType="+operType;
	editBox.sendRequest(url);
}
var custIdBox;
function openCustIdBox(){
	var handlerId = "CustomerListSelectList"; 
	if (!custIdBox){
		custIdBox = new PopupBox('custIdBox','请选择客户',{size:'big',height:'500px',width:'700px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=CUST_ID&targetName=CUST_ID_NAME';
	custIdBox.sendRequest(url);
} 
var userIdBox;
function openUserIdBox(){
	var handlerId = "SalesmanListSelectList"; 
	if (!userIdBox){
		userIdBox = new PopupBox('userIdBox','请选择销售人员',{size:'normal',height:'420px',width:'550px',top:'2px'});  
	}
	var url = 'index?'+handlerId+'&targetId=CLUE_SALESMAN&targetName=CLUE_SALESMAN_NAME';
	userIdBox.sendRequest(url);
} 
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if(pageBean.getBoolValue("doEdit8Save")){ %>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td></aeai:previlege>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
<%} %>  
<%if(pageBean.getBoolValue("doAssign")){ %>
   <aeai:previlege code="allot"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doAssign();"><input value="&nbsp;" type="button" class="submitImgBtn" title="分配" />分配</td></aeai:previlege>
<%} %> 
<%if(pageBean.getBoolValue("doClaim")){ %>
   <aeai:previlege code="claim"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doClaim();"><input value="&nbsp;" type="button" class="doClaimImgBtn" title="认领" />认领</td></aeai:previlege>
<%} %> 
<%if(pageBean.getBoolValue("doPause8Close")){ %>
<aeai:previlege code="shelve"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doPause();"><input value="&nbsp;" type="button" class="removeImgBtn" title="搁置" />搁置</td></aeai:previlege>
<aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doClose();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
<%} %>
<aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>客户名称</th>
	<td><input id="CUST_ID_NAME" label="客户名称" name="CUST_ID_NAME" type="text" value="<%=pageBean.inputValue("CUST_ID_NAME")%>" readonly="readonly" style="width: 302px" size="24" class="text" />
	<%if(!pageBean.getBoolValue("readOnly")){ %>
	 	<img id="custIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openCustIdBox()" />	
    	<%} %>
    	<input id="CUST_ID" label="客户ID"  name="CUST_ID" type="hidden"	value="<%=pageBean.inputValue("CUST_ID")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>联系人</th>
	<td><input id="CLUE_LIKEMAN_NAME" label="联系人" name="CLUE_LIKEMAN_NAME" type="text" value="<%=pageBean.inputValue("CLUE_LIKEMAN_NAME")%>" size="24" style="width: 302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>联系方式</th>
	<td><input id="CLUE_CONN_WAY" label="联系方式" name="CLUE_CONN_WAY" type="text" value="<%=pageBean.inputValue("CLUE_CONN_WAY")%>" style="width:540px" size="24" class="text" />
</td>
</tr>
<%if(!pageBean.getBoolValue("readOnly")){ %>
<tr>
	<th width="100" nowrap>来源方式</th>
	<td><select id="CLUE_SOURCE" label="来源方式" name="CLUE_SOURCE" class="select"><%=pageBean.selectValue("CLUE_SOURCE")%></select>
</td>
</tr>
<%}%>
<%if(pageBean.getBoolValue("readOnly")){ %>
<tr>
	<th width="100" nowrap>来源方式</th>
	<td>
	<input id="CLUE_SOURCE_TEXT" label="来源方式" name="CLUE_SOURCE_TEXT" type="text" value="<%=pageBean.selectedText("CLUE_SOURCE")%>" size="24"  style="width: 302px" class="text" readonly="readonly"/>
	<input id="CLUE_SOURCE" label="来源方式" name="CLUE_SOURCE" type="hidden" value="<%=pageBean.selectedValue("CLUE_SOURCE")%>" />
   </td>
</tr>
<%} %>

<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="CLUE_CREATE_NAME" type="text" class="text"	id="CLUE_CREATE_NAME"	value="<%=pageBean.inputValue("CLUE_CREATE_NAME")%>" size="24" style="width: 302px"	readonly="readonly" label="创建人" /> 
		<input id="CLUE_CREATE_MAN" label="创建人"  name="CLUE_CREATE_MAN" type="hidden"	value="<%=pageBean.inputValue("CLUE_CREATE_MAN")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="CLUE_CREATE_TIME" label="创建时间" name="CLUE_CREATE_TIME" type="text" value="<%=pageBean.inputTime("CLUE_CREATE_TIME")%>" readonly="readonly" size="24" style="width: 302px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="CLUE_STATE_TEXT" label="状态" name="CLUE_STATE_TEXT" type="text" value="<%=pageBean.selectedText("CLUE_STATE")%>" size="24" style="width: 302px" class="text" readonly="readonly"/>
	<input id="CLUE_STATE" label="状态" name="CLUE_STATE" type="hidden" value="<%=pageBean.selectedValue("CLUE_STATE")%>" />
   </td>
</tr>
<%if(pageBean.getBoolValue("showSalesman")){ %>
<tr>
	<th width="100" nowrap>跟进人员</th>
	<td><input id="CLUE_SALESMAN_NAME" label="跟进人员" name="CLUE_SALESMAN_NAME" type="text" class="text markable" value="<%=pageBean.inputValue("CLUE_SALESMAN_NAME")%>" readonly="readonly"  size="24" class="text" />
	<%if(!pageBean.getBoolValue("readOnly")){ %>
	<img id="userIdSelectImage" src="images/sta.gif" width="16" height="16" onclick="openUserIdBox()" />
	<%} %>
	<input id="CLUE_SALESMAN" label="跟进人员" name="CLUE_SALESMAN" type="hidden" value="<%=pageBean.inputValue("CLUE_SALESMAN")%>"  />
</td>
</tr>
<tr>
	<th width="100" nowrap>认领时间</th>
	<td><input id="CLUE_GET_TIME" label="认领时间" name="CLUE_GET_TIME" type="text" value="<%=pageBean.inputTime("CLUE_GET_TIME")%>" readonly="readonly" size="24" style="width: 302px" class="text" />
</td>
</tr>
<%} %>
<%if(pageBean.getBoolValue("doDispose")){ %>
<tr>
<th width="100" nowrap>处置原因</th>
	<td><textarea id="CLUE_DISPOSE_DEL" label="处置原因" name="CLUE_DISPOSE_DEL" cols="40" rows="3" style="width: 540px" class="textarea"><%=pageBean.inputValue("CLUE_DISPOSE_DEL")%></textarea>
</td>
</tr>
<%} %>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="CLUE_ID" name="CLUE_ID" value="<%=pageBean.inputValue("CLUE_ID")%>" />
</form>
<script language="javascript">
$('#CLUE_DISPOSE_DEL').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
requiredValidator.add("CUST_ID");
requiredValidator.add("CLUE_SOURCE");
requiredValidator.add("CLUE_LIKEMAN_NAME");
requiredValidator.add("CLUE_CONN_WAY");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
