<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>线索管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function doClaim(actionType){
	if (actionType != insertRequestActionValue && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'doClaim'});
}
function doPause(actionType){
	if (actionType != insertRequestActionValue && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'doPause'});
}
function doClose(actionType){
	if (actionType != insertRequestActionValue && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'doClose'});
}


function controlUpdateBtn(stateResult){
	if(stateResult =='0'){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		enableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doCreateOpp")){ %>
		disableButton("createOppImgBtn");
		<%} %>   
		enableButton("detailImgBtn");
		<%if(pageBean.getBoolValue("doAssign")){ %>
		enableButton("assignImgBtn");
		<%} %> 
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		disableButton("claimImgBtn");
		disableButton("disposeImgBtn");
		<%} %> 
		enableButton("delImgBtn");
		<%if(pageBean.getBoolValue("isSalesDirector")){ %>
		enableButton("disposeImgBtn");
		<%}%>
	}
	if(stateResult =='1'){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doCreateOpp")){ %>
		disableButton("createOppImgBtn");
		<%} %> 
		enableButton("detailImgBtn");
		<%if(pageBean.getBoolValue("doAssign")){ %>
		enableButton("assignImgBtn");
		<%} %> 
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		enableButton("claimImgBtn");
		disableButton("disposeImgBtn");
		<%} %>
		disableButton("delImgBtn");
		<%if(pageBean.getBoolValue("isSalesDirector")){ %>
		enableButton("disposeImgBtn");
		<%}%>
	}
	if(stateResult =='2'){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		enableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doCreateOpp")){ %>
		enableButton("createOppImgBtn");
		<%} %> 
		enableButton("detailImgBtn");
		<%if(pageBean.getBoolValue("doAssign")){ %>
		disableButton("assignImgBtn");
		<%} %> 
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		disableButton("claimImgBtn");
		enableButton("disposeImgBtn");
		<%} %>
		disableButton("delImgBtn");
		<%if(pageBean.getBoolValue("isSalesDirector")){ %>
		enableButton("disposeImgBtn");
		<%}%>
	}
	if(stateResult =='3'){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doCreateOpp")){ %>
		disableButton("createOppImgBtn");
		<%} %> 
		enableButton("detailImgBtn");
		<%if(pageBean.getBoolValue("doAssign")){ %>
		enableButton("assignImgBtn");
		<%} %> 
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		enableButton("claimImgBtn");
		enableButton("disposeImgBtn");
		<%} %>
		disableButton("delImgBtn");
		<%if(pageBean.getBoolValue("isSalesDirector")){ %>
		enableButton("disposeImgBtn");
		<%}%>
	}
	if(stateResult =='4'){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doCreateOpp")){ %>
		disableButton("createOppImgBtn");
		<%} %> 
		enableButton("detailImgBtn");
		<%if(pageBean.getBoolValue("doAssign")){ %>
		enableButton("assignImgBtn");
		<%} %> 
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		disableButton("claimImgBtn");
		disableButton("disposeImgBtn");
		<%} %>
		disableButton("delImgBtn");
	}
}
function controlCanDis(clueSalesMan,clueState){
	if (clueState == "1" && clueSalesMan != "<%=pageBean.getStringValue("currentUserId")%>"){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		disableButton("claimImgBtn");
		disableButton("disposeImgBtn");
		 <%} %> 
		 <%if(pageBean.getBoolValue("isSalesDirector")){ %>
			enableButton("disposeImgBtn");
			<%}%>
	}
	if (clueState == "2" && clueSalesMan != "<%=pageBean.getStringValue("currentUserId")%>"){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doCreateOpp")){ %>
		disableButton("createOppImgBtn");
		<%} %> 
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		disableButton("claimImgBtn");
		disableButton("disposeImgBtn");
		 <%} %> 
		 <%if(pageBean.getBoolValue("isSalesDirector")){ %>
			enableButton("disposeImgBtn");
			<%}%>
	}
	if (clueState == "3" && clueSalesMan != "<%=pageBean.getStringValue("currentUserId")%>"){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		disableButton("claimImgBtn");
		disableButton("disposeImgBtn");
		 <%} %> 
		 <%if(pageBean.getBoolValue("isSalesDirector")){ %>
			enableButton("disposeImgBtn");
			<%}%>
	}
	if (clueState == "4" && clueSalesMan != "<%=pageBean.getStringValue("currentUserId")%>"){
		<%if(pageBean.getBoolValue("doEdit8Save")){ %>
		disableButton("editImgBtn");
		<%}%>
		<%if(pageBean.getBoolValue("doClaim8doPause8doClose")){ %>
		disableButton("claimImgBtn");
		disableButton("disposeImgBtn");
		 <%} %> 
	}
}

</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="create"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input id="insertImgBtn" value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td></aeai:previlege>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input id="editImgBtn" value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td></aeai:previlege>
   <aeai:previlege code="generationBusiness"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doRequest('doCreateOppAction')" ><input id="createOppImgBtn" value="&nbsp;"type="button" class="editImgBtn" id="revokeApproval" title="生成商机" />生成商机</td></aeai:previlege>
   <aeai:previlege code="detail"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input id="detailImgBtn" value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td></aeai:previlege>   
   <aeai:previlege code="allot"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doRequest('doAssignRequestAction')"><input id="assignImgBtn" value="&nbsp;" title="分配" type="button" class="editImgBtn" />分配</td></aeai:previlege>
   <aeai:previlege code="claim"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doClaim()" ><input id="claimImgBtn" value="&nbsp;"type="button" class="doClaimImgBtn" title="认领" />认领</td></aeai:previlege>
   <aeai:previlege code="disposal"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doRequest('doDispose')" ><input id="disposeImgBtn" value="&nbsp;"type="button" class="editImgBtn" title="处置" />处置</td></aeai:previlege>
   <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></aeai:previlege>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;状态<select id="CLUE_STATE" label="状态" name="CLUE_STATE" class="select" onchange="doQuery()"><%=pageBean.selectValue("CLUE_STATE")%></select>
&nbsp;来源方式<select id="CLUE_SOURCE" label="状态" name="CLUE_SOURCE" class="select" onchange="doQuery()"><%=pageBean.selectValue("CLUE_SOURCE")%></select>
&nbsp;客户姓名<input id="CUST_ID_NAME" label="客户姓名" name="CUST_ID_NAME" type="text" value="<%=pageBean.inputValue("CUST_ID_NAME")%>" size="15" class="text" ondblclick="emptyText('CUST_ID_NAME')" />
&nbsp;联系人<input id="CLUE_LIKEMAN_NAME" label="联系人" name="CLUE_LIKEMAN_NAME" type="text" value="<%=pageBean.inputValue("CLUE_LIKEMAN_NAME")%>" size="15" class="text" ondblclick="emptyText('CLUE_LIKEMAN_NAME')" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" 
retrieveRowsCallback="process" 
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{CLUE_ID:'${row.CLUE_ID}'});controlUpdateBtn('${row.CLUE_STATE}');controlCanDis('${row.CLUE_SALESMAN}','${row.CLUE_STATE}');refreshConextmenu()" onclick="selectRow(this,{CLUE_ID:'${row.CLUE_ID}'});controlUpdateBtn('${row.CLUE_STATE}');controlCanDis('${row.CLUE_SALESMAN}','${row.CLUE_STATE}');">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CUST_ID_NAME" title="客户名称"   />
	<ec:column width="100" property="CLUE_LIKEMAN_NAME" title="联系人"   />
	<ec:column width="100" property="CLUE_SOURCE" title="来源方式"   mappingItem="CLUE_SOURCE"/>
	<ec:column width="100" property="CLUE_STATE" title="状态"  mappingItem="CLUE_STATE" />
	<ec:column width="100" property="CLUE_SALESMAN_NAME" title="跟进人员"   />
	<ec:column width="100" property="CLUE_GET_TIME" title="认领时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="CLUE_CREATE_NAME" title="创建人"   />
	<ec:column width="100" property="CLUE_CREATE_TIME" title="创建时间" cell="date" format="yyyy-MM-dd HH:mm" />
</ec:row>
</ec:table>
<input type="hidden" name="CLUE_ID" id="CLUE_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('CLUE_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
