<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>生成商机</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="save"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'createOpp'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td></aeai:previlege>
   <aeai:previlege code="close"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<td width="100" colspan="2" style="text-align: center;height:30px;font-weight: bold;padding-left: 10px;">正在生成商机</td>
<tr>
	<th width="100" nowrap>商机名称</th>
	<td><input name="OPP_NAME" type="text" class="text"	id="OPP_NAME"	value="<%=pageBean.inputValue("OPP_NAME")%>" size="24" style="width: 302px"	 label="商机名称" /> 
	</td>
</tr>
<tr>
	<th width="100" nowrap>客户名称</th>
	<td><input name="CUST_ID_NAME" type="text" class="text"	id="CUST_ID_NAME"	value="<%=pageBean.inputValue("CUST_ID_NAME")%>" size="24" style="width: 302px"	readonly="readonly" label="客户名称" /> 
		<input id="CUST_ID" label="客户标识" name="CUST_ID" type="hidden" value="<%=pageBean.inputValue("CUST_ID")%>" size="24" class="text" />
		<input id="CLUE_ID" label="线索标识" name="CLUE_ID" type="hidden" value="<%=pageBean.inputValue("CLUE_ID")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>联系人</th>
	<td><input name="CONT_ID_NAME" type="text" class="text"	id="CONT_ID_NAME"	value="<%=pageBean.inputValue("CONT_ID_NAME")%>" size="24" style="width: 302px"	readonly="readonly" label="联系人" /> 
		<input id="CONT_ID" label="联系人"  name="CONT_ID" type="hidden"	value="<%=pageBean.inputValue("CONT_ID")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>跟进人员</th>
	<td><input name="CLUE_SALESMAN_NAME" type="text" class="text"	id="CLUE_SALESMAN_NAME"	value="<%=pageBean.inputValue("CLUE_SALESMAN_NAME")%>" size="24" style="width: 302px"	readonly="readonly" label="跟进人员" /> 
		<input id="CLUE_SALESMAN" label="跟进人员" name="CLUE_SALESMAN" type="hidden" value="<%=pageBean.inputValue("CLUE_SALESMAN")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>商机说明</th>
	<td><textarea id="OPP_DES" label="商机说明" name="OPP_DES" cols="40" rows="3" style="width:540px" class="textarea"><%=pageBean.inputValue("OPP_DES")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="OPP_STATE_TEXT" label="状态" name="OPP_STATE_TEXT" type="text" value="<%=pageBean.selectedText("OPP_STATE")%>" size="24" style="width:302px"  class="text" readonly="readonly"/>
	<input id="OPP_STATE" label="状态" name="OPP_STATE" type="hidden" value="<%=pageBean.selectedValue("OPP_STATE")%>" />
   </td>
</tr>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="OPP_CREATER_NAME" type="text" class="text"	id="OPP_CREATER_NAME"	value="<%=pageBean.inputValue("OPP_CREATER_NAME")%>" size="24" style="width:302px"	readonly="readonly" label="创建人" /> 
		<input id="OPP_CREATER" label="创建人"  name="OPP_CREATER" type="hidden"	value="<%=pageBean.inputValue("OPP_CREATER")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="OPP_CREATE_TIME" label="创建时间" name="OPP_CREATE_TIME" type="text" value="<%=pageBean.inputTime("OPP_CREATE_TIME")%>" readonly="readonly" style="width:302px" size="24" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="OPP_ID" name="OPP_ID" value="<%=pageBean.inputValue("OPP_ID")%>" />
</form>
<script language="javascript">
$('#OPP_DES').inputlimiter({
	limit: 100,
	remText: '还可以输入  %n 字 /',
	limitText: '%n 字',
	zeroPlural: false
	});
initDetailOpertionImage();
requiredValidator.add("OPP_NAME");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
